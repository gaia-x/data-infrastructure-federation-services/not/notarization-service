
/*
 *
 */
package eu.gaiax.notarization.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Neil Crossley
 */
public class VcTaskStart {

    @NotNull
    public String invitationURL;

    @NotBlank
    public String holderDID;
}