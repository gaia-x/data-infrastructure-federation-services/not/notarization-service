package eu.gaiax.notarization;

import eu.gaiax.notarization.domain.Profile;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import eu.gaiax.notarization.domain.RequestSession;
import eu.gaiax.notarization.domain.RequestStatus;
import eu.gaiax.notarization.domain.RequestSession.RequestValues;
import eu.gaiax.notarization.domain.RequestSession.SessionTaskSummary;
import eu.gaiax.notarization.domain.RequestSession.SessionTaskTree;
import eu.gaiax.notarization.domain.VcTaskStart;
import eu.gaiax.notarization.environment.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.MatcherAssert.*;

import static org.hamcrest.Matchers.*;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Assertions;

public class RequestorStepDefinitions {

    private static Logger logger = Logger.getLogger(RequestorStepDefinitions.class);

    private static final String BROWSER_IDENTIFICATION_TASK = "browserIdentificationTask";
    private static final String VC_IDENTIFICATION_TASK = "vcIdentificationTask";

    @Inject
    PersonManagement personManagement;

    @Inject
    RequestManagement requestManagement;

    @Inject
    RequestSubmissionApi requestSubmission;

    @Inject
    ProfileManagement profileManagement;

    @Inject
    DocumentManagement docManagement;

    @Inject
    IssuerManagement credentialManagement;

    @Inject
    HolderManagement holderManagement;

    @Inject
    EntityManager entityManager;

    @Given("I am a requestor")
    public void i_am_a_requestor() {
        personManagement.iAmARequestor();
    }

    @Given("I create a notarization request session")
    public void i_create_a_new_session_() {
        requestSubmission.createSession();
    }

    @Given("I have a submission session")
    public void i_have_a_submission_session() {
        requestSubmission.createSession()
                .statusCode(201);
    }

    @Given("there is a submission session with request {string}")
    public void i_have_a_submission_session(String name) {
        requestSubmission.createSession(name)
                .statusCode(201);
    }

    @Given("I have a submission session with an identification precondition")
    public void i_have_a_submission_session_with_identification_precondition() {
        requestSubmission.createSession(profileManagement.profileIdWithIdentificationPrecondition())
                .statusCode(201);
    }

    @Given("There is a submission session with an identification precondition and request {string}")
    @Given("I have a submission session with an identification precondition and request {string}")
    public void i_have_a_submission_session_with_identification_precondition(String requestName) {
        requestSubmission.createSession(requestName, profileManagement.profileIdWithIdentificationPrecondition())
                .statusCode(201);
    }

    @Given("I have a submission session for the portal")
    @Given("I create a notarization request session for the portal")
    @Given("I create(d)? a notarization request session for the portal")
    public void i_have_a_submission_session_for_the_portal() {
        requestSubmission.createSession(profileManagement.somePortalProfileId())
                .statusCode(201);
    }

    @Then("the request has the (status|state) {status}")
    public void the_request_has_the_status_status(RequestStatus status) {
        RequestSession currentRequest = requestManagement.currentRequest();
        currentRequest.person().given().get(currentRequest.location()).then().body("state", equalTo(status.toString()));
    }

    @When("I submit a notarization request")
    @When("I submit the notarization request")
    public void i_submit_a_notarization_request() {
        requestSubmission.submitNotarizationRequest();
    }

    @Given("I submit a notarization request {string}")
    public void i_submitted_a_notarization_request(String requestName) {
        requestSubmission.submitNotarizationRequest(requestName)
                .statusCode(201);
    }

    @Given("I created a notarization session with an identification precondition")
    @Given("I created a notarization session with any identification precondition")
    public void i_created_a_notarization_session_with_any_identification_precondition() {
        this.requestSubmission.createSession(this.profileManagement.profileIdWithIdentificationPrecondition());
    }

    @Given("I submitted a notarization request for the portal")
    public void i_submitted_a_notarization_request_requiring_identification() {
        this.requestSubmission.createSession(this.profileManagement.somePortalProfileId());
        requestSubmission.submitNotarizationRequest()
                .statusCode(201);
    }

    @Given("I submitted a notarization request for the profile {string}")
    public void i_submitted_a_notarization_request_for_the_profile(String profile) {
        this.requestSubmission.createSession(new Profile(profile));
        requestSubmission.submitNotarizationRequest()
                .statusCode(201);
    }

    @Given("I have a submission session for the profile {string}")
    public void i_have_a_submission_session_for_the_profile(String profile) {
        this.requestSubmission.createSession(new Profile(profile));
    }

    @Given("I have a submission session {string} for the profile {string}")
    public void i_have_a_submission_session_for_the_profile(String requestName, String profile) {
        this.requestSubmission.createSession(requestName, new Profile(profile));
    }

    @Then("I am able to submit a notarization request")
    public void i_am_able_to_submit_a_notarization_request() {
        requestSubmission.submitNotarizationRequest(
                this.requestManagement.currentRequest())
                .statusCode(201);
    }

    @Given("I am identified")
    public void i_am_identified() throws InterruptedException {
        final String taskType = BROWSER_IDENTIFICATION_TASK;
        var state = this.requestSubmission.fetchSummary(requestManagement.currentRequest());

        SessionTaskSummary identificationTask = findTaskByType(state, taskType);

        if (identificationTask != null) {
            this.requestSubmission.performBrowserIdentificationTask(
                    this.personManagement.currentRequestor(),
                    this.requestManagement.currentRequest(),
                    identificationTask);
        }

        int attempts = 0;
        do {
            Thread.sleep(200);
            state = this.requestSubmission.fetchSummary(requestManagement.currentRequest());

            identificationTask = findTaskByType(state, taskType);
        } while(identificationTask != null && (++attempts) < 3);
        assertThat("I could not be identified", identificationTask, nullValue());
    }

    private SessionTaskSummary findTaskByType(RequestValues state, final String taskType) {
        RequestSession.SessionTaskSummary identificationTask = null;
        if (!state.preconditionTasksFulfilled) {
            identificationTask = findTaskByType(state.preconditionTaskTree, taskType);
        }
        if (identificationTask == null && !state.tasksFulfilled) {
            identificationTask = findTaskByType(state.taskTree, taskType);
        }
        return identificationTask;
    }

    @When("I identify myself with the verifiable credential")
    public void i_identify_myself_with_verifiable_credentials() throws InterruptedException {
        final String taskType = VC_IDENTIFICATION_TASK;
        var state = this.requestSubmission.fetchSummary(requestManagement.currentRequest());

        var did = credentialManagement.latestCredentialDid();
        var invitationUrl = holderManagement.createInvitation();

        VcTaskStart vcTaskStart = new VcTaskStart();
        vcTaskStart.holderDID = did;
        vcTaskStart.invitationURL = invitationUrl;

        SessionTaskSummary identificationTask = findTaskByType(state, taskType);

        if (identificationTask != null) {
            this.requestSubmission.performVcIdentificationTask(
                    this.personManagement.currentRequestor(),
                    this.requestManagement.currentRequest(),
                    identificationTask,
                    vcTaskStart);
        }

        int attempts = 0;
        do {
            Thread.sleep(1000);
            state = this.requestSubmission.fetchSummary(requestManagement.currentRequest());

            identificationTask = findTaskByType(state, taskType);
        } while(identificationTask != null && (++attempts) < 10);
        assertThat("I could not be identified", identificationTask, nullValue());
    }

    @Given("I created a credential for profile {string}")
    public void i_created_a_credential(String profile) {
        credentialManagement.createCredential(profile);
    }

    @When("the credential is issued")
    public void the_credential_is_issued() {
        var did = credentialManagement.latestCredentialDid();
        credentialManagement.waitTilTheCredentialIsIssued(did);
    }

    @When("the maximum time period for session updates for {string} is over")
    @Transactional
    public void setModificationTimeOverdue(String name) {
        var sessId = requestManagement.request(name).sessionId();
        entityManager.createNativeQuery("update requestsession set lastModified = '1970-01-01' where id = '" + sessId + "'").executeUpdate();
    }

    @When("the scheduler sent the cleanup trigger")
    public void waitForSchedulerTrigger() throws InterruptedException {
        //We just wait
        Thread.sleep(2000);
    }

    @When("I mark my request done")
    public void i_mark_my_request_done() {
        this.requestSubmission.markDone(this.personManagement.currentRequestor(), requestManagement.currentRequest());
    }

    @Given("I have submitted a notarization request {string}")
    public void i_have_submitted_a_notarization_request(String name) {
        requestSubmission.createSession(name, profileManagement.somePortalProfileId());
        requestSubmission.submitNotarizationRequest(name);

    }

    @Given("I submitted a notarization request")
    public void i_submitted_a_notarization_request() {
        requestSubmission.createSession(profileManagement.somePortalProfileId());
        requestSubmission.submitNotarizationRequest();
    }

    @Given("I am not identified")
    public void i_am_not_identified() {
        logger.warn("No identification is the default state.");
    }

    @Then("the request has the status {string}")
    public void the_request_has_the_state(String state) {
        RequestValues summary = requestSubmission.fetchSummary(requestManagement.currentRequest());
        assertThat(summary.state, is(state));
    }

    @Then("the request {string} has the status {string}")
    @Then("the request {string} has the state {string}")
    public void the_request_has_the_state(String requestName, String state) {
        RequestValues summary = requestSubmission.fetchSummary(requestManagement.request(requestName));
        assertThat(summary.state, is(state));
    }

    @When("I upload a valid signed document")
    public void i_upload_a_valid_signed_document() {
        var session = requestManagement.currentRequest();
        docManagement.uploadValidPDFDocument(session);
    }

    @When("I upload an invalid signed document")
    public void i_upload_an_invalid_signed_document() {
        var session = requestManagement.currentRequest();
        docManagement.uploadInvalidXMLDocument(session);
    }

    @Given("A notarization request {string} with an uploaded valid document for the profile {string}")
    public void a_notarization_request_with_an_uploaded_valid_document(String requestName, String profile) {
        this.requestSubmission.createSession(requestName, new Profile(profile));
        requestSubmission.submitNotarizationRequest()
                .statusCode(201);
        docManagement.uploadValidPDFDocument(requestManagement.currentRequest());
        requestSubmission.markDone(this.personManagement.currentRequestor(), requestManagement.currentRequest());
    }

    @Given("A notarization request {string} for AIP 1.0 with an uploaded valid document for the profile {string}")
    public void a_notarization_request_for_aip_with_an_uploaded_valid_document_for_the_profile(String requestName, String profile) {
        this.requestSubmission.createSession(requestName, new Profile(profile));
        var request = requestManagement.currentRequest();
        requestSubmission.submitNotarizationRequestAIP10(request.person(), request)
                .statusCode(201);
        docManagement.uploadValidPDFDocument(requestManagement.currentRequest());
        requestSubmission.markDone(this.personManagement.currentRequestor(), requestManagement.currentRequest());
    }

    @Given("I submitted a ready request {string} without invitation URL.")
    public void a_business_owner_submitted_a_request_without_invitation_url(String requestName) {
        requestSubmission.createSession(requestName, profileManagement.profileIdWithoutTasks());
        requestSubmission.submitNotarizationRequestWithoutInvitation(requestManagement.request(requestName));
        requestSubmission.markDone(personManagement.currentRequestor(), requestManagement.request(requestName));
    }

    @Then("the invitation URL is available in at least {int} seconds")
    public void the_invitation_url_can_be_fetched_within_seconds(Integer timeout) {
        String invitationUrl = requestSubmission.fetchInvitationUrl(
                personManagement.currentRequestor(),
                requestManagement.currentRequest(),
                timeout);

        logger.infov("Invitation URL: {0}", invitationUrl);

        Pattern pattern = Pattern.compile("http://acapy:8030.*", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(invitationUrl);
        Assertions.assertTrue(matcher.find());
    }

    @When("I upload new data for {string}")
    @Given("I uploaded new data for {string}")
    public void i_upload_new_data_for(String requestName) {
        requestSubmission.updateNotarizationRequest(requestManagement.request(requestName));
    }

    @When("I delete my notarization request {string}")
    public void i_delete_my_notarization_request(String requestName) {
        requestSubmission.deleteNotarizationRequest(requestManagement.request(requestName));
    }

    @Given("a business owner has submitted a notarization request {string} for AIP1.0")
    public void a_business_owner_has_submitted_a_notarization_request_for_aip(String requestName) throws InterruptedException {
        var profile = profileManagement.profileIdAIP10();

        this.requestSubmission.createSession(requestName, profile);
        this.i_am_identified();

        this.requestSubmission.submitNotarizationRequestAIP10(
                personManagement.currentRequestor(),
                requestManagement.request(requestName));
        this.requestSubmission.markDone(
                personManagement.currentRequestor(),
                requestManagement.request(requestName));
    }

    private SessionTaskSummary findTaskByType(SessionTaskTree preconditionTaskTree, String targetType) {

        if (preconditionTaskTree == null) {
            return null;
        }
        if (preconditionTaskTree.task != null) {
            return Objects.equals(targetType, preconditionTaskTree.task.type)
                    ? preconditionTaskTree.task
                    : null;
        }
        if (preconditionTaskTree.allOf != null && !preconditionTaskTree.allOf.isEmpty()) {
            return findTaskByType(preconditionTaskTree.allOf, targetType);
        }
        if (preconditionTaskTree.oneOf != null && !preconditionTaskTree.oneOf.isEmpty()) {
            return findTaskByType(preconditionTaskTree.oneOf, targetType);
        }
        return null;
    }
    private SessionTaskSummary findTaskByType(Set<RequestSession.SessionTaskTree> tree, String targetType) {

        for (SessionTaskTree currentNode : tree) {
            var found = findTaskByType(currentNode, targetType);
            if (found != null) {
                return found;
            }
        }
        return null;
    }
}
