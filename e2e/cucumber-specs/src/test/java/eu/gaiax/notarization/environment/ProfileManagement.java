package eu.gaiax.notarization.environment;

import eu.gaiax.notarization.domain.Profile;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import java.util.Map;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.JsonArray;
import javax.json.JsonObject;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class ProfileManagement {

    private static final Logger logger = Logger.getLogger(ProfileManagement.class);

	private static String PROFILES_ALL_PATH = "/api/v1/profiles";
	private static String PROFILES_INITIALIZE_PATH = "/api/v1/routines/request-init-profiles";
    private static String PROFILES_SSI_DATA = PROFILES_ALL_PATH + "/{profileId}/ssi-data";

	private Map<Profile, JsonObject> knownProfiles;

	@Inject
	Configuration configuration;

	@Before
	public void before(Scenario scenario) {
		if (knownProfiles == null) {
                    try {
                    given()
                            .when()
                            .post(configuration.profile().url().toString() + PROFILES_INITIALIZE_PATH)
                            .then()
                            .statusCode(200);
                    } catch(Throwable t) {
                        logger.error("Could not initialize the profile DIDs!", t);
                    }
                    try {
			var profiles = given()
					.accept(ContentType.JSON)
					.when().get(configuration.profile().url().toString() + PROFILES_ALL_PATH)
					.as(JsonArray.class);


			knownProfiles = profiles.stream().map(v -> v.asJsonObject()).collect(Collectors.toMap(
					p -> new Profile(p.getString("id")), p -> p));
                    } catch(Throwable t) {
                        logger.error("Could not fetch known profiles!", t);
                    }
		}
		this.currentProfileId = null;
	}

	private Profile currentProfileId;

    public String fetchIssuingDIDForProfile(String profile) {
        var rawResponse = given()
                .when()
                .accept(ContentType.JSON)
                .pathParam("profileId", profile)
                .get(configuration.profile().url().toString() + PROFILES_SSI_DATA)
                .then()
                .statusCode(200);

        return rawResponse.extract().jsonPath().getString("issuingDid");
    }

	public Profile someProfileId() {
		return somePortalProfileId();
	}

	public Profile somePortalProfileId() {
		currentProfileId = new Profile(this.configuration.portalProfileId());
		return currentProfileId;
	}
	public Profile profileIdWithIdentificationPrecondition() {
		currentProfileId = new Profile(this.configuration.profileIdWithIdentificationPrecondition());
		return currentProfileId;
	}
    public Profile profileIdWithoutTasks() {
        currentProfileId = new Profile(this.configuration.profileIdWithoutTasks());
        return currentProfileId;
    }

    public Profile profileIdAIP10() {
        currentProfileId = new Profile(this.configuration.profileIdAip10());
        return currentProfileId;
    }
}
