
package eu.gaiax.notarization.environment;

import eu.gaiax.notarization.domain.AIP_1_0_Credential;
import eu.gaiax.notarization.domain.AIP_1_0_Results;
import eu.gaiax.notarization.domain.Credential;
import eu.gaiax.notarization.domain.Results;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class HolderManagement {

    private static final Logger logger = Logger.getLogger(HolderManagement.class);

    @Inject
    Configuration config;

    private String did;

    @Before
    public void beforeEach(Scenario scenario) {
        if (did == null) {
            try {
                did = given()
                        .accept(ContentType.JSON)
                        .contentType(ContentType.JSON)
                        .body("""
                              {
                                "method": "key",
                                "options": {
                                  "key_type": "ed25519"
                                }
                              }
                              """)
                        .post(config.holder().url().toString() + "/wallet/did/create")
                        .then()
                        .statusCode(200)
                        .extract()
                        .body().path("result.did").toString();
            } catch(Throwable t) {
                logger.error("Could not create DID", t);
            }
        }
    }

    public String did() {
        return did;
    }

    public String createInvitation() {
        var invite = given()
                    .accept(ContentType.JSON)
                    .contentType(ContentType.JSON)
                    .body("{}")
                    .queryParam("auto_accept", "true")
                    .post(config.holder().url().toString() + "/connections/create-invitation")
                    .then()
                    .statusCode(200)
                    .extract()
                    .body().path("invitation_url").toString();
        return invite;
    }

    public Optional<Credential> fetchCredentialByToken(String requestToken){
        var resultCreds = given()
                    .accept(ContentType.JSON)
                    .contentType(ContentType.JSON)
                    .body("{}")
                    .post(config.holder().url().toString() + "/credentials/w3c")
                    .then()
                    .statusCode(200)
                    .extract()
                    .body().as(Results.class);

        return resultCreds.results.stream()
            .filter(cred -> cred.requestToken().equals(requestToken))
            .findFirst();
    }

    public Optional<AIP_1_0_Credential> fetchAIP_1_0_CredentialByToken(String requestToken){
        var resultCreds = given()
                    .accept(ContentType.JSON)
                    .get(config.holder().url().toString() + "/credentials")
                    .then()
                    .statusCode(200)
                    .extract()
                    .body().as(AIP_1_0_Results.class);

        return resultCreds.results.stream()
            .filter(cred -> cred.requestToken().equals(requestToken))
            .findFirst();
    }

    public void removeCredential(String credentialId) {
        given()
                .pathParam("credentialId", credentialId)
                .delete(config.holder().url().toString() + "/credential/{credentialId}")
                .then()
                .statusCode(200);
    }
}
