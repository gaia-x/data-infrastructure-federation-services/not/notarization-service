package eu.gaiax.notarization.domain;

import javax.validation.constraints.NotNull;

/**
 *
 * @author Neil Crossley
 */
public record Profile(
	@NotNull String id) {

}