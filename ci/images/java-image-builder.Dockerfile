FROM ghcr.io/graalvm/jdk:ol8-java17-22

ENV GRAALVM_HOME=$JAVA_HOME
ENV PATH=${GRAALVM_HOME}bin:$PATH
