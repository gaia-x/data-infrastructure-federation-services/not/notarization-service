# BDD Tests

To demonstrate the functionality of the system, the project contains definitions and an environment to execute cucumber-tests for test-cases specified in gherkin language.

Definitions of scenarios can be found in `e2e/cucumber-specs/src/test/resources/specs`.

To execute the tests, first make sure the system is installed and running via Docker Compose as described in [documentation/developer/install-docker-compose.md](../developer/install-docker-compose.md)

When the system is running the tests can be executed as follows:
```sh
cd e2e/cucumber-specs
./mvnw test
```

One can limit the test execution by setting different tags in the following way:
```sh
./mvnw test -Dcucumber.filter.tags="@notary"
```
This will execute only scenarios tagged with `@notary`.

This allows testing for different aspects of the system.
The test specification defines the following tags:


| Tag        | Description                                                                                                     |
|------------|-----------------------------------------------------------------------------------------------------------------|
| @system    | Tests showing the functionality of the system besides the use-cases of users, like logging, administration etc. |
| @requestor | Tests regarding use-cases of the requestor                                                                      |
| @notary    | Tests regarding use-cases of the notary                                                                         |
| @happypath | Tests showing the functionality of processes as specified                                                       |
| @security  | Tests that consider security relevant details, like authorization                                                      |

