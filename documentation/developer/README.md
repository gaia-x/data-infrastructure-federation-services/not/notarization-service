# Developer guide

## Introduction

The Notarization system consists of several microservices and third-party components.

Following services are part of the notarization system:

* profile
* request-processing
* oidc-identity-resolver
* scheduler
* revocation
* ssi-issuance

The following third-party components are used:

* [Prometheus](https://prometheus.io/)
* [Grafana](https://grafana.com/)
* [Jaeger](https://www.jaegertracing.io/)
* [OpenTelemetry](https://opentelemetry.io/)
* [PostgreSQL](https://www.postgresql.org/)
* [RabbitMQ](https://www.rabbitmq.com/)
* [Hyperledger Aries Cloud Agent](https://github.com/hyperledger/aries-cloudagent-python)
* [Keycloak](https://www.keycloak.org/) (for Docker-Compose Deployment)

## Project structure

| Folder        | Description                                                                               |
|---------------|-------------------------------------------------------------------------------------------|
| ci            | Contains Gitlab CI specific files.                                                        |
| deploy        | Contains files for different kinds of deployments, such as docker-compose and k8s.        |
| documentation | Contains project documentation, such as admin, dev, design documentation.                 |
| e2e           | Contains BDD-Tests and Cucumber specification.                                            |
| scripts       | Contains some essential scripts to build docker and k8s resources.                        |
| services      | Contains the actual source code for the several services used in the notarization system. |

## Get started 

### Build from source

The `services` directory contains all services used in the notarization system.
They are built either with Java or NodeJS.
Each service contains its own README describing how the respective service can be built.

The services can be run locally via Docker-Compose, for more information take a look at the admin guide at [documentation/developer/install-docker-compose.md](../developer/install-docker-compose.md).

### Creating service images

The guide for creating images can be found [documentation/developer/images.md](./images.md)

### OpenAPI

The OpenAPI specification for the several services can be downloaded:

* [Profile](https://gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/-/jobs/artifacts/main/download?job=openapi-profile)
* [Request-Processing](https://gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/-/jobs/artifacts/main/download?job=openapi-request-processing)
* [OIDC-Identity-Resolver](https://gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/-/jobs/artifacts/main/download?job=openapi-oidc-identity-resolver)
* [Revocation](https://gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/-/jobs/artifacts/main/download?job=openapi-revocation)

Those OpenAPI specifications represent the current state of the services on the `main` branch. 

## Task types

A single [profile](../admin/profiles.md) specifies the configuration of individual tasks to be performed by the requestor within a request submission process. The development of new task types to be integrated into and supported by the request submission process is outlined [here](task-type-development.md).

## Notification Mechanism

In the notarization system, the `request-processing` service uses RabbitMQ as message queue system.
The `request-processing` service contributes messages over this message queue, for example when a request was accepted or rejected.

For more information about all the several messages and their data, see [here](notification-mechanism.md).

## BDD Tests

The guide for the execution of BDD tests can be found in [documentation/developer/bdd-tests.md](./bdd-tests.md)

## Load Tests

The guide for load tests can be found in [documentation/developer/load-tests.md](./load-tests.md)
