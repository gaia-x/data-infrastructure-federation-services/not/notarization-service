# Installation on Kubernetes

The installation on Kubernetes is recommended for productive environments.
Before the actual deployment can happen, some preconditions must be met.

## Installation prerequisites

* A shared PostgreSQL database.

  Please see the [database configuration section](database.md) for more information.
* A shared RabbitMQ instance.

  Please see the [message queue configuration section](message-queue.md) for more information.
* Identity provider.

  Please see the [notary authentication and authorization configuration section](auth.md) for more information.
* Aries Cloudagent.

  Please see the [acapy configuration section](acapy.md) for more information.
* Jaeger and OpenTelemetry.

  Please see the [monitoring configuration section](../monitoring.md) for more information.
* Digital Signature Service.

  Please see the [dss section](./dss.md) for more information.

## Deployment

Kubernetes Resources are provided in the directory `deploy/k8s`.
Take a look at the `README` there to understand how to deploy the notarization system.

The following services need to be configured:

* [Profile Service](services/profile.md)
* [OIDC-Identity-Resolver Service](services/oidc-identity-resolver.md)
* [Request-Processing Service](services/request-processing.md)
* [SSI-Issuance](services/ssi.md)
* [Revocation](services/revocation.md)
* [Scheduler](services/scheduler.md)

## Further information

Further information about the deployment, setting up ingress, protecting endpoints with TLS and more, can be found in the [../../deploy/k8s/README.md](../../deploy/k8s/README.md).
