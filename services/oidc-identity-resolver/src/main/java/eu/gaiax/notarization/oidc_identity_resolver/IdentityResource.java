/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.oidc_identity_resolver;

import io.quarkus.security.Authenticated;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.jboss.resteasy.reactive.ResponseStatus;

@Path("/")
public class IdentityResource {

    public static final String START_LOGIN_RES = "start-login";
    public static final String START_LOGIN_RES_NONCE = START_LOGIN_RES + "/{nonce}";
    public static final String OIDC_LOGIN_RES = "login";
    public static final String OIDC_ERROR_PATH = "error";
    public static final String SESSION_RES = "session";
    public static final String SESSION_RES_NONCE = SESSION_RES + "/{nonce}";

    private final IdentityStore store;
    private final Optional<URI> externalUrl;
    private final URI redirectLoginSuccess;
    private final URI redirectLoginFailure;


    @Context
    SecurityIdentity secId;

    @Context UriInfo request;

    public IdentityResource(
            IdentityStore store,
            @ConfigProperty(name = "oidc-identity-resolver.external.url") Optional<URI> externalUrl,
            @ConfigProperty(name = "redirect.login-success.url") URI redirectLoginSuccess,
            @ConfigProperty(name = "redirect.login-failure.url") URI redirectLoginFailure) {
        this.store = store;
        this.externalUrl = externalUrl;
        this.redirectLoginSuccess = redirectLoginSuccess;
        this.redirectLoginFailure = redirectLoginFailure;
    }

    @POST
    @Path(SESSION_RES)
    @Produces(MediaType.APPLICATION_JSON)
    @ResponseStatus(200)
    @APIResponse(responseCode = "200", description = "A redirect and a cancel URL.", content = @Content(schema = @Schema(implementation = BeginResponse.class)))
    public Uni<BeginResponse> beginIdentification(
        @Context UriInfo uriInfo,
        @QueryParam("success") URI success,
        @QueryParam("failure") URI failure
    ) throws URISyntaxException {
    return store.startIdentification(success, failure)
        .onItem().ifNotNull().transform(nonces -> {

            return new BeginResponse(
                uriBuilder(uriInfo)
                    .path(START_LOGIN_RES_NONCE)
                    .build(nonces.loginNonce()),
                uriBuilder(uriInfo)
                    .path(SESSION_RES_NONCE)
                    .build(nonces.cancelNonce()));
        });
    }

    private UriBuilder uriBuilder(UriInfo uriInfo) {
        return externalUrl
                .map(url -> UriBuilder.fromUri(url))
                .orElseGet(() -> uriInfo.getBaseUriBuilder());
    }

    @DELETE
    @Path(SESSION_RES_NONCE)
    public Uni<Void> cancelIdentification(
        @PathParam("nonce") String nonce
    ){
        return store.cancelIdentification(nonce);
    }

    @GET
    @Path(START_LOGIN_RES + "/" + "{nonce}")
    public Response startLogin(
        @PathParam("nonce") String nonce
    ) throws URISyntaxException {
        NewCookie cookie = createNonceCookie(nonce);
        URI oidcLoginUri = UriBuilder.fromPath(OIDC_LOGIN_RES).build();
        return Response.seeOther(oidcLoginUri).cookie(cookie).build();
    }

    @GET
    @Path(OIDC_LOGIN_RES)
    @Authenticated
    @SecurityRequirement(name = "oidc")
    public Uni<Response> oidcLogin(
        @CookieParam("nonce") String nonce
    ) throws URISyntaxException {
        return store.performLogin(nonce, secId)
                .map((Void v) -> Response.seeOther(this.redirectLoginSuccess).build());
    }

    @GET
    @Path(OIDC_ERROR_PATH)
    public Uni<Response> oidcError(
            @CookieParam("nonce") String nonce
    ) throws URISyntaxException {
        return store.cancelIdentification(nonce)
                .map(this::oidcErrorResponse);
    }

    private Response oidcErrorResponse(Void v) {
        final UriBuilder targetErrorUri = UriBuilder.fromUri(this.redirectLoginFailure);
        for (Map.Entry<String, List<String>> queryParameterEntry : request.getQueryParameters().entrySet()) {
            targetErrorUri.queryParam(queryParameterEntry.getKey(), queryParameterEntry.getValue().toArray());
        }
        return Response.seeOther(targetErrorUri.build())
                .build();
    }

    private NewCookie createNonceCookie(String nonce) {
        return new NewCookie(
                "nonce",
                nonce,
                "/",
                null,
                Cookie.DEFAULT_VERSION,
                null,
                NewCookie.DEFAULT_MAX_AGE,
                null,
                true,
                true
        );
    }
}
