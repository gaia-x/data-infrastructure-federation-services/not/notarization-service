/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 * @author Florian Otto
 */
public class ProfileTaskTree {

        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        public Set<ProfileTaskTree> allOf;
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        public Set<ProfileTaskTree> oneOf;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        public String taskName;

        public ProfileTaskTree(){
            this.allOf = new HashSet<>();
            this.oneOf= new HashSet<>();
            this.taskName = null;
        }

        public boolean treeEmpty(){
            return
                this.taskName == null
                && this.allOf.isEmpty()
                && this.oneOf.isEmpty()
                ;
        }

        public boolean containsTaskByName(String taskName){
            if(this.taskName.equals(taskName)){
                return true;
            } else {
                return Stream.concat(allOf.stream(), oneOf.stream())
                    .filter(childTree -> childTree.containsTaskByName(taskName))
                    .findAny()
                    .isPresent()
                    ;
            }

        }

        public Set<String> allNames(){
            var res = new HashSet<String>();
            this.fillNames(res);
            return res;
        }

        private void fillNames(Set<String> set){
            if(this.taskName != null){
                set.add(this.taskName);
            }else{
                allOf.stream().forEach((sub)->sub.fillNames(set));
                oneOf.stream().forEach((sub)->sub.fillNames(set));
            }

        }

        public boolean structureValid(){
            if(taskName != null){
                return allOf.isEmpty() && oneOf.isEmpty();
            }else if(!allOf.isEmpty()) {
                return oneOf.isEmpty()
                    && allOf.stream().allMatch(sub -> sub.structureValid());
            }else{
                return oneOf.stream().allMatch(sub -> sub.structureValid());
            }
        }

        //tells if a nonEmpty tree is fulfilled without the need of performing any task (all optional)
        public boolean fulfilledByDefault(){
            //no tasks, that's ok
            if(allNames().size()==0){
                return false;
            } else {
                return this.fullfilled();
            }
        }

        public boolean fullfilled(){
            if(this.treeEmpty()){
                return true;
            } else if(!allOf.isEmpty()){
                return allOf.stream().allMatch(subtree -> subtree.fullfilled());
            } else if(!oneOf.isEmpty()){
                return oneOf.stream().anyMatch(subtree -> subtree.fullfilled());
            }
            return false;
        }

}
