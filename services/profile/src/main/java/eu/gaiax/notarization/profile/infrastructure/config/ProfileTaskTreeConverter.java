/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.profile.domain.model.ProfileTaskTree;
import org.eclipse.microprofile.config.spi.Converter;

/**
 *
 * @author Neil Crossley
 */
public class ProfileTaskTreeConverter implements Converter<ProfileTaskTree> {

    public final ObjectMapper mapper = new ObjectMapper();

    @Override
    public ProfileTaskTree convert(String value) throws IllegalArgumentException, NullPointerException {

        try {
            return mapper.readValue(value, ProfileTaskTree.class);
        } catch (JsonProcessingException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
}
