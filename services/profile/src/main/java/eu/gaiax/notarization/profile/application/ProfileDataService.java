/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.application;

import com.fasterxml.jackson.databind.JsonNode;
import eu.gaiax.notarization.profile.domain.entity.ProfileDid;
import eu.gaiax.notarization.profile.domain.service.ProfileService;
import eu.gaiax.notarization.profile.infrastructure.rest.client.RevocationHttpClient;
import eu.gaiax.notarization.profile.infrastructure.rest.client.SsiIssuanceHttpClient;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class ProfileDataService {

    private static Set<Status> ACCEPTABLE_REVOCATION = Set.of(
            Status.ACCEPTED,
            Status.NO_CONTENT,
            Status.CONFLICT
    );

    @Inject
    ProfileService profileService;

    @RestClient
    SsiIssuanceHttpClient ssiIssuanceClient;

    @RestClient
    RevocationHttpClient revocationClient;

    @Inject
    Logger logger;

    public Uni<Long> requestUpdateOutstandingDids() {
        return findUnprocessedProfiles()
            .onItem().transformToUniAndConcatenate(profileName -> initDids(profileName))
            .onItem().transformToUniAndConcatenate(profileName -> initRevocation(profileName))
            .collect().with(Collectors.counting());
    }

    private Multi<String> findUnprocessedProfiles() {
        return ProfileDid.<ProfileDid>findAll(Sort.ascending("profileId"))
            .list()
            .onItem().transformToMulti(foundItems -> {
                var currentProfileIds = foundItems.stream().map(item -> item.profileId).collect(Collectors.toSet());
                var knownProfileIds = profileService.map().keySet();
                List<String> unknownItems = new ArrayList<>(knownProfileIds.size());

                for (String knownProfileId : knownProfileIds) {
                    if (!currentProfileIds.contains(knownProfileId)) {
                        unknownItems.add(knownProfileId);
                    }
                }
                return Multi.createFrom().items(unknownItems.stream());
            });
    }

    @ReactiveTransactional
    Uni<String> initDids(String profileId) {
        if (profileId == null) {
            return Uni.createFrom().nullItem();
        }
        return this.ssiIssuanceClient.initiate(SsiIssuanceHttpClient.DidInitRequest.from(profileId))
                .map(response -> new ProfileDidInit(profileId, response))
                .flatMap(item -> {
                    if (item != null && item.response != null) {
                        ProfileDid newRecord = new ProfileDid();
                        newRecord.id = UUID.randomUUID();
                        newRecord.profileId = item.profileId;
                        newRecord.issuanceContent = item.response;
                        return newRecord.persist().map(v -> profileId);
                    } else {
                        return Uni.createFrom().nullItem();
                    }
                })
                .onFailure().recoverWithUni(t -> {
                    logger.errorv("Could not initialize the DIDs of {0}", profileId, t);
                    return Uni.createFrom().nullItem();
                });
    }

    private Uni<Response> initRevocation(String profileId) {
        if (profileId == null) {
            return Uni.createFrom().nullItem();
        }
        return revocationClient.initiate(profileId).onFailure(WebApplicationException.class).recoverWithItem((throwable) -> {
            if (throwable instanceof WebApplicationException) {
                final Response response = ((WebApplicationException)throwable).getResponse();

                if (response.getStatus() != Status.CONFLICT.getStatusCode()) {
                    logger.errorv("Received an unexpected error while initializing the revocation service for the profile {0}: [{1},{2},{3}]",
                        profileId,
                        response.getStatus(), response.getEntity(), response.getHeaders());
                    throw new IllegalArgumentException("Unexcepted response from revocation service", throwable);
                } else {
                    return response;
                }
            }
            else {
                throw new IllegalArgumentException("Unexpected exception from revocation client", throwable);
            }
        })
        .onFailure().recoverWithUni(t -> {
            logger.errorv("Could not initialize the revocation of {0}", profileId, t);
            return Uni.createFrom().nullItem();
        });
    }

    public static record ProfileDidInit(String profileId, JsonNode response) {}
}
