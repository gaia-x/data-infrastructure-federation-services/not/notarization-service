/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.profile.domain.model.NotaryAccess;
import eu.gaiax.notarization.profile.domain.model.Profile;
import eu.gaiax.notarization.profile.domain.model.TaskDescription;
import eu.gaiax.notarization.profile.domain.service.ProfileService;
import eu.gaiax.notarization.profile.infrastructure.config.ProfileSourceConfig.NotaryAccessConfig;
import eu.gaiax.notarization.profile.infrastructure.config.ProfileSourceConfig.ProfileConfig;
import io.quarkus.runtime.StartupEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.jose4j.jwk.EllipticCurveJsonWebKey;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jwk.RsaJsonWebKey;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class ConfigBackedProfileService implements ProfileService {

    private static final Map<String, String> FALLBACK_NOTARY_ALGORITHMS = Map.of(
            RsaJsonWebKey.KEY_TYPE, "RSA-OAEP-256",
            EllipticCurveJsonWebKey.KEY_TYPE, "ECDH-ES+A256KW"
    );

    private Map<String, Profile> profilesById = new HashMap<>();
    private List<Profile> profiles;

    @Inject
    ProfileSourceConfig profilesConfig;

    @Inject
    ObjectMapper mapper;

    @Override
    public List<Profile> list() {
        return profiles;
    }

    @Override
    public Profile fetchProfile(String id) {
        return profilesById.get(id);
    }

    void onStartup(@Observes StartupEvent ev) {

        List<Profile> allProfiles = new ArrayList<>(profilesConfig.config().size());
        for (ProfileConfig profileConfig : profilesConfig.config()) {

            try {
                var converted = asProfile(profileConfig);
                profilesById.put(converted.id(), converted);
                allProfiles.add(converted);
            }catch (InvalidTaskTreeConfigurationException ex){
                throw new RuntimeException(ex);
            }
        }
        this.profiles = Collections.unmodifiableList(allProfiles);
    }

    Profile asProfile(ProfileConfig profile) throws InvalidTaskTreeConfigurationException {

        var result = new Profile(
                profile.id(),
                profile.aip(),
                profile.name(),
                profile.description(),
                profile.encryption(),
                asNotaryAccess(profile.notaries()),
                profile.validFor(),
                profile.isRevocable(),
                profile.template(),
                profile.documentTemplate().orElse(null),
                profile.taskDescriptions().getValuesAs((v)->
                    mapper.convertValue(v, TaskDescription.class)
                ),
                profile.tasks(),
                profile.preconditionTasks()
        );
        result.assertTreesValid();
        return result;
    }

    List<NotaryAccess> asNotaryAccess(List<NotaryAccessConfig> access) {
        return access.stream().map(this::asNotaryAccess).toList();
    }

    NotaryAccess asNotaryAccess(NotaryAccessConfig access) {
        final PublicJsonWebKey jwk = access.jwk();
        final String algorithm;
        if (access.algorithm().isPresent()) {
            algorithm = access.algorithm().get();
        } else {
            algorithm = profilesConfig.defaultAlgorithms().getOrDefault(jwk.getKeyType(), FALLBACK_NOTARY_ALGORITHMS.get(jwk.getKeyType()));
        }
        return new NotaryAccess(algorithm, jwk);
    }

    @Override
    public Map<String, Profile> map() {
        return this.profilesById;
    }
}
