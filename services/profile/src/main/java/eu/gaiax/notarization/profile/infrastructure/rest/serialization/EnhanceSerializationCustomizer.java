/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.rest.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr353.JSR353Module;
import io.quarkus.jackson.ObjectMapperCustomizer;
import javax.ws.rs.ext.Provider;
import org.jose4j.jwk.PublicJsonWebKey;

/**
 *
 * @author Neil Crossley
 */
@Provider
public class EnhanceSerializationCustomizer implements ObjectMapperCustomizer {

    @Override
    public void customize(ObjectMapper objectMapper) {
        objectMapper.registerModule(new JSR353Module());
        var module = new SimpleModule();
        module.addSerializer(new PublicJsonWebKeySerializer());
        module.addDeserializer(PublicJsonWebKey.class, new PublicJsonWebKeyDeserializer());
        objectMapper.registerModule(module);
    }

}
