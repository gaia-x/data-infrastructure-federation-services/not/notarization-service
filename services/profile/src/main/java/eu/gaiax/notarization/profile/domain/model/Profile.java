/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.domain.model;

import eu.gaiax.notarization.profile.infrastructure.config.InvalidTaskTreeConfigurationException;
import java.time.Period;
import java.util.List;
import javax.json.JsonValue;
import javax.validation.constraints.NotNull;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
public record Profile(
        @NotNull String id,
        @NotNull AipVersion aip,
        @NotNull String name,
        @NotNull String description,
        @NotNull String encryption,
        @NotNull List<NotaryAccess> notaries,
        @NotNull Period validFor,
        @NotNull boolean isRevocable,
        @NotNull JsonValue template,
        String documentTemplate,
        @NotNull List<TaskDescription> taskDescriptions,
        @NotNull ProfileTaskTree tasks,
        @NotNull ProfileTaskTree preconditionTasks
    ) {

    public static Logger log = Logger.getLogger(Profile.class);

    public void assertTreesValid() throws InvalidTaskTreeConfigurationException {

        if(!tasks.structureValid())
            throw new InvalidTaskTreeConfigurationException("Tasks tree structure of profile \""+name+"\" not valid.");
        if(!preconditionTasks.structureValid())
            throw new InvalidTaskTreeConfigurationException("Preconditiontasks tree structure of profile \""+name+"\" not valid.");

        if(preconditionTasks.fulfilledByDefault()){
            log.warn(
                """
                The precondition tree defines tasks, but the tree defines at least one path which fulfills the tree,
                without performing any task (all tasks are optional).
                This basically says that there is a precondition which has to be fulfilled before any other action is allowed,
                but this condition is imediately fulfilled. (You MUST perform an OPTIONAL task)
                If this is intended, consider putting the tasks in the "normal" task tree, which has the same effect without possible confusion.
                """
                );
        }

        var missingName = tasks.allNames().stream()
                .filter(name ->
                    taskDescriptions.stream()
                    .map(desc -> desc.name())
                    .filter(descName -> descName.equals(name))
                    .findAny().isEmpty()
                ).findAny();
        if(missingName.isPresent()){
            throw new InvalidTaskTreeConfigurationException("The task \""+ missingName.get() +"\" of profile: \""+ name +"\" in tasks tree is missing in taskDescriptions.");
        }

        missingName = preconditionTasks.allNames().stream()
                .filter(name ->
                    taskDescriptions.stream()
                    .map(desc -> desc.name())
                    .filter(descName -> descName.equals(name))
                    .findAny().isEmpty()
                ).findAny();
        if(missingName.isPresent()){
            throw new InvalidTaskTreeConfigurationException("The task \""+ missingName.get() +"\" of profile: \""+ name +"\" in preconditiontasks tree is missing in taskDescriptions.");
        }

    }
}
