/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.config;

import eu.gaiax.notarization.profile.domain.model.NotaryAccess;
import eu.gaiax.notarization.profile.domain.model.Profile;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Neil Crossley
 */
public class ProfileWrappingEquality {

    public final Profile profile;

    public ProfileWrappingEquality(Profile profile) {
        this.profile = profile;
    }

    public static ProfileWrappingEquality comparable(Profile profile) {
        return new ProfileWrappingEquality(profile);
    }

    public static List<ProfileWrappingEquality> comparable(Collection<Profile> profile) {
        return profile.stream().map(ProfileWrappingEquality::comparable).toList();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ProfileWrappingEquality other) {
            return this.equalsProfile(other.profile);
        }
        if (!(obj instanceof Profile)) {
            return false;
        }
        Profile other = (Profile)obj;

        return equalsProfile(other);
    }

    private boolean equalsProfile(Profile other) {
        return Objects.equals(profile.id(), other.id()) &&
                Objects.equals(profile.name(), other.name()) &&
                Objects.equals(profile.description(), other.description()) &&
                Objects.equals(profile.template(), other.template()) &&
                Objects.equals(profile.validFor(), other.validFor()) &&
                equals(profile.notaries(), other.notaries());
    }

    private boolean equals(List<NotaryAccess> given, List<NotaryAccess> other) {
        return itemsMatch(given, other) && itemsMatch(other, given);
    }

    private boolean itemsMatch(List<NotaryAccess> left, List<NotaryAccess> right) {
        for (NotaryAccess outerAccess : left) {
            boolean hasMatch = false;
            var outerKey = outerAccess.key().toJson();
            for (NotaryAccess innerAccess : right) {
                if (Objects.equals(outerAccess.algorithm(), innerAccess.algorithm()) &&
                        Objects.equals(outerKey, innerAccess.key().toJson())) {
                    hasMatch = true;
                    break;
                }
            }
            if (!hasMatch) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = prime * result + ((profile.id() == null) ? 0 : profile.id().hashCode());
        result = prime * result + ((profile.description() == null) ? 0 : profile.description().hashCode());
        result = prime * result + ((profile.name()== null) ? 0 : profile.name().hashCode());
        result = prime * result + ((profile.template()== null) ? 0 : profile.template().hashCode());
        return result;
    }

}
