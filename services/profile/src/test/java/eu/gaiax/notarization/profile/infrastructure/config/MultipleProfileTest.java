/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.profile.domain.model.Profile;
import static eu.gaiax.notarization.profile.infrastructure.config.ProfileMatcher.equalsProfile;
import static eu.gaiax.notarization.profile.infrastructure.config.ProfileWrappingEquality.comparable;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import java.util.HashSet;
import java.util.function.IntFunction;
import javax.inject.Inject;
import org.hamcrest.Matcher;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Neil Crossley
 */
@QuarkusTest
@TestProfile(MultiProfileTestProfile.class)
public class MultipleProfileTest {

    private static final String PROFILE_VARIABLE = "profileId";
    private static final String PROFILE_SINGLE_PATH = "/api/v1/profiles/{profileId}";
    private static final String PROFILES_ALL_PATH = "/api/v1/profiles";

    @Inject
    ConfigBackedProfileService profileService;

    @Inject
    ObjectMapper objectMapper;

    @Test
    public void hasSingleProfile() {
        assertThat(profileService.list(), hasSize(MultiProfileTestProfile.values.size()));
    }

    @Test
    public void hasExpectedItems() {

        assertThat(profileService.list(), hasItems(
                MultiProfileTestProfile.values.stream()
                        .map(ProfileMatcher::equalsProfile)
                        .toArray(genericArray(Matcher[]::new))));
    }

    @SuppressWarnings("unchecked")
    static <T, R extends T> IntFunction<R[]> genericArray(IntFunction<T[]> arrayCreator) {
        return size -> (R[]) arrayCreator.apply(size);
    }

    @Test
    public void canFetchAllProfiles() throws JsonProcessingException {
        var expected = comparable(MultiProfileTestProfile.values);

        var resultResponse = given()
                .accept(ContentType.JSON)
                .when().get(PROFILES_ALL_PATH)
                .then()
                .statusCode(200)
                .body("", hasSize(expected.size()))
                .extract();

        var resultProfiles = objectMapper.readValue(resultResponse.asString(), new TypeReference<HashSet<Profile>>() { });
        // assertThat(comparable(resultProfiles), everyItem(isIn(expected)));

        assertThat(resultProfiles, hasItems(
                MultiProfileTestProfile.values.stream()
                        .map(ProfileMatcher::equalsProfile)
                        .toArray(genericArray(Matcher[]::new))));
    }

    @Test
    public void canFetchSingleProfile() throws JsonProcessingException {
        var expected = MultiProfileTestProfile.values.get(0);

        var resultResponse = given()
                .accept(ContentType.JSON)
                .pathParam(PROFILE_VARIABLE, expected.id())
                .when().get(PROFILE_SINGLE_PATH)
                .then()
                .statusCode(200)
                .extract();

        var resultProfile = objectMapper.readValue(resultResponse.asString(), Profile.class);
        assertThat(resultProfile, equalsProfile(expected));
    }
}
