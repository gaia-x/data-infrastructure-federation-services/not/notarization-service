/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.config;

import eu.gaiax.notarization.profile.domain.model.NotaryAccess;
import eu.gaiax.notarization.profile.domain.model.Profile;
import java.util.List;
import java.util.Objects;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 *
 * @author Neil Crossley
 */
public class ProfileMatcher extends TypeSafeMatcher<Profile> {

    public final Profile expected;

    public ProfileMatcher(Profile expected) {
        this.expected = expected;
    }

    @Override
    protected boolean matchesSafely(Profile item) {
        return Objects.equals(item.id(), expected.id())
                && Objects.equals(item.name(), expected.name())
                && Objects.equals(item.description(), expected.description())
                && Objects.equals(item.template(), expected.template())
                && Objects.equals(item.validFor(), expected.validFor())
                && equals(item.notaries(), expected.notaries());
    }

    private boolean equals(List<NotaryAccess> given, List<NotaryAccess> other) {
        return itemsMatch(given, other) && itemsMatch(other, given);
    }

    private boolean itemsMatch(List<NotaryAccess> left, List<NotaryAccess> right) {
        for (NotaryAccess outerAccess : left) {
            boolean hasMatch = false;
            var outerKey = outerAccess.key().toJson();
            for (NotaryAccess innerAccess : right) {
                if (Objects.equals(outerAccess.algorithm(), innerAccess.algorithm()) &&
                        Objects.equals(outerKey, innerAccess.key().toJson())) {
                    hasMatch = true;
                    break;
                }
            }
            if (!hasMatch) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue(this.expected);
    }

    public static ProfileMatcher equalsProfile(Profile profile) {
        return new ProfileMatcher(profile);
    }
}
