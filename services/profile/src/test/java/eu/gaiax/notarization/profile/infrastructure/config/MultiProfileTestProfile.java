/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.profile.infrastructure.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.profile.domain.model.NotaryAccess;
import eu.gaiax.notarization.profile.domain.model.Profile;
import eu.gaiax.notarization.profile.domain.model.ProfileTaskTree;
import eu.gaiax.notarization.profile.domain.model.AipVersion;
import eu.gaiax.notarization.profile.domain.model.TaskDescription;
import eu.gaiax.notarization.profile.domain.model.TaskType;
import io.quarkus.test.junit.QuarkusTestProfile;
import java.io.StringReader;
import java.time.Period;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.Map.entry;
import javax.json.Json;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.lang.JoseException;

/**
 *
 * @author Neil Crossley
 */
public class MultiProfileTestProfile implements QuarkusTestProfile {

    public static final List<Profile> values;
    private static final PublicJsonWebKey publicKey1_1;
    private static final PublicJsonWebKey publicKey2_1;

    static {
        try {
            publicKey1_1 = PublicJsonWebKey.Factory.newPublicJwk(
                    """
                    {"kty":"EC",
                     "crv":"P-256",
                     "x":"MKBCTNIcKUSDii11ySs3526iDZ8AiTo7Tu6KPAqv7D4",
                     "y":"4Etl6SRW2YiLUrN5vfvVHuhp7x8PxltmWWlbbM4IFyM",
                     "use":"enc",
                     "kid":"1"}
                    """);
            publicKey2_1 = PublicJsonWebKey.Factory.newPublicJwk(
                    """
                    {"kty":"EC",
                     "d":"gHltlw4V2JKep2v4oRziohrYvU3cvRQ50T3-6WPf0B3d4_2yZ-AGf6sghQVoCPj-",
                     "crv":"P-384",
                     "x":"I9_0yRjMwcRLAhNBwWLPmII2BJK6pIPTqr0LRy0WkoLJiyA846iLUWmLuvM3P8-w",
                     "y":"rPaBxsFyHKPs-h5Pkp6zQ0y5ygdIjvDOc9PTPDqxZnmyBZngFlPNkcl29SSqKzAs"}
                    """);
            values = List.of(
                    new Profile("firstProfile",
                            AipVersion.V2_0,
                            "First name",
                            "Some first description",
                            "A256GCM",
                            List.of(new NotaryAccess("RSA-OAEP-256", publicKey1_1)),
                            Period.ofYears(1),
                            true,
                            Json.createReader(
                                    new StringReader("""
                                             {"first object":"other", "first field": 1}
                                             """)).readObject(),
                            """
                            { "documents": ["1234"] }
                            """,
                            List.of(new TaskDescription("name", TaskType.BROWSER_IDENTIFICATION_TASK, "desc")),
                            new ProfileTaskTree(),
                            new ProfileTaskTree()
                    ),
                    new Profile("second Profile",
                            AipVersion.V2_0,
                            "Second name",
                            "Some second description",
                            "A256GCM",
                            List.of(new NotaryAccess("ECDH-ES+A256KW", publicKey2_1)),
                            Period.ofYears(1),
                            false,
                            Json.createReader(
                                    new StringReader("""
                                                     {"second object":"other", "second field": 1}
                                                     """)).readObject(),
                            null,
                            List.of(new TaskDescription("name", TaskType.BROWSER_IDENTIFICATION_TASK, "desc")),
                            new ProfileTaskTree(),
                            new ProfileTaskTree()
                    ),
                    new Profile("third Profile",
                            AipVersion.V1_0,
                            "Third name",
                            "Some third description",
                            "A256GCM",
                            List.of(new NotaryAccess("ECDH-ES+A256KW", publicKey2_1)),
                            Period.ofDays(6),
                            true,
                            Json.createReader(
                                    new StringReader("""
                                                     {"third object":"other", "third field": 1}
                                                     """)).readObject(),
                            """
                            { "documents": ["654", 1234"] }
                            """,
                            List.of(),
                            new ProfileTaskTree(),
                            new ProfileTaskTree()
                    ),
                    new Profile("fourth-Profile",
                            AipVersion.V2_0,
                            "Fourth name",
                            "Some fourth description",
                            "A256GCM",
                            List.of(new NotaryAccess("ECDH-ES+A256KW", publicKey2_1)),
                            Period.ofYears(1),
                            false,
                            Json.createReader(
                                    new StringReader("""
                                                     {"fourth object": "other", "fourth field": 1}
                                                     """)).readObject(),
                            null,
                            List.of(),
                            new ProfileTaskTree(),
                            new ProfileTaskTree()
                    )
            );
        } catch (JoseException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static ObjectMapper mapper = new ObjectMapper();

    @Override
    public Map<String, String> getConfigOverrides() {

        var overrides = new HashMap<String, String>();
        for (int index = 0; index < values.size(); index++) {
            try {
                var profile = values.get(index);
                var prefix = "gaia-x.profile.config[" + index + "].";

                overrides.putAll(Map.ofEntries(
                    entry(prefix + "id", profile.id()),
                    entry(prefix + "name", profile.name()),
                    entry(prefix + "aip", profile.aip().toString()),
                    entry(prefix + "description", profile.description()),
                    entry(prefix + "valid-for", profile.validFor().toString()),
                    entry(prefix + "is-revocable", Boolean.toString(profile.isRevocable())),
                    entry(prefix + "template", profile.template().toString()),
                    entry(prefix + "tasks", mapper.writeValueAsString(profile.tasks())),
                    entry(prefix + "precondition-tasks", mapper.writeValueAsString(profile.preconditionTasks())),
                    entry(prefix + "task-descriptions", mapper.writeValueAsString(profile.taskDescriptions()))
                ));
                if (profile.documentTemplate() != null) {
                    overrides.put(prefix + "document-template", profile.template().toString());
                }
                int keyIndex = 0;
                for (NotaryAccess notaryKey : profile.notaries()) {
                    overrides.put(prefix + "notaries[" + keyIndex + "].algorithm", notaryKey.algorithm());
                    overrides.put(prefix + "notaries[" + keyIndex + "].jwk", notaryKey.key().toJson());
                    keyIndex++;
                }
            } catch (JsonProcessingException ex) {
                throw new RuntimeException(ex);
            }
        }
        return overrides;
    }
}
