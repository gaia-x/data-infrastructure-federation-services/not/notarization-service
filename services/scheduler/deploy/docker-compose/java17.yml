version: "3"
services:

  # Jaeger
  jaeger-all-in-one:
    image: jaegertracing/all-in-one:1.34.1
    container_name: jaegertracing
    ports:
      - "16686:16686"
      - "14268"
      - "14250"

  # OpenTelemetry Collector
  otel-collector:
    image: otel/opentelemetry-collector:0.50.0
    container_name: opentelemetry-collector
    command: ["--config=/etc/otel-collector-config.yaml"]
    volumes:
      - ../../../deploy/resources/config/otel-collector-config.yaml:/etc/otel-collector-config.yaml
    ports:
      - "13133:13133"  # Health_check extension
      - "4317:4317"    # OTLP gRPC receiver
      - "55680:55680"  # OTLP gRPC receiver alternative port
    depends_on:
      - jaeger-all-in-one

  scheduler-db:
    image: postgres:14
    container_name: scheduler-db
    ports:
      - "5432"
    environment:
      POSTGRES_USER: scheduler
      POSTGRES_PASSWORD: scheduler
      POSTGRES_DB: scheduler_database
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U scheduler -d scheduler_database"]
      interval: 5s
      timeout: 5s
      retries: 5

  scheduler-flyway:
    image: flyway/flyway:8.5
    container_name: scheduler-flyway
    command: "-locations=filesystem:/sql-migrations -url=jdbc:postgresql://scheduler-db:5432/scheduler_database -schemas=public -user=scheduler -password=scheduler -connectRetries=60 -baselineOnMigrate=true -table=flyway_quarkus_history migrate"
    volumes:
      - ../../../services/scheduler/src/main/jib/db-flyway/:/sql-migrations
    depends_on:
      scheduler-db:
        condition: service_healthy

  scheduler-java17:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/scheduler:java17-latest
    container_name: scheduler-java17
    depends_on:
      request-processing-java17:
        condition: service_healthy
      scheduler-db:
        condition: service_healthy
      profile-java17:
        condition: service_healthy
    ports:
      - "8087:8087"
    restart: on-failure
    environment:
      "QUARKUS_HTTP_PORT": 8087
      "QUARKUS_HTTP_ACCESS_LOG_ENABLED": "true"
      QUARKUS_OPENTELEMETRY_TRACER_EXPORTER_OTLP_ENDPOINT: http://otel-collector:4317
      QUARKUS_REST_CLIENT_REQUESTPROCESSING_API_URL: http://request-processing:8084
      QUARKUS_REST_CLIENT_REVOCATION_API_URL: http://revocation:8086
      QUARKUS_REST_CLIENT_PROFILE_API_URL: http://profile:8083
      QUARKUS_QUARTZ_CLUSTERED: "true"
      QUARKUS_QUARTZ_STORE_TYPE: jdbc-cmt
      # Datasource configuration.
      QUARKUS_DATASOURCE_DB_KIND: postgresql
      QUARKUS_DATASOURCE_USERNAME: scheduler
      QUARKUS_DATASOURCE_PASSWORD: scheduler
      QUARKUS_DATASOURCE_JDBC_URL: jdbc:postgresql://scheduler-db:5432/scheduler_database
      # Hibernate configuration
      QUARKUS_HIBERNATE_ORM_DATABASE_GENERATION: none
      QUARKUS_HIBERNATE_ORM_LOG_SQL: "true"
      QUARKUS_HIBERNATE_ORM_SQL_LOAD_SCRIPT: no-file
      # flyway configuration
      QUARKUS_FLYWAY_CONNECT_RETRIES: 10
      QUARKUS_FLYWAY_TABLE: flyway_quarkus_history
      QUARKUS_FLYWAY_MIGRATE_AT_START: "true"
      QUARKUS_FLYWAY_BASELINE_ON_MIGRATE: "true"
      QUARKUS_FLYWAY_BASELINE_VERSION: "1_0"
      QUARKUS_FLYWAY_BASELINE_DESCRIPTION: Quartz
      # quarz cron style (Seconds Minutes Hours 'Day Of Month' Month 'Day Of Week' Year)
      CRON_PRUNE_TERMINATED: "*/10 * * ? * * *"
      CRON_PRUNE_TIMEOUT: "*/5 * * ? * * *"
      CRON_PRUNE_SUBMISSION_TIMEOUT: "*/2 * * ? * * *"
      CRON_ISSUE_REVOCATION_CREDENTIALS: "0 * * ? * * *"
      CRON_PROFILE_REQUEST_OUTSTANDING_DIDS: "0 * * ? * * *"
    networks:
      default:
        aliases:
          - scheduler
    deploy:
      resources:
        limits:
          memory: 256M
          cpus: '0.25'
        reservations:
          memory: 128M
          cpus: '0.10'

