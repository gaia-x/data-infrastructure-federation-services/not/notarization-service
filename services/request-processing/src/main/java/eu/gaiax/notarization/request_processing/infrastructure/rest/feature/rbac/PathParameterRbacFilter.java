/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.feature.rbac;

import eu.gaiax.notarization.request_processing.domain.exception.AuthorizationException;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import io.micrometer.core.instrument.MeterRegistry;
import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
public class PathParameterRbacFilter implements ContainerRequestFilter {

    private static final Logger logger = Logger.getLogger(PathParameterRbacFilter.class);

    final NotarizationRequestAction action;
    final String pathParameterName;
    final MeterRegistry registry;

    public PathParameterRbacFilter(
            NotarizationRequestAction action,
            String pathParameterName,
            MeterRegistry registry) {
        this.action = action;
        this.pathParameterName = pathParameterName;
        this.registry = registry;
    }

    @Override
    public void filter(ContainerRequestContext crc) throws IOException {
        var securityContext = crc.getSecurityContext();

        var user = securityContext.getUserPrincipal();
        var pathValue = crc.getUriInfo().getPathParameters().getFirst(pathParameterName);
        if (user == null) {
            throw new AuthorizationException("Missing authentication", new SessionId(pathValue));
        }
        if (pathValue == null) {
            throw new AuthorizationException("Missing value", new SessionId(pathValue));
        }

        if (!securityContext.isUserInRole(pathValue)) {
            this.registry.counter("rbac.authentication.failure", "action", this.action.name(), "role", pathValue).increment();
            throw new AuthorizationException("Not authorized", new SessionId(pathValue));
        }
        this.registry.counter("rbac.authentication.success", "action", this.action.name(), "role", pathValue).increment();
    }

}
