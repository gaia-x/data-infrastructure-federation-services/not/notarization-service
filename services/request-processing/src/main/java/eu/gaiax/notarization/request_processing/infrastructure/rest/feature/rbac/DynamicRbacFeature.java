/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.feature.rbac;

import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.infrastructure.rest.Api;
import eu.gaiax.notarization.request_processing.infrastructure.rest.feature.audit.Auditable;
import io.micrometer.core.instrument.MeterRegistry;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Neil Crossley
 */
@Provider
public class DynamicRbacFeature implements DynamicFeature {

    private static final Set<NotarizationRequestAction> roleSpecificNotaryAction;

    static {
        roleSpecificNotaryAction = new HashSet<>(NotarizationRequestAction.NotaryActions);
        roleSpecificNotaryAction.remove(NotarizationRequestAction.NOTARY_FETCH_ALL);
        roleSpecificNotaryAction.remove(NotarizationRequestAction.NOTARY_REVOKE);
    }

    @Inject
    MeterRegistry registry;

    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        if (resourceInfo.getResourceMethod().isAnnotationPresent(Auditable.class)) {
            var annotation = resourceInfo.getResourceMethod().getAnnotation(Auditable.class);

            var action = annotation.action();
            if (roleSpecificNotaryAction.contains(action)) {
                context.register(
                        new PathParameterRbacFilter(
                                action,
                                Api.Param.PROFILE_ID,
                                registry),
                        Priorities.AUTHORIZATION);
            }
        }
    }
}
