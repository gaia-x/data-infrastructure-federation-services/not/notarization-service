/*
 *
 */
package eu.gaiax.notarization.request_processing.infrastructure.stringtemplate;

import io.vertx.core.json.JsonObject;

/**
 *
 * @author Neil Crossley
 */
public record JsonObjectSTWrapper(JsonObject json) {

    public static JsonObjectSTWrapper from(JsonObject json) {
        return json == null ? null : new JsonObjectSTWrapper(json);
    }
}
