/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.model;

import java.time.Period;
import java.util.List;
import javax.json.JsonValue;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author Neil Crossley
 */
@Schema(description = "A notarization profile specifies a single notarization submission process and the resulting credential type.")
public record Profile(
        @Schema(required = true, description = "The unique identifier of the profile.")
        String id,
        @Schema(required = true)
        AipVersion aip,
        @Schema(required = true, description = "A human readable name of the profile")
        String name,
        @Schema(required = true, description = "A human readable description of the profile")
        String description,
        @Schema(required = false, description = "This value specifies the fallback encryption algorithm if not defined in a NotaryAccess.")
        String encryption,
        @Schema(required = true)
        List<NotaryAccess> notaries,
        @Schema(required = true)
        Period validFor,
        @Schema(required = true)
        boolean isRevocable,
        @Schema(required = true)
        JsonValue template,
        @Schema(description = "A template that is applied to the evidence documents. The result is merged into the issued credential. The specification is StringTemplate (https://www.stringtemplate.org/).")
        String documentTemplate,
        @Schema(required = true,
                ref = "#/components/schemas/TaskTreeNode",
                example = """
                            { "anyOf": [
                                { "taskName": "signedUpload" },
                                { }
                            ] }
                          """)
        ProfileTaskTree tasks,
        @Schema(required = true,
                ref = "#/components/schemas/TaskTreeNode",
                example = """
                            { "allOf": [
                                { "taskName": "eID" },
                                { "taskName": "signedFormUpload" }
                            ] }
                          """)
        ProfileTaskTree preconditionTasks,
        List<TaskDescription> taskDescriptions
    ) {

}
