/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.feature.audit;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.UUID;

import javax.enterprise.inject.Instance;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.UriInfo;

import org.jboss.logging.Logger;

import eu.gaiax.notarization.request_processing.domain.model.Channel;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import eu.gaiax.notarization.request_processing.domain.model.OnAuditableHttp;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import eu.gaiax.notarization.request_processing.infrastructure.rest.Api;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.mutiny.core.eventbus.EventBus;

/**
 *
 * @author Neil Crossley
 */
public class RequestorAuditingFilter implements ContainerResponseFilter, ContainerRequestFilter {

    private static final Logger logger = Logger.getLogger(RequestorAuditingFilter.class);

    private static final String CACHE_REQUEST = "AUDIT_CACHE_REQUEST";
    private static final String RECEIVED_AT_TIME = "AUDIT_RECEIVED_REQUEST";
    private static final String TASK_NAME = "eu.gaiax.notarization.request_processing.RequestorAuditingFilter.TASK_NAME";

    private final NotarizationRequestAction action;
    private final Instance<EventBus> bus;
    private final Instance<HttpServerRequest> request;
    private final boolean auditRequestContent;

    public RequestorAuditingFilter(NotarizationRequestAction action, Instance<EventBus> bus, Instance<HttpServerRequest> request) {
        this.action = action;
        this.bus = bus;
        this.request = request;
        this.auditRequestContent = !NotarizationRequestAction.NonAuditableContent.contains(action);
    }

    @Override
    public void filter(ContainerRequestContext crc) throws IOException {
        crc.setProperty(RECEIVED_AT_TIME, OffsetDateTime.now());

        var resolvedRequest = request.get();
        try {
            if (auditRequestContent) {
                resolvedRequest.bodyHandler(buffer -> {
                    crc.setProperty(CACHE_REQUEST, buffer);
                });
            } else {
                // HACK: force the loading/initialization of the proxied instance.
                resolvedRequest.absoluteURI();
            }
        } finally {
            request.destroy(resolvedRequest);
        }
    }

    public static void storeTaskName(HttpServerRequest request, String taskName) {
        request.params().add(TASK_NAME, taskName);
    }

    private String getStoredTaskName(NotarizationRequestAction action){
        if(action == NotarizationRequestAction.TASK_START || action == NotarizationRequestAction.TASK_CANCEL){
            return request.get().getParam(TASK_NAME);
        } else {
            return null;
        }
    }

    @Override
    public void filter(ContainerRequestContext crc, ContainerResponseContext crc1) throws IOException {
        final UriInfo uriInfo = crc.getUriInfo();

        var pathParameters = uriInfo.getPathParameters();

        var resolvedRequest = request.get();

        try {
            var ipAddress = resolvedRequest.remoteAddress().hostAddress();

            var body = (Buffer)crc.getProperty(CACHE_REQUEST);
            String sessionId = pathParameters.getFirst(Api.Param.SESSION);
            if (sessionId == null) {
                sessionId = crc1.getHeaderString(Api.Param.SESSION);
                if (sessionId != null) {
                    crc1.getHeaders().remove(Api.Param.SESSION);
                }
            }

            var event = new OnAuditableHttp(
                    UUID.randomUUID(),
                    uriInfo.getRequestUri(),
                    asSessionId(sessionId),
                    asRequestId(pathParameters.getFirst(Api.Param.NOTARIZATION_REQUEST_ID)),
                    action,
                    ipAddress,
                    (OffsetDateTime)crc.getProperty(RECEIVED_AT_TIME),
                    (body != null ? body.toString() : null),
                    crc1.getStatus(),
                    null,
                    getStoredTaskName(action)
            );

            notify(event);

        } finally {
            request.destroy(resolvedRequest);
        }
    }

    private void notify(OnAuditableHttp event) {
        var resolvedBus = this.bus.get();
        try {
            resolvedBus.requestAndForget(Channel.ON_AUDITABLE,  event);
        } finally {
            bus.destroy(resolvedBus);
        }
    }

    private static SessionId asSessionId(String value) {
        return value == null ? null : new SessionId(value);
    }
    private static NotarizationRequestId asRequestId(String value) {
        return value == null ? null : new NotarizationRequestId(value);
    }
}
