/*
 *
 */
package eu.gaiax.notarization.request_processing.infrastructure.stringtemplate;

import java.util.Locale;
import org.stringtemplate.v4.AttributeRenderer;

/**
 *
 * @author Neil Crossley
 */
public class JsonObjectSTWrapperRenderer implements AttributeRenderer<JsonObjectSTWrapper> {

    @Override
    public String toString(JsonObjectSTWrapper wrapper, String formatString, Locale locale) {
        var model = wrapper.json();
        return model.encode();
    }

}
