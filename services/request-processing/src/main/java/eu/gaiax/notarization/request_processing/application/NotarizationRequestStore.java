/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application;

import java.io.StringReader;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Objects;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceException;

import com.apicatalog.jsonld.JsonLd;
import com.apicatalog.jsonld.JsonLdError;
import com.apicatalog.jsonld.JsonLdOptions;
import com.apicatalog.jsonld.document.JsonDocument;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.hibernate.HibernateException;
import org.jboss.logging.Logger;

import eu.gaiax.notarization.request_processing.application.domain.SessionInfo;
import eu.gaiax.notarization.request_processing.application.taskprocessing.TaskManager;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.RequestorIdentity;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.exception.*;
import eu.gaiax.notarization.request_processing.domain.model.AccessToken;
import eu.gaiax.notarization.request_processing.domain.model.DistributedIdentity;
import eu.gaiax.notarization.request_processing.domain.model.IssuanceResponse;
import eu.gaiax.notarization.request_processing.domain.model.MarkReadyResponse;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.NotaryAccess;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskId;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskInstance;
import eu.gaiax.notarization.request_processing.domain.services.IssuanceService;
import eu.gaiax.notarization.request_processing.domain.services.ProfileService;
import eu.gaiax.notarization.request_processing.domain.services.RequestNotificationService;
import eu.gaiax.notarization.request_processing.infrastructure.rest.Api;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.NotarizationRequestView;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.SessionSummary;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.SubmitNotarizationRequest;
import eu.gaiax.notarization.request_processing.infrastructure.rest.feature.audit.CallbackAuditingFilter;
import eu.gaiax.notarization.request_processing.infrastructure.rest.feature.audit.RequestorAuditingFilter;
import io.micrometer.core.instrument.MeterRegistry;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.quarkus.runtime.StartupEvent;
import io.smallrye.mutiny.Uni;
import io.vertx.pgclient.PgException;
import java.net.URI;
import java.time.OffsetDateTime;
import java.time.Period;
import java.util.Set;
import java.util.UUID;
import javax.ws.rs.core.UriBuilder;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jose4j.lang.JoseException;
import io.vertx.core.http.HttpServerRequest;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class NotarizationRequestStore {

    private final Logger logger;
    private SecureRandom secureRandom;
    private final MeterRegistry registry;
    private final ObjectMapper objectMapper;
    private final ProfileService profileService;
    private final RequestNotificationService notifications;
    private final TaskManager taskManager;
    private final IssuanceService issuanceService;
    private final HttpServerRequest request;
    private JsonLdOptions options;

    public NotarizationRequestStore(
        Logger logger,
        MeterRegistry registry,
        ObjectMapper objectMapper,
        ProfileService profileService,
        RequestNotificationService notifications,
        IssuanceService issuanceService,
        HttpServerRequest request,
        TaskManager taskManager
    ) {
        this.logger = logger;
        this.registry = registry;
        this.objectMapper = objectMapper;
        this.profileService = profileService;
        this.notifications = notifications;
        this.taskManager = taskManager;
        this.issuanceService = issuanceService;
        this.request = request;
    }

    void onStartup(@Observes StartupEvent ev) {
        /*
         * Native builds optimise the application by initializing all instances at build time.
         *
         * However, SecureRandom will only correctly work in containers if initalized at runtime, using the relevant resources of the host machine.
         */
        this.secureRandom = new SecureRandom();

        /*
         * SchemeRouter.INSTANCE initializes both new Threads and an instance of SecureRandom.
         */
        this.options = new JsonLdOptions();
    }

    private void checkAccessToken(Session dbSession, SessionInfo sessionInfo) throws AuthorizationException {
        if (!Objects.equals(dbSession.accessToken, sessionInfo.token().token)) {
            throw new AuthorizationException("Not authorized", sessionInfo.id());
        }
    }

    @ReactiveTransactional
    public Uni<Session> createNewSession(ProfileId profileId) {
        return this.profileService.find(profileId)
            .onItem().ifNull().failWith(() -> new InvalidProfileException(profileId))
            .onItem().ifNotNull().transformToUni(profile -> {
                return Session.createNew(
                    new SessionId(urlSafeString(secureRandom, new byte[32])),
                    new AccessToken(urlSafeString(secureRandom, new byte[48])),
                    profile
                );
            });
    }

    @ReactiveTransactional
    public Uni<SessionSummary> fetchSession(SessionInfo sessionInfo) {
        return
            Session.<Session>findById(sessionInfo.id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .onItem().ifNotNull()
            .call(s -> Mutiny.fetch(s.tasks))
            .onItem().transformToUni(dbSession -> {
                checkAccessToken(dbSession, sessionInfo);

                return this.profileService.find(new ProfileId(dbSession.profileId))
                    .onItem().ifNotNull()
                    .transform(p -> {

                        SessionSummary summary = SessionSummary.asSummary(dbSession, p);
                        return summary;
                    });
            });
    }

    @ReactiveTransactional
    public Uni<TaskInstance> startTask(SessionInfo sessionInfo, TaskId taskId, JsonNode data) {
        return Session.<Session>findById(sessionInfo.id().id).onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .call(s -> Mutiny.fetch(s.tasks))
            .chain(dbSession -> {
                checkAccessToken(dbSession, sessionInfo);
                if (!dbSession.state.isTaskProcessingAllowed()) {
                    throw new InvalidRequestStateException(sessionInfo.id(), NotarizationRequestState.taskProcessingStates, dbSession.state);
                }
                var sessionTask = dbSession.tasks.stream()
                    .filter(t -> t.taskId.equals(taskId.id()))
                    .findFirst().orElseThrow(()->new NotFoundException("Task"));

                    RequestorAuditingFilter.storeTaskName(request, sessionTask.name);

                return this.profileService.find(new ProfileId(dbSession.profileId))
                    .onItem().ifNotNull()
                    .transformToUni(p -> {

                        //precondition is not fulfilled and asked task is none of precondition -> leads to error
                        if(
                           !p.preconditionTasks().treeFulfilledBySession(dbSession)
                           &&
                           !p.preconditionTasks().containsTaskByName(sessionTask.name)
                        ){
                            throw new InvalidTaskStateException("Task state not valid");
                        }

                        return taskManager.startTask(taskId, data)
                                .invoke(task -> this.notifications.onExternalTask(sessionInfo.id(), task.uri));
                    });
            });
    }

    @ReactiveTransactional
    public Uni<Void> cancelTask(SessionInfo sessionInfo, TaskId taskId) {

        return Session.<Session>findById(sessionInfo.id().id).onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .call(s -> Mutiny.fetch(s.tasks))
            .chain(dbSession -> {
                checkAccessToken(dbSession, sessionInfo);
                if (!dbSession.state.isTaskProcessingAllowed()) {
                    throw new InvalidRequestStateException(sessionInfo.id(), NotarizationRequestState.taskProcessingStates, dbSession.state);
                }
                var sessionTask = dbSession.tasks.stream()
                    .filter(t -> t.taskId.equals(taskId.id()))
                    .findFirst().orElseThrow(()->new NotFoundException("Task"));

                RequestorAuditingFilter.storeTaskName(request, sessionTask.name);

                return taskManager.cancelTask(taskId);

            });
    }

    @ReactiveTransactional
    public Uni<Void> assignIdentity(SessionId sessionId, String body) {

        return Session.<Session>findById(sessionId.id, LockModeType.NONE)
                .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
                .onItem().invoke(()->{
                    if(body.isEmpty()){
                        throw new BadParameterException("Identity data was empty.");
                    }
                })
                .onItem().ifNotNull().transformToUni(dbSession -> {
                    var profileId = new ProfileId(dbSession.profileId);
                    return this.profileService.find(profileId)
                            .onItem().ifNull().failWith(() -> new InvalidProfileException(profileId))
                            .onItem().ifNotNull().call(foundProfile -> {
                                return Mutiny.fetch(dbSession.identities).invoke(() -> {
                                    for (NotaryAccess notary : foundProfile.notaries()) {
                                        try {
                                            RequestorIdentity requestor = new RequestorIdentity();
                                            requestor.data = notary.encrypt(foundProfile.encryption(), body);
                                            requestor.session=dbSession;
                                            requestor.id = UUID.randomUUID();
                                            requestor.jwk = notary.key().toJson();
                                            requestor.encryption = foundProfile.encryption();
                                            requestor.algorithm = notary.algorithm();
                                            dbSession.identities.add(requestor);
                                        } catch (JoseException ex) {
                                            logger.error("Could not encrypt the identity", ex);
                                        }
                                    }
                                });
                            }).eventually(() -> {
                                return dbSession.persist();
                            }).onItem().invoke(() -> {
                                this.registry.counter("request.identity.success").increment();
                            }).replaceWithVoid();
                });
    }

    @ReactiveTransactional
    public Uni<Void> failIdentityAssignment(SessionId sessionId) {
        return Session.<Session>findById(sessionId.id, LockModeType.NONE)
                .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
                .onItem().ifNotNull().invoke( () -> {
                    this.registry.counter("request.identity.failure").increment();
                }).replaceWithVoid();
    }

    @ReactiveTransactional
    public Uni<Void> softDeleteSession(SessionInfo session) {
        return Session.<Session>findById(session.id().id, LockModeType.NONE)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(sess -> {
                checkAccessToken(sess, session);
                if (!sess.state.isRevokeable()) {
                    throw new InvalidRequestStateException(session.id(), NotarizationRequestState.revokeableStates, sess.state);
                }

                sess.state = NotarizationRequestState.TERMINATED;
                return sess.cleanup(taskManager, notifications);
            });
    }

    @ConfigProperty(name = "notarization-processing.terminated.session.retention.period")
    Period retentionPeriod;

    @ReactiveTransactional
    public Uni<Void> pruneTerminatedSessions() {
        return Session.delete(
            "state in ?1 and lastModified < ?2",
            Set.of(NotarizationRequestState.TERMINATED, NotarizationRequestState.ISSUED),
            OffsetDateTime.now().minus(retentionPeriod)
        )
       .replaceWithVoid();
    }

    @ConfigProperty(name = "notarization-processing.session.timeout.period")
    Period timeoutPeriod;

    @ReactiveTransactional
    public Uni<Void> pruneTimeoutSessions() {
        return Uni.combine().all().unis(Session.<Session>find(
                    "state in ?1 and lastModified < ?2",
                    NotarizationRequestState.statesAffectedBySessionTimeout,
                    OffsetDateTime.now().minus(timeoutPeriod)
                )
                .stream().onItem().transformToUniAndConcatenate(sess -> {
                    sess.state = NotarizationRequestState.TERMINATED;
                    return sess.cleanup(taskManager, notifications);
                }).collect().asList()
            ).discardItems();
    }

    @ConfigProperty(name = "notarization-processing.session.submission.timeout.period")
    Period submissionTimeoutPeriod;

    public Uni<Void> pruneSubmissionTimeoutSessions() {
        return Uni.combine().all().unis(Session.<Session>find(
                    "state in ?1 and lastModified < ?2",
                    NotarizationRequestState.statesAffectedBySubmissionTimeout,
                    OffsetDateTime.now().minus(submissionTimeoutPeriod)
                )
                .stream().onItem().transformToUniAndConcatenate(sess -> {
                    sess.state = NotarizationRequestState.TERMINATED;
                    return sess.cleanup(taskManager, notifications);
                }).collect().asList()
            ).discardItems();
    }

    @ReactiveTransactional
    public Uni<Void> updateRequest(SessionInfo sessionInfo, JsonNode data) {
        return Session.<Session>findById(sessionInfo.id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(session -> {
                checkAccessToken(session, sessionInfo);
                if (!session.state.isUpdateable()) {
                    throw new InvalidRequestStateException(sessionInfo.id(), NotarizationRequestState.updateableStates, session.state);
                }
                final var rawData = data.toString();
                assertValid(rawData);
                session.request.data = rawData;
                return session.request.persistAndFlush().replaceWithVoid();
            });

    }

    @ReactiveTransactional
    public Uni<NotarizationRequest> submitNewRequest(NotarizationRequestId id,
        SessionInfo sessionInfo,
        SubmitNotarizationRequest submissionRequest) {
        return Session.<Session>findById(sessionInfo.id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(session -> {
                checkAccessToken(session, sessionInfo);
                if (session.state != NotarizationRequestState.SUBMITTABLE) {
                    throw new InvalidRequestStateException(
                        new SessionId(session.id),
                        NotarizationRequestState.SUBMITTABLE,
                        session.state);
                }
                var rawData = submissionRequest.data.toString();
                assertValid(rawData);
                var newRequest = new NotarizationRequest();
                newRequest.id = id.id;
                newRequest.did = DistributedIdentity.valueOf(submissionRequest.holder);
                newRequest.requestorInvitationUrl = submissionRequest.invitation;
                newRequest.session = session;
                newRequest.data = rawData;
                session.state = NotarizationRequestState.EDITABLE;
                return session.persist().chain(() -> newRequest.<NotarizationRequest>persistAndFlush());
            }).onFailure(PersistenceException.class).transform(ex -> {
            var cause = ex.getCause();
            if (cause instanceof HibernateException) {
                cause = cause.getCause();
                if (cause instanceof PgException) {
                    var code = ((PgException) cause).getCode();
                    if ("23505".equals(code)) {
                        return new CannotReuseTokenException("Cannot reuse token");
                    }
                }
            }
            this.logger.error("Did not handle PersistenceException", ex);
            return ex;
        });
    }

    private void assertValid(String data) {
        JsonDocument document;
        try {
            document = JsonDocument.of(new StringReader(data));
        } catch (JsonLdError ex) {
            throw new InvalidJsonLdException("Provided data is invalid", ex);
        }

        try {
            JsonLd.flatten(document).options(options).get();
        } catch (JsonLdError ex) {
            throw new InvalidJsonLdException("Provided data is invalid", ex);
        }
    }

    public static String urlSafeString(SecureRandom secureRandom, byte[] tokenBuffer) {
        secureRandom.nextBytes(tokenBuffer);
        var accessToken = Base64.getUrlEncoder().encodeToString(tokenBuffer);
        return accessToken;
    }

    @ReactiveTransactional
    public Uni<MarkReadyResponse> markReady(SessionInfo sessionInfo, Boolean manualRelease) {
        return NotarizationRequest.findWithDocuments(sessionInfo.id())
                .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
                .chain(request -> {
                    var session = request.session;
                    checkAccessToken(session, sessionInfo);
                    if (!session.state.isMarkReadyAllowed()) {
                        throw new InvalidRequestStateException(sessionInfo.id(), NotarizationRequestState.markReadyStates, session.state);
                    }
                        return
                            this.profileService.find(new ProfileId(session.profileId))
                            .call((ign) -> Mutiny.fetch(session.tasks))
                            .onItem().ifNotNull()
                            .transformToUni(p -> {
                                    if(
                                       p.preconditionTasks().treeFulfilledBySession(session)
                                       &&
                                       p.tasks().treeFulfilledBySession(session)
                                    ){
                                        session.state = NotarizationRequestState.READY_FOR_REVIEW;
                                        session.manualRelease = false;
                                        URI releaseUrl = null;
                                        if(manualRelease == true){
                                            session.manualRelease = true;
                                            var noncedUri = buildManualReleaseUrl();
                                            session.manualReleaseToken = noncedUri.nonce;
                                            releaseUrl = noncedUri.uri;
                                        }
                                        return session.persistAndFlush()
                                            .invoke(s -> this.notifications.onReadyForReview(new NotarizationRequestId(request.id), new ProfileId(session.profileId)))
                                            .replaceWith(new MarkReadyResponse(releaseUrl));
                                    } else {
                                        throw new InvalidTaskStateException("Tasks not fulfilled.");
                                    }

                            });
                });
    }

    private String createNonce(){
        return urlSafeString(secureRandom, new byte[64]);
    }
    public record NoncedUri(URI uri, String nonce){}

    @ConfigProperty(name = "notarization-processing.internal-url")
    URI internalUrl;
    private NoncedUri buildManualReleaseUrl() {
        var nonce = createNonce();
        return new NoncedUri(
            UriBuilder.fromUri(internalUrl)
                .path(Api.Path.SESSION_RESOURCE)
                .path(Api.Path.ISSUE_MANUALY)
                .path(nonce)
                .build(),
            nonce
        );
    }

    @ReactiveTransactional
    public Uni<Void> markUnready(SessionInfo sessionInfo) {
        return Session.<Session>findById(sessionInfo.id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(session -> {
            checkAccessToken(session, sessionInfo);
                if (!session.state.isMarkUnreadyAllowed()) {
                    throw new InvalidRequestStateException(sessionInfo.id(), NotarizationRequestState.markUnreadyStates, session.state);
                }
                session.state = NotarizationRequestState.EDITABLE;
                return session.persistAndFlush().replaceWithVoid();
            });

    }

    @ReactiveTransactional
    public Uni<NotarizationRequestView> fetchNotarizationRequest(SessionInfo sessionInfo) {
        return Session.<Session>findById(sessionInfo.id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(session -> {
            checkAccessToken(session, sessionInfo);

                return NotarizationRequest.findWithDocuments(sessionInfo.id())
                    .map(res -> NotarizationRequestView.from(res, objectMapper));
            });
    }

    @ReactiveTransactional
    public Uni<Void> assignDidHolder(SessionInfo sessionInfo, String didHolder, String invitation) {
        return Session.<Session>findById(sessionInfo.id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .chain(session -> {
                checkAccessToken(session, sessionInfo);
                    var profileId = new ProfileId(session.profileId);
                    return this.profileService.find(profileId)
                            .onItem().ifNull().failWith(() -> new InvalidProfileException(profileId))
                            .onItem().ifNotNull().transformToUni(profile -> {
                                if (!session.state.isAssignDidAllowed() || (session.request.hasHolderAccess(profile) && session.state == NotarizationRequestState.ACCEPTED)) {
                                    throw new InvalidRequestStateException(sessionInfo.id(), NotarizationRequestState.assignDidStates, session.state);
                                }
                                if (didHolder != null) {
                                    session.request.did = didHolder;
                                }
                                if (invitation != null) {
                                    session.request.requestorInvitationUrl = invitation;
                                }
                                if (session.state == NotarizationRequestState.PENDING_DID
                                        && session.request.hasHolderAccess(profile)) {
                                    if (session.manualRelease != null && session.manualRelease) {
                                        session.state = NotarizationRequestState.PENDING_RQUESTOR_RELEASE;
                                    } else {
                                        session.state = NotarizationRequestState.ACCEPTED;
                                    }
                                }
                                return session.request.persist()
                                        .chain(r -> session.<Session>persistAndFlush())
                                        .chain(s -> this.handlePostAcceptance(s.request, profile)).replaceWithVoid();
                            });

                });
    }

    private Uni<Void> handlePostAcceptance(NotarizationRequest request, Profile profile) {
        if (request.session.state == NotarizationRequestState.ACCEPTED) {
            return this.issuanceService.issue(request, profile)
                .chain((IssuanceResponse resp) -> {
                    var invitationUrl = resp.invitationURL;
                    if(invitationUrl != null){
                        request.ssiInvitationUrl = invitationUrl.toString();
                        return request.persist()
                            .replaceWithVoid()
                            .invoke(() -> this.notifications.onAccepted(new SessionId(request.session_id), resp.invitationURL.toString()))
                            ;
                    } else {
                        return Uni.createFrom().voidItem()
                            .invoke(() -> this.notifications.onAccepted(new SessionId(request.session_id)));
                    }
                });
        } else {
            return Uni.createFrom().voidItem();
        }
    }

    @ReactiveTransactional
    public Uni<String> getSsiInvitationUrl(SessionInfo sessionInfo){
        return Session.<Session>findById(sessionInfo.id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .onItem().ifNotNull().transform(session -> {
                checkAccessToken(session, sessionInfo);
                if (session.state != NotarizationRequestState.ACCEPTED) {
                    throw new InvalidRequestStateException(sessionInfo.id(), NotarizationRequestState.ACCEPTED, session.state);
                }
                return session.request.ssiInvitationUrl;
            });
    }

    @ReactiveTransactional
    public Uni<Void> updateContact(SessionInfo sessionInfo, String contact) {
        return Session.<Session>findById(sessionInfo.id().id)
            .onItem().ifNull().failWith(() -> new NotFoundException("Session"))
            .invoke(session -> {
                checkAccessToken(session, sessionInfo);

                this.notifications.onContactUpdate(new SessionId(session.id), contact);

            }).replaceWithVoid();
    }

    @ReactiveTransactional
    public Uni<Void> manualRelease(String releaseToken) {
        return Session.<Session>find(
                "manualReleaseToken = ?1 and state = ?2",
                releaseToken,
                NotarizationRequestState.PENDING_RQUESTOR_RELEASE
            ).firstResult()
            .onItem().ifNull().failWith(()->new NotFoundException("Session"))
            .onItem().ifNotNull()
            .transformToUni(sess -> {
                CallbackAuditingFilter.storeSessionId(request, sess.id);
                sess.state = NotarizationRequestState.ACCEPTED;

                    return Uni.combine().all()
                            .unis(
                                    sess.<Session>persistAndFlush(),
                                    profileService.find(new ProfileId(sess.profileId)))
                            .asTuple().chain(mapped -> this.handlePostAcceptance(sess.request,
                            mapped.getItem2()));

                }).replaceWithVoid();
    }
}
