/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import eu.gaiax.notarization.request_processing.domain.entity.Document;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.model.AipVersion;
import eu.gaiax.notarization.request_processing.domain.model.IssuanceResponse;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.domain.services.IssuanceService;
import eu.gaiax.notarization.request_processing.infrastructure.stringtemplate.ProfileTemplateRenderer;
import io.smallrye.mutiny.Uni;
import java.time.Instant;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class SsiIssuanceClient implements IssuanceService {

    private final SsiIssuanceRestClient issuanceClient;
    private final ProfileTemplateRenderer profileTemplateRenderer;
    private final ObjectMapper mapper;
    private final Logger logger;

    public SsiIssuanceClient(
            @RestClient SsiIssuanceRestClient issuanceClient,
            ProfileTemplateRenderer profileTemplateRenderer,
            ObjectMapper mapper,
            Logger logger) {
        this.issuanceClient = issuanceClient;
        this.profileTemplateRenderer = profileTemplateRenderer;
        this.mapper = mapper;
        this.logger = logger;
    }

    @Override
    public Uni<IssuanceResponse> issue(NotarizationRequest value, Profile profile) {
        return value.loadDocuments()
                .flatMap(r -> r.loadCredentialAugmentation())
                .map(r -> r.session.documents)
                .flatMap(docs -> {
                    var request = new SsiIssuanceRestClient.IssuanceRequest();

                    request.profileID = new ProfileId(value.session.profileId);
                    request.holderDID = value.did;
                    request.invitationURL = value.requestorInvitationUrl;
                    request.issuanceTimestamp = Instant.now();
                    request.successURL = value.session.successCBUri;
                    request.failureURL = value.session.failCBUri;

                    request.credentialData = determineCredentialData(value, profile, docs);

                    return this.issuanceClient.sendIssuanceRequest(request);
                }
                );

    }

    private JsonNode determineCredentialData(NotarizationRequest value,
            Profile profile,
            Set<Document> docs) throws IllegalArgumentException {
        JsonNode requestorData;
        try {
            requestorData = value.data != null ? mapper.readTree(value.data) : mapper.createObjectNode();
        } catch (JsonProcessingException ex) {
            logger.errorv("An unexpected error occurred while parsing the data for the request {0}", value.id, ex);
            throw new IllegalArgumentException(ex);
        }
        if (profile.aip() == AipVersion.V2_0) {
            if (requestorData instanceof ObjectNode objectNode) {
                objectNode.put("id", value.did);
            }
            var wrapper = mapper.createObjectNode();
            wrapper.set("credentialSubject", requestorData);
            requestorData = wrapper;
        }
        if (profile.documentTemplate() != null) {

            final String documentTemplate = profile.documentTemplate();

            String rendered = profileTemplateRenderer.render(profile, documentTemplate, docs, value);
            try {
                return mapper.readerForUpdating(requestorData)
                        .readTree(rendered);
            } catch (JsonProcessingException ex) {
                logger.errorv("An unexpected error occurred while parsing the template data for the request {0}", value.id, ex);
                throw new IllegalArgumentException(ex);
            }
        } else {
            return requestorData;
        }
    }

}
