/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application.taskprocessing;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.request_processing.application.DocumentStore;
import eu.gaiax.notarization.request_processing.domain.entity.Document;
import eu.gaiax.notarization.request_processing.domain.entity.DocumentStoreDocument;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.entity.SessionTask;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskId;
import eu.gaiax.notarization.request_processing.domain.model.taskprocessing.TaskInstance;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.smallrye.mutiny.Uni;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;

/**
 *
 * @author Florian Otto
 */
@ApplicationScoped
public class FileUpload implements FileUploadHandler {

    private final ObjectMapper mapper;
    private final Logger logger;
    private final DocumentStore documentStore;


    public FileUpload(
        Logger logger,
        ObjectMapper mapper,
        DocumentStore documentStore
    ){
        this.logger = logger;
        this.mapper = mapper;
        this.documentStore = documentStore;
    }

    public static final Logger LOG = Logger.getLogger(FileUpload.class);

    @Override
    @ReactiveTransactional
    public Uni<TaskInstance> createTask(TaskId id, JsonNode data, Session profile) {
        if (data != null) {
            throw new IllegalArgumentException();
        }
        return documentStore.startUploadTask(id)
            .chain(()-> {
                return Uni.createFrom().item(new TaskInstance());
            });
    }

    @Override
    @ReactiveTransactional
    public Uni<Void> cancelTask(TaskId id) {
        return documentStore.cancelTask(id);
    }

    @Override
    @ReactiveTransactional
    public Uni<Void> finishTaskSuccess(SessionTask sessionTask, JsonNode data) {

        try {
            var reader = mapper.readerFor(new TypeReference<List<DocumentStoreDocument>>() {});
            List<DocumentStoreDocument> lst = reader.readValue(data);

            return Mutiny.<Set<Document>>fetch(sessionTask.session.documents)
                .chain(docs -> {
                    var unis = lst.stream()
                        .map((d) -> {
                            var doc = new Document();
                            doc.id = d.id;
                            doc.content = d.content;
                            doc.title = d.title;
                            doc.mimetype = d.mimetype;
                            doc.extension = d.extension;
                            doc.shortDescription = d.shortDescription;
                            doc.longDescription = d.longDescription;
                            doc.verificationReport = d.verificationReport;
                            doc.hash = d.hash;
                            doc.createdAt = d.createdAt;
                            doc.lastModified = d.lastModified;

                            doc.session = sessionTask.session;

                            docs.add(doc);
                            return doc.persist().replaceWithVoid();
                        })
                        .collect(Collectors.toSet());

                    sessionTask.fulfilled = true;
                    unis.add(sessionTask.persist().replaceWithVoid());
                    unis.add(sessionTask.session.persist().replaceWithVoid());

                    return Uni.combine().all().unis(unis).discardItems();

                });

        } catch (IOException ex) {
            sessionTask.fulfilled = false;
        }

        return sessionTask.persist().replaceWithVoid();
    }

    @Override
    @ReactiveTransactional
    public Uni<Void> finishTaskFail(SessionTask sessionTask, JsonNode data) {
        sessionTask.fulfilled = false;
        return sessionTask.persist().replaceWithVoid();
    }

}
