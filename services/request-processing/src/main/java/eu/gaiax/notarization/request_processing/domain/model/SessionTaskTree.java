/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import eu.gaiax.notarization.request_processing.domain.entity.SessionTask;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.SessionTaskSummary;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Florian Otto
 */
public class SessionTaskTree {

        public SessionTaskTree(){
            this.allOf = new HashSet<>();
            this.oneOf= new HashSet<>();
            this.task = null;
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public SessionTaskSummary task;
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        public Set<SessionTaskTree> allOf;
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        public Set<SessionTaskTree> oneOf;

        public static SessionTaskTree buildTree(ProfileTaskTree profileTree, Set<SessionTask> tasks){

            var sessTaskTree = new SessionTaskTree();

            if(profileTree.taskName != null){
                sessTaskTree.task = tasks.stream()
                    .filter((t)->t.name.equals(profileTree.taskName))
                    .findFirst()
                    .map(SessionTaskSummary::valueOf)
                    .orElse(null)
                    ;

            } else {
                sessTaskTree.allOf =
                    profileTree.allOf.stream()
                    .map((childProfileTree) -> buildTree(childProfileTree, tasks))
                    .collect(Collectors.toSet());

                sessTaskTree.oneOf =
                    profileTree.oneOf.stream()
                    .map((childProfileTree) -> buildTree(childProfileTree, tasks))
                    .collect(Collectors.toSet());
            }

            return sessTaskTree;
        }
}
