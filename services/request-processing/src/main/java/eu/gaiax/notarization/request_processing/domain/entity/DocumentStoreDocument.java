/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.entity;

import java.time.OffsetDateTime;
import java.util.UUID;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;

/**
 *
 * @author Florian Otto
 */
@Entity
public class DocumentStoreDocument extends PanacheEntityBase {

    @Id
    public UUID id;

    public byte[] content;

    public UUID taskId;

    @Convert(converter = StringConverter.class)
    public String title;
    @Convert(converter = StringConverter.class)
    public String shortDescription;
    @Convert(converter = StringConverter.class)
    public String longDescription;

    @Convert(converter = StringConverter.class)
    public String mimetype;
    @Convert(converter = StringConverter.class)
    public String extension;

    @Convert(converter = StringConverter.class)
    public String verificationReport;
    public String hash;

    @CreationTimestamp
    public OffsetDateTime createdAt;
    @UpdateTimestamp
    public OffsetDateTime lastModified;

}
