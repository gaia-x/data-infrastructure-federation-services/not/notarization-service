/*
 *
 */
package eu.gaiax.notarization.request_processing.application.template;

import eu.gaiax.notarization.request_processing.domain.model.TemplateModel;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
public class DocumentHashModel implements TemplateModel.DocumentHash {

    private static final Logger logger = Logger.getLogger(DocumentHashModel.class);

    private final byte[] hash;

    public DocumentHashModel(byte[] hash) {
        this.hash = hash;
    }

    @Override
    public String getHex() {
        try {
            return Hex.encodeHexString(hash);
        } catch (IllegalArgumentException ex) {
            logger.warn("Could not get hex", ex);
            throw ex;
        }
    }

    @Override
    public String getBase64() {
        try {
            return Base64.encodeBase64String(hash);
        } catch (IllegalArgumentException ex) {
            logger.warn("Could not get base64", ex);
            throw ex;
        }
    }

    @Override
    public String getBase() {
        try {
            return Base64.encodeBase64String(hash);
        } catch (IllegalArgumentException ex) {
            logger.warn("Could not get base", ex);
            throw ex;
        }
    }

    @Override
    public String getBase64URLSafe() {
        return Base64.encodeBase64URLSafeString(hash);
    }

    public static DocumentHashModel fromHex(String hex) {
        if (hex == null) {
            return new DocumentHashModel(new byte[0]);
        }
        try {
            return new DocumentHashModel(Hex.decodeHex(hex));
        } catch (DecoderException ex) {
            logger.warnv("Could not handle hex value: {0}", hex, ex);
            throw new IllegalArgumentException("The given value could not be con", ex);
        }
    }
}
