
package eu.gaiax.notarization.request_processing.infrastructure.rest.openapi;

import java.util.List;
import org.eclipse.microprofile.openapi.models.media.Schema;

/**
 *
 * @author Neil Crossley
 */
public class SchemaStringWrapper {

    private final List<Schema> wrapped;

    public SchemaStringWrapper(Schema wrapped) {
        this.wrapped = List.of(wrapped);
    }

    public SchemaStringWrapper(List<Schema> wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (wrapped.size() == 1) {
            append(builder, wrapped.get(0));
        } else {
            builder.append("[");
            append(builder, wrapped);
            builder.append("]");
        }

        return builder.toString();
   }

    public void append(StringBuilder builder, List<Schema> schemas) {
        boolean hasElement = false;
        for (Schema item : schemas) {
            hasElement = addSeperator(builder, hasElement);
            append(builder, item);
        }
    }
    public void append(StringBuilder builder, Schema schema) {
        builder.append("[");

        boolean hasAdded = false;

        if (schema.getDescription() != null) {
            hasAdded = addSeperator(builder, hasAdded);
            builder.append("description:");
            builder.append(schema.getDescription());
        }
        if (schema.getTitle()!= null) {
            hasAdded = addSeperator(builder, hasAdded);
            builder.append("title:");
            builder.append(schema.getTitle());
        }
        if (schema.getRef() != null) {
            hasAdded = addSeperator(builder, hasAdded);
            builder.append("ref:");
            builder.append(schema.getRef());
        }
        if (schema.getType()!= null) {
            hasAdded = addSeperator(builder, hasAdded);
            builder.append("type:");
            builder.append(schema.getType());
        }
        if (schema.getItems() != null) {
            hasAdded = addSeperator(builder, hasAdded);
            builder.append("items:");
            append(builder, schema.getItems());
        }
        if (schema.getNullable()!= null) {
            hasAdded = addSeperator(builder, hasAdded);
            builder.append("nullable:");
            builder.append(schema.getNullable());
        }
        if (schema.getAllOf() != null) {
            hasAdded = addSeperator(builder, hasAdded);
            builder.append("allOf:");
            append(builder, schema.getAllOf());
        }
        if (schema.getAnyOf() != null) {
            hasAdded = addSeperator(builder, hasAdded);
            builder.append("anyOf:");
            append(builder, schema.getAnyOf());
        }
        if (schema.getOneOf()!= null) {
            hasAdded = addSeperator(builder, hasAdded);
            builder.append("oneOf:");
            append(builder, schema.getOneOf());
        }
        if (schema.getAdditionalPropertiesSchema() != null) {
            hasAdded = addSeperator(builder, hasAdded);
            builder.append("additionalProperties:");
            append(builder, schema.getAdditionalPropertiesSchema());
        }
        builder.append("]");
    }

    boolean addSeperator(StringBuilder builder, boolean hasAppended) {
        if (hasAppended) {
            builder.append(";");
        }
        return true;
    }

}
