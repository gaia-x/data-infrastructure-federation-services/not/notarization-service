/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.dto;

import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import eu.gaiax.notarization.request_processing.domain.model.SessionTaskTree;
import java.util.Set;
import java.util.stream.Collectors;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author Neil Crossley
 */
public record SessionSummary(
        @Schema(readOnly = true) SessionId sessionId,
        @Schema(readOnly = true) String profileId,
        @Schema(readOnly = true) NotarizationRequestState state,
        Set<SessionTaskSummary> tasks,
        SessionTaskTree preconditionTaskTree,
        SessionTaskTree taskTree,
        boolean preconditionTasksFulfilled,
        boolean tasksFulfilled
    ){

    public static SessionSummary asSummary(Session dbSession, Profile p) {
        var summary = new SessionSummary(
                new SessionId(dbSession.id),
                p.id(),
                dbSession.state,
                dbSession.tasks.stream().map(SessionTaskSummary::valueOf).collect(Collectors.toSet()),
                SessionTaskTree.buildTree(p.preconditionTasks(), dbSession.tasks),
                SessionTaskTree.buildTree(p.tasks(), dbSession.tasks),
                p.preconditionTasks().treeFulfilledBySession(dbSession),
                p.tasks().treeFulfilledBySession(dbSession)
        );
        return summary;
    }
}
