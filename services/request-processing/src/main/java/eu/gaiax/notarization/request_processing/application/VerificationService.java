/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application;

import eu.gaiax.notarization.request_processing.application.domain.VerificationResult;
import eu.gaiax.notarization.request_processing.domain.services.SignatureVerifierService;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.smallrye.mutiny.Uni;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class VerificationService {

    private final SignatureVerifierService signatureVerifier;
    private final Logger logger;

    public VerificationService(
            SignatureVerifierService signatureVerifier,
            Logger logger) {
        this.signatureVerifier = signatureVerifier;
        this.logger = logger;
    }

    @ReactiveTransactional
    public Uni<VerificationResult> verify(byte[] bytes) {
        return verify(new ByteArrayInputStream(bytes));
    }

    @ReactiveTransactional
    public Uni<VerificationResult> verify(InputStream is) {
        var digestAlgorithm = "SHA3-256";

        MessageDigest md;
        try {
            md = MessageDigest.getInstance(digestAlgorithm);
        } catch (NoSuchAlgorithmException ex) {
            throw new IllegalArgumentException("Illegal digest", ex);
        }
        DigestInputStream dis = new DigestInputStream(is, md);

        return this.signatureVerifier.verify(dis)
            .onTermination().invoke(() -> {
                try {
                    dis.close();
                } catch (IOException ex) {
                    logger.warn("Could not close the digest input stream", ex);
                }
                try {
                    is.close();
                } catch (IOException ex) {
                    logger.warn("Could not close the file input stream", ex);
                }
            }).map(rawReport -> {
                byte[] digest = md.digest();
                return new VerificationResult(digestAlgorithm, digest, rawReport);
        });
               
    }
}
