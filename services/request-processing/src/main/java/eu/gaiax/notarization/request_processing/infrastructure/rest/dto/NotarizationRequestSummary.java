/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.time.OffsetDateTime;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequestDbView;
import eu.gaiax.notarization.request_processing.domain.exception.BusinessException;

import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import eu.gaiax.notarization.request_processing.domain.model.DistributedIdentity;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;

/**
 *
 * @author Neil Crossley
 */
@Schema(description = "The overview of the notarization request")
public class NotarizationRequestSummary {
    @NotNull
    public NotarizationRequestId id;
    @NotNull
    public ProfileId profileId;
    @NotNull
    @Schema(readOnly = true)
    public OffsetDateTime createdAt;
    @NotNull
    @Schema(readOnly = true)
    public OffsetDateTime lastModified;
    @NotNull
    @Schema(readOnly = true, description = "The current processed state of the notarization request.")
    public NotarizationRequestState requestState;
    @NotNull
    @Schema(type = SchemaType.OBJECT,
            nullable = false,
            description = "The proposed content of the credential to be issued.")
    public JsonNode data;
    public DistributedIdentity holder;
    public int totalDocuments;
    @Schema(description = "The notarization request may be rejected by a notary. This is the given reason.")
    public String rejectComment;


    public static NotarizationRequestSummary from(NotarizationRequestDbView req, ObjectMapper objectMapper) throws BusinessException {
        try {
            var summary = new NotarizationRequestSummary();

            summary.id = new NotarizationRequestId(req.requestId);
            summary.createdAt = req.createdAt;
            summary.profileId = new ProfileId(req.profileId);
            summary.lastModified = req.lastModified;
            summary.requestState = req.state;
            var data = req.data;
            summary.data = data != null ? objectMapper.readTree(req.data) : null;
            summary.holder = new DistributedIdentity(req.did);
            var totalDocuments = req.totalDocuments;
            summary.totalDocuments = totalDocuments != null ? totalDocuments : 0;
            summary.rejectComment = req.rejectComment;

            return summary;
        }
        catch (JsonProcessingException e) {
            throw new BusinessException("Invalid json data", e);
        }
    }

}
