/*
 *
 */
package eu.gaiax.notarization.request_processing.infrastructure.stringtemplate;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.Iterator;

/**
 *
 * @author Neil Crossley
 */
public record JsonArraySTWrapper(JsonArray json) implements Iterable<Object> {

    @Override
    public Iterator<Object> iterator() {
        return new IterStWrapper(json, json.iterator());
    }

    public static class IterStWrapper implements Iterator<Object> {
        private final Iterator<Object> iterator;
        private final JsonArray json;

        public IterStWrapper(JsonArray json, Iterator<Object> iterator) {
            this.json = json;
            this.iterator = iterator;
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public Object next() {
            var current = iterator.next();
            if (current instanceof JsonObject jsonObject) {
                return new JsonObjectSTWrapper(jsonObject);
            }
            if (current instanceof JsonArray jsonArray) {
                return new JsonArraySTWrapper(jsonArray);
            }
            return current;
        }

        @Override
        public String toString() {
            return this.json.encode();
        }
    }
}
