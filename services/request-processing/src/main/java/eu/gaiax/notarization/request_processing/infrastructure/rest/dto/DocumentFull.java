/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.dto;

import eu.gaiax.notarization.request_processing.domain.entity.Document;
import eu.gaiax.notarization.request_processing.domain.model.DocumentId;
import eu.gaiax.notarization.request_processing.domain.model.VerificationReport;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author Neil Crossley
 */
public class DocumentFull extends DocumentView {

    @Schema(required = true)
    public byte[] content;

    @Schema(required = true)
    public VerificationReport verificationReport;
    @Schema(required = true)
    public String hash;

    public static DocumentFull fromDocument(Document document) {
        var full = new DocumentFull();

        full.id = new DocumentId(document.id);
        full.content = document.content;
        full.verificationReport = VerificationReport.valueOf(document.verificationReport);
        full.hash = document.hash;
        full.title = document.title;
        full.shortDescription = document.shortDescription;
        full.longDescription = document.longDescription;
        full.mimetype = document.mimetype;
        full.extension = document.extension;

        return full;
    }
}
