/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.openapi;

import com.github.fge.jackson.jsonpointer.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import eu.gaiax.notarization.request_processing.domain.model.ProfileTaskTree;
import io.smallrye.openapi.api.util.MergeUtil;
import jakarta.json.JsonValue.ValueType;
import java.security.Key;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.eclipse.microprofile.openapi.OASFilter;
import org.eclipse.microprofile.openapi.models.media.Schema;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.security.cert.X509Certificate;
import org.eclipse.microprofile.openapi.OASFactory;
import org.eclipse.microprofile.openapi.models.OpenAPI;
import org.eclipse.microprofile.openapi.models.Operation;
import org.eclipse.microprofile.openapi.models.PathItem;
import org.eclipse.microprofile.openapi.models.media.MediaType;
import org.eclipse.microprofile.openapi.models.media.Schema.SchemaType;
import org.eclipse.microprofile.openapi.models.parameters.Parameter;
import org.eclipse.microprofile.openapi.models.parameters.RequestBody;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import org.jose4j.jwk.PublicJsonWebKey;

/**
 *
 * @author Neil Crossley
 */
public class OpenApiCorrection implements OASFilter {

    private static final Logger LOG = Logger.getLogger(OpenApiCorrection.class);

    private final Set<Class<?>> unwantedClasses = Set.of(
            JsonNode.class,
            JsonPointer.class,
            javax.json.JsonValue.class,
            jakarta.json.JsonValue.class,
            ValueType.class,
            FileUpload.class,
            ProfileTaskTree.class,
            PublicJsonWebKey.class,
            PrivateKey.class,
            X509Certificate.class,
            PublicKey.class,
            Key.class,
            Principal.class
    );
    private final Set<String> unwantedNames = Set.of(
            "JsonValue1",
            "ValueType1"
    );
    private final Set<String> unwantedComponents = Stream.concat(unwantedClasses.stream()
            .map(clazz -> clazz.getSimpleName()), unwantedNames.stream())
            .collect(Collectors.toSet());

    private final Set<String> unwantedReferences = unwantedComponents.stream()
            .map(component -> "#/components/schemas/" + component)
            .collect(Collectors.toSet());

    private final Set<String> unwantedTags = Set.of(
            "CDI Wrapper"
    );

    private final List<Schema> foundReferences = new ArrayList<>();

    @Override
    public void filterOpenAPI(OpenAPI openAPI) {
        boolean shouldReplaceReferencesWithPrimitives = false;
        if (shouldReplaceReferencesWithPrimitives) {
            final String componentsschemas = "#/components/schemas/";
            var components = openAPI.getComponents().getSchemas();
            replaceReferencesWithPrimitives(componentsschemas, components);
        }
        final Map<String, Schema> componentSchemas = new HashMap<>(openAPI.getComponents().getSchemas());

        for (String unwantedComponent : unwantedComponents) {
            var removed = componentSchemas.remove(unwantedComponent);
            if (removed != null) {
                LOG.debugv("Removed component {0}", unwantedComponent);
            }
        }
        openAPI.getComponents().setSchemas(componentSchemas);

        var removablePaths = new HashSet<String>();
        for (Map.Entry<String, PathItem> pathItemEntry : openAPI.getPaths().getPathItems().entrySet()) {
            var path = pathItemEntry.getKey();
            var pathItem = pathItemEntry.getValue();
            for (Map.Entry<PathItem.HttpMethod, Operation> operationEntry : pathItem.getOperations().entrySet()) {
                if (!isWanted(operationEntry.getValue())) {
                    LOG.debugv("Removing operation [{0}:{1}]", path, operationEntry.getKey());
                    pathItem.setOperation(operationEntry.getKey(), null);
                }
            }
            if (pathItem.getOperations().isEmpty()) {
                LOG.debugv("Removing path [{0}]", path);
                removablePaths.add(path);
            }
        }
        if (!removablePaths.isEmpty()) {
            var replacementPathItems = new HashMap<String, PathItem>(openAPI.getPaths().getPathItems());
            replacementPathItems.keySet().removeAll(removablePaths);
            openAPI.getPaths().setPathItems(replacementPathItems);
        }

        fixJsonObjectType(openAPI);
    }

    public void replaceReferencesWithPrimitives(final String componentsschemas, Map<String, Schema> components) {
        var primitiveTypes = Set.of(
                SchemaType.STRING,
                SchemaType.BOOLEAN,
                SchemaType.NUMBER,
                SchemaType.INTEGER
        );
        for (Schema referencingSchema : foundReferences) {
            var reference = referencingSchema.getRef();
            if (reference != null && reference.startsWith(componentsschemas)) {
                var referencedComponentName = reference.substring(componentsschemas.length());

                var referencedComponent = components.get(referencedComponentName);
                if (referencedComponent != null
                        && primitiveTypes.contains(referencedComponent.getType())
                        && !hasItems(referencedComponent.getEnumeration())) {
                    LOG.debugv("Removing references to schema {0} {1} in schema {2} ",
                        referencedComponentName,
                        new SchemaStringWrapper(referencedComponent),
                        new SchemaStringWrapper(referencingSchema));
                    referencingSchema.setRef(null);
                    referencingSchema = MergeUtil.mergeObjects(referencingSchema, referencedComponent);
                }
            }
        }
    }

    private boolean isWanted(Operation operation) {
        var currentTags = operation.getTags();
        if (currentTags == null || currentTags.isEmpty()) {
            return true;
        }
        for (String currentTag : currentTags) {
            if (unwantedTags.contains(currentTag)) {
                LOG.debugv("Removing unwanted operation [{0},{1}] with tag {2}",
                        operation.getOperationId(),
                        operation.getSummary(),
                        currentTag);
                return false;
            }
        }
        return true;
    }

    @Override
    public Parameter filterParameter(Parameter parameter) {
        var schema = parameter.getSchema();

        var result = sanitize(schema);
        if (result.changed) {
            LOG.debugv("Parameter ''{0}'' had schema {1}, is now {2}",
                            parameter.getName(),
                            new SchemaStringWrapper(schema),
                            new SchemaStringWrapper(result.value));
            parameter.setSchema(result.value);
        }
        return parameter;
    }

    @Override
    public RequestBody filterRequestBody(RequestBody requestBody) {

        var content = requestBody.getContent();

        for (Map.Entry<String, MediaType> entry : content.getMediaTypes().entrySet()) {
            var key = entry.getKey();
            var mediaType = entry.getValue();
            var originalSchema = mediaType.getSchema();
            var result = sanitize(originalSchema);
            if (result.changed) {
                LOG.debugv("Mediatype ''{0}'' was {1}, is now {2}",
                        key,
                        new SchemaStringWrapper(originalSchema),
                        new SchemaStringWrapper(result.value));
                mediaType.setSchema(result.value);
            }
        }

        return requestBody;
    }

    @Override
    public Schema filterSchema(Schema schema) {

        var props = schema.getProperties();

        if (props != null) {
            HashMap<String, Schema> changes = new HashMap<>();
            for (Map.Entry<String, Schema> entry : props.entrySet()) {
                var key = entry.getKey();
                var val = entry.getValue();
                var result = sanitize(val);

                if (result.changed) {
                    LOG.debugv("Property ''{0}'' was {1}, is now {2}",
                            key,
                            new SchemaStringWrapper(val),
                            new SchemaStringWrapper(result.value));
                    changes.put(key, result.value);
                }
            }
            if (!changes.isEmpty()) {
                for (Map.Entry<String, Schema> entry : props.entrySet()) {
                    String key = entry.getKey();
                    Schema val = entry.getValue();
                    changes.putIfAbsent(key, val);
                }

                schema.setProperties(changes);
            }
        }

        return schema;
    }

    private void fixJsonObjectType(OpenAPI openAPI) {
        openAPI.getComponents().getSchemas()
                .get("JsonObject")
                .type(SchemaType.OBJECT);
    }

    private ChangeStatus<Schema> sanitize(Schema schema) {
        if (schema == null) {
            return ChangeStatus.unchanged(schema);
        }
        try {
            var withoutUnwanted = removeUnwantedReferences(schema);
            var simplifieyAllOf = compactAllOf(withoutUnwanted.value);
            var compacted = simplifySingleAllOfRef(simplifieyAllOf.value);
            storeReferences(compacted.value);

            return new ChangeStatus<>(compacted.value,
                    simplifieyAllOf.changed || withoutUnwanted.changed || compacted.changed);
        } catch (Throwable t) {

            LOG.error("An error occurred while processing changes to the OpenAPI specification", t);
            throw t;
        }
    }

    private ChangeStatus<Schema> compactAllOf(Schema propertySchema) {
        List<Schema> allOf = propertySchema.getAllOf();
        if (allOf != null && !allOf.isEmpty()) {
            Schema mergedResult = MergeUtil.mergeObjects(OASFactory.createSchema(), propertySchema);
            final List<Schema> resultAllOf = new ArrayList<>(allOf.size());
            for (Schema schema : allOf) {
                if (schema != null) {
                    if (schema.getType() == null && schema.getRef() == null) {
                        mergedResult = MergeUtil.mergeObjects(mergedResult, schema);
                    } else {
                        resultAllOf.add(schema);
                    }
                }
            }
            if (resultAllOf.isEmpty()) {
                mergedResult.setAllOf(null);
            } else if (resultAllOf.size() == 1) {
                mergedResult.setAllOf(null);
                mergedResult = MergeUtil.mergeObjects(mergedResult, resultAllOf.get(0));
            } else {
                mergedResult.setAllOf(resultAllOf);
            }
            return ChangeStatus.changed(mergedResult);
        } else {
            return ChangeStatus.unchanged(propertySchema);
        }
    }

    private ChangeStatus<Schema> removeUnwantedReferences(Schema propertySchema) {
        final List<Schema> allOf = propertySchema.getAllOf();
        if (allOf != null && !allOf.isEmpty()) {
            FilterResults result = filterWanted(allOf);
            if (result.hasUnwanted) {
                Schema mergedResult = MergeUtil.mergeObjects(OASFactory.createSchema(), propertySchema);
                mergedResult.setAllOf(result.wanted);
                var compactedResult = compactAllOf(mergedResult);
                if (compactedResult.changed) {
                    var compacted = compactedResult.value;
                    if (compacted.getAllOf() == null || compacted.getAllOf().isEmpty()) {
                        compacted.setAllOf(null);
                        if (compacted.getType() == null) {
                            compacted.setType(SchemaType.OBJECT);
                        }
                    }
                    return ChangeStatus.changed(compacted);
                } else {
                    return ChangeStatus.changed(mergedResult);
                }
            }
        }
        final List<Schema> anyOf = propertySchema.getAnyOf();
        if (anyOf != null && anyOf.size() >= 2) {
            FilterResults result = filterWanted(anyOf);
            if (result.hasUnwanted) {
                if (result.wanted.size() > 1) {
                    propertySchema.setAnyOf(result.wanted);
                    return ChangeStatus.changed(propertySchema);
                } else {
                    propertySchema.setAnyOf(null);
                    return ChangeStatus.changed(result.wanted.get(0));
                }
            }
        }
        final var ref = propertySchema.getRef();
        if (ref != null && unwantedReferences.contains(ref)) {
            LOG.debugv("Removing ref: {0}", ref);
            propertySchema.setRef(null);
            propertySchema.setType(SchemaType.OBJECT);
            return ChangeStatus.changed(propertySchema);
        }
        return ChangeStatus.unchanged(propertySchema);
    }

    private ChangeStatus<Schema> simplifySingleAllOfRef(Schema propertySchema) {
        final List<Schema> allOf = propertySchema.getAllOf();
        if (allOf != null && allOf.size() >= 2) {
            int refTypeCount = 0;
            for (var currentSchema : allOf) {
                if (currentSchema.getRef() != null) {
                    refTypeCount += 1;
                    continue;
                }
                if (currentSchema.getType() != null) {
                    refTypeCount += 1;
                    continue;
                }
            }
            if (refTypeCount == 1) {
                Schema mergedResult = MergeUtil.mergeObjects(OASFactory.createSchema(), propertySchema);
                mergedResult.setAllOf(null);

                for (var currentSchema : allOf) {
                    mergedResult = MergeUtil.mergeObjects(mergedResult, currentSchema);
                }
                return ChangeStatus.changed(mergedResult);
            }
        }
        return ChangeStatus.unchanged(propertySchema);
    }

    private FilterResults filterWanted(final List<Schema> schemas) {
        List<Schema> wanted = new ArrayList<>(schemas.size());
        int unwanted = 0;
        for (Schema currentSchema : schemas) {
            var currentRef = currentSchema.getRef();
            if (currentRef != null && unwantedReferences.contains(currentRef)) {
                unwanted += 1;
            } else {
                wanted.add(currentSchema);
            }
        }
        FilterResults result = new FilterResults(unwanted > 0, wanted);
        return result;
    }

    private void storeReferences(Schema result) {
        if (result.getRef() != null) {
            foundReferences.add(result);
        }
        storeReferences(result.getOneOf());
        storeReferences(result.getAllOf());
        storeReferences(result.getAnyOf());
        var items = result.getItems();
        if (items != null) {
            if (items.getRef() != null) {
                foundReferences.add(items);
            }
        }
    }

    private void storeReferences(List<Schema> schemas) {
        if (schemas == null) {
            return;
        }
        for (Schema schema : schemas) {
            if (schema.getRef() != null) {
                foundReferences.add(schema);
            }
        }
    }

    private <T> boolean hasItems(List<T> list) {
        return list != null && !list.isEmpty();
    }

    private static class FilterResults {

        public final boolean hasUnwanted;
        public final List<Schema> wanted;

        public FilterResults(boolean hasUnwanted, List<Schema> wanted) {
            this.hasUnwanted = hasUnwanted;
            this.wanted = wanted;
        }

    }

    private static class ChangeStatus<T> {

        public final T value;
        public final boolean changed;

        public ChangeStatus(T value, boolean changed) {
            this.value = value;
            this.changed = changed;
        }

        public static <T> ChangeStatus<T> changed(T value) {
            return new ChangeStatus<>(value, true);
        }
        public static <T> ChangeStatus<T> unchanged(T value) {
            return new ChangeStatus<>(value, false);
        }
    }
}
