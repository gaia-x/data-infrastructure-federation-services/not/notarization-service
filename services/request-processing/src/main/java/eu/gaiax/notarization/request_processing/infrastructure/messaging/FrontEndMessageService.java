/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.messaging;

import java.time.ZonedDateTime;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Metadata;

import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import eu.gaiax.notarization.request_processing.domain.services.RequestNotificationService;
import static eu.gaiax.notarization.request_processing.infrastructure.messaging.MsgType.CONTACT_UPDATE;
import static eu.gaiax.notarization.request_processing.infrastructure.messaging.MsgType.EXTERNAL_TASK_STARTED;
import static eu.gaiax.notarization.request_processing.infrastructure.messaging.MsgType.REQUEST_ACCEPTED;
import static eu.gaiax.notarization.request_processing.infrastructure.messaging.MsgType.REQUEST_REJECTED;
import static eu.gaiax.notarization.request_processing.infrastructure.messaging.MsgType.REQUEST_TERMINATED;
import static eu.gaiax.notarization.request_processing.infrastructure.messaging.MsgType.REQUEST_DELETED;
import static eu.gaiax.notarization.request_processing.infrastructure.messaging.MsgType.REQUEST_ACCEPTED_PENDING_DID;
import io.smallrye.reactive.messaging.rabbitmq.OutgoingRabbitMQMetadata;
import io.vertx.core.json.JsonObject;
import java.net.URI;
import static eu.gaiax.notarization.request_processing.infrastructure.messaging.MsgType.READY_FOR_REVIEW;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@ApplicationScoped
public class FrontEndMessageService implements RequestNotificationService {

    public static record EvtRequestorMessage(String id, MsgType msg, String payload){}
	public static record EvtOperatorMessage(String id, MsgType msg, ProfileId profileId){}

    public static final String onRequestorRequestChanged = "onRequestorRequestChanged";

    public static final String onOperatorRequestChanged = "onOperatorRequestChanged";

    public static final String outgoingRequestorRequestChanged = "requestor-request-changed";
    public static final String outgoingOperatorRequestChanged = "operator-request-changed";

    @Channel(outgoingOperatorRequestChanged)
    Emitter<JsonObject> operatorChannel;

    @Channel(outgoingRequestorRequestChanged)
    Emitter<JsonObject> requestorChannel;

    public void requestorRequestChanged(JsonObject data) {
        final OutgoingRabbitMQMetadata metadata = new OutgoingRabbitMQMetadata.Builder()
            .withRoutingKey(data.getString("id"))
            .withTimestamp(ZonedDateTime.now())
            .build();

        var msg = Message.of(data, Metadata.of(metadata));
        requestorChannel.send(msg);
    }

    public void operatorRequestChanged(JsonObject data) {
        final OutgoingRabbitMQMetadata metadata = new OutgoingRabbitMQMetadata.Builder()
            .withRoutingKey(String.format("%s.%s", data.getString("profileId"), data.getString("id")))
            .withTimestamp(ZonedDateTime.now())
            .build();

        var msg = Message.of(data, Metadata.of(metadata));
        operatorChannel.send(msg);
    }

    @Override
    public void onReadyForReview(NotarizationRequestId request, ProfileId profile) {
        if (request == null || request.id == null || profile == null || profile.id() == null) {
            return;
        }
        sendToOperatorChannel(new EvtOperatorMessage(request.id.toString(), READY_FOR_REVIEW, profile));
    }

    @Override
    public void onRejected(SessionId identifier) {
        if (identifier == null || identifier.id == null) {
            return;
        }
        sendToRequestorChannel(new EvtRequestorMessage(identifier.id, REQUEST_REJECTED, null));
    }

    @Override
    public void onPendingDid(SessionId identifier) {
        if (identifier == null || identifier.id == null) {
            return;
        }
        sendToRequestorChannel(new EvtRequestorMessage(identifier.id, REQUEST_ACCEPTED_PENDING_DID, null));
    }

    @Override
    public void onAccepted(SessionId identifier) {
        if (identifier == null || identifier.id == null) {
            return;
        }
        sendToRequestorChannel(new EvtRequestorMessage(identifier.id, REQUEST_ACCEPTED, null));
    }
    @Override
    public void onAccepted(SessionId identifier, String inviteUrl) {
        if (identifier == null || identifier.id == null) {
            return;
        }
        sendToRequestorChannel(new EvtRequestorMessage(identifier.id, REQUEST_ACCEPTED, inviteUrl));
    }
    @Override
    public void onExternalTask(SessionId identifier, URI redirect) {
        if (identifier == null || identifier.id == null) {
            return;
        }
        sendToRequestorChannel(new EvtRequestorMessage(identifier.id, EXTERNAL_TASK_STARTED, toString(redirect)));
    }

    @Override
    public void onContactUpdate(SessionId identifier, String contact) {
        if (identifier == null || identifier.id == null) {
            return;
        }
        sendToRequestorChannel(new EvtRequestorMessage(identifier.id, CONTACT_UPDATE, contact));
    }

    @Override
    public void onDeleted(SessionId identifier) {
        if (identifier == null || identifier.id == null) {
            return;
        }
        sendToRequestorChannel(new EvtRequestorMessage(identifier.id, REQUEST_DELETED, null));
    }

    @Override
    public void onTerminated(SessionId identifier) {
        if (identifier == null || identifier.id == null) {
            return;
        }
        sendToRequestorChannel(new EvtRequestorMessage(identifier.id, REQUEST_TERMINATED, null));
    }

    private void sendToRequestorChannel(EvtRequestorMessage evtMsg){
        var obj = JsonObject.mapFrom(evtMsg);
        requestorRequestChanged(obj);
    }

    private void sendToOperatorChannel(EvtOperatorMessage evtMsg){
        var obj = JsonObject.mapFrom(evtMsg);
        operatorRequestChanged(obj);
    }

    private <T> String toString(T item) {
        return item == null ? null : item.toString();
    }
}
