/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.mappers;

import eu.gaiax.notarization.request_processing.domain.exception.InvalidTaskStateException;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mappers.problem_details.ProblemDetails;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

/**
 *
 * @author Neil Crossley
 */
@Provider
public class InvalidTaskStateExceptionMapper implements ExceptionMapper<InvalidTaskStateException> {

    @Override
    @APIResponse(responseCode = "400", description = "Bad Request",
            content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ProblemDetails.class)))
    @Produces(MediaType.APPLICATION_JSON)
    public Response toResponse(InvalidTaskStateException e) {
        return Response.status(Response.Status.BAD_REQUEST).entity(
                new ProblemDetails(
                        "Bad request",
                        Response.Status.BAD_REQUEST.getStatusCode(),
                        e.getMessage().isEmpty() ? "The request was bad.": e.getMessage(),
                        null
                )).build();
    }

}
