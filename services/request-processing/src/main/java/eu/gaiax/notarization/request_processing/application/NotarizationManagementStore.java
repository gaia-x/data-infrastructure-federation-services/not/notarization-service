/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.application;


import com.fasterxml.jackson.databind.JsonNode;
import javax.enterprise.context.ApplicationScoped;
import com.fasterxml.jackson.databind.ObjectMapper;
import static eu.gaiax.notarization.request_processing.application.NotarizationRequestStore.urlSafeString;
import eu.gaiax.notarization.request_processing.application.taskprocessing.TaskManager;
import eu.gaiax.notarization.request_processing.domain.entity.Document;

import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequestDbView;
import eu.gaiax.notarization.request_processing.domain.entity.RequestorIdentity;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.exception.AuthorizationException;
import eu.gaiax.notarization.request_processing.domain.exception.InvalidProfileException;
import eu.gaiax.notarization.request_processing.domain.exception.InvalidRequestStateException;
import eu.gaiax.notarization.request_processing.domain.exception.NotFoundException;
import eu.gaiax.notarization.request_processing.domain.model.DocumentId;
import eu.gaiax.notarization.request_processing.domain.model.IssuanceResponse;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.domain.model.RequestFilter;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import eu.gaiax.notarization.request_processing.domain.services.IssuanceService;
import eu.gaiax.notarization.request_processing.domain.services.ProfileService;
import eu.gaiax.notarization.request_processing.domain.services.RequestNotificationService;
import eu.gaiax.notarization.request_processing.domain.services.RevocationService;
import eu.gaiax.notarization.request_processing.infrastructure.rest.Api;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.DocumentFull;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.IdentityView;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.NotarizationRequestSummary;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.NotarizationRequestView;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.PagedNotarizationRequestSummary;
import eu.gaiax.notarization.request_processing.infrastructure.rest.feature.audit.CallbackAuditingFilter;
import io.quarkus.hibernate.reactive.panache.PanacheQuery;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.quarkus.runtime.StartupEvent;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.enterprise.event.Observes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.ClientWebApplicationException;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class NotarizationManagementStore {

    private final ObjectMapper objectMapper;
    private SecureRandom secureRandom;
    private final ProfileService profileService;
    private final RequestNotificationService notifications;
    private final IssuanceService issuanceService;
    private final RevocationService revocationService;
    private final TaskManager taskManager;
    private final Logger logger;
    private final HttpServerRequest request;

    public NotarizationManagementStore(
            ObjectMapper objectMapper,
            ProfileService profileService,
            RequestNotificationService notifications,
            IssuanceService issuanceService,
            RevocationService revocationService,
            TaskManager taskManager,
            HttpServerRequest request,
            Logger logger
            ) {
        this.objectMapper = objectMapper;
        this.profileService = profileService;
        this.notifications = notifications;
        this.issuanceService = issuanceService;
        this.revocationService = revocationService;
        this.taskManager = taskManager;
        this.request = request;
        this.logger = logger;
    }

    void onStartup(@Observes StartupEvent ev) {
        /*
         * Native builds optimise the application by initializing all instances at build time.
         *
         * However, SecureRandom will only correctly work in containers if initalized at runtime, using the relevant resources of the host machine.
         */
        this.secureRandom = new SecureRandom();
    }

    private static PanacheQuery<NotarizationRequestDbView> fetchAvailNotarizationRequests(
            RequestFilter filter,
            Set<String> profileIds,
            String notaryName
        ) {
        var states = statesByFilter(filter);
        if(filter == RequestFilter.ownClaimed){
             return NotarizationRequestDbView.<NotarizationRequestDbView>find(
                "state in ?1 and profileId in ?2 and claimedBy = ?3",
                states,
                profileIds,
                notaryName
            );
        }
        else {
            return NotarizationRequestDbView.<NotarizationRequestDbView>find(
                "state in ?1 and profileId in ?2",
                states,
                profileIds
            );
        }
    }

    private static Set<NotarizationRequestState> statesByFilter(RequestFilter filter){
        return switch (filter) {
            case available -> Set.of(NotarizationRequestState.READY_FOR_REVIEW);
            case allClaimed -> Set.of(NotarizationRequestState.WORK_IN_PROGRESS);
            case ownClaimed -> Set.of(NotarizationRequestState.WORK_IN_PROGRESS);
            default -> Set.of();
        };
    }

    @ReactiveTransactional
    public Uni<PagedNotarizationRequestSummary> fetchAvailableRequests(
		int pageOffset,
		int limit,
        RequestFilter filter,
        SecurityContext securityContext
    ) {
        return this.profileService.findAll().flatMap(profiles -> {

            Set<String> permittedProfiles = new HashSet<>(profiles.size());
            for (Profile profile : profiles) {
                if (securityContext.isUserInRole(profile.id())) {
                    permittedProfiles.add(profile.id());
                }
            }

            var availableRequests = fetchAvailNotarizationRequests(filter, permittedProfiles, securityContext.getUserPrincipal().getName());

            var count = availableRequests.count();
            var page = availableRequests.page(pageOffset, limit);
            var pageEntries = page.list();
            var pageCount = page.pageCount();

            return Uni.combine().all().unis(pageEntries, pageCount, count).asTuple().map(tuple -> {
                var pagedNotarizationSummary = new PagedNotarizationRequestSummary();

                List<NotarizationRequestSummary> results = new ArrayList<>();
                for (NotarizationRequestDbView foundRequest : tuple.getItem1()) {
                    results.add(NotarizationRequestSummary.from(foundRequest, objectMapper));
                }

                pagedNotarizationSummary.pageCount = tuple.getItem2();
                pagedNotarizationSummary.notarizationRequests = results;
                pagedNotarizationSummary.requestCount = tuple.getItem3();

                return pagedNotarizationSummary;
            });
        });

    }

    @ReactiveTransactional
    public Uni<DocumentFull> fetchAvailableDocument(ProfileId profileId, NotarizationRequestId notReqId, DocumentId docId) {
         return NotarizationRequest.findById(notReqId)
            .onItem().ifNull().failWith(()->{return new NotFoundException("NotarizationRequest");})
            .onItem().ifNotNull()
            .transformToUni(req -> {
                if (!Objects.equals(req.session.profileId, profileId.id())) {
                    throw new NotFoundException("The given request could not be found.");
                }
                if(!req.session.state.isFetchableByNotary()) {
                    throw new InvalidRequestStateException(new SessionId(req.session.id), NotarizationRequestState.fetchByNotaryStates, req.session.state);
                }
                return Document.<Document>findById(docId.id)
                    .map(DocumentFull::fromDocument);
            });

    }

    @ReactiveTransactional
    public Uni<NotarizationRequestView> fetchAvailableRequest(ProfileId profileId, NotarizationRequestId id) {
        return NotarizationRequest.<NotarizationRequest>findById(id)
            .onItem().ifNull().failWith(()->{return new NotFoundException("NotarizationRequest");})
            .onItem().ifNotNull()
            .transformToUni(req -> {
                if (!Objects.equals(req.session.profileId, profileId.id())) {
                    throw new NotFoundException("The given request could not be found.");
                }
                if(!req.session.state.isFetchableByNotary()) {
                    throw new InvalidRequestStateException(new SessionId(req.session.id), NotarizationRequestState.fetchByNotaryStates, req.session.state);
                }
                return req.loadDocuments().map(r -> NotarizationRequestView.from(req, objectMapper));
            });
    }

    @ReactiveTransactional
    public Uni<Void> claimAvailableRequest(ProfileId profileId, NotarizationRequestId id, SecurityContext securityContext) {
        return NotarizationRequest.<NotarizationRequest>findById(id)
            .onItem().ifNull().failWith(()->{return new NotFoundException("NotarizationRequest");})
            .onItem().ifNotNull()
            .transformToUni(req -> {
                if (!Objects.equals(req.session.profileId, profileId.id())) {
                    throw new NotFoundException("The given request could not be found.");
                }
                if(!req.session.state.isClaimableByNotary()) {
                    throw new InvalidRequestStateException(new SessionId(req.session.id), NotarizationRequestState.claimByNotaryStates, req.session.state);
                }

                req.session.state = NotarizationRequestState.WORK_IN_PROGRESS;
                req.claimedBy = securityContext.getUserPrincipal().getName();
                return req.session.persist().chain(() -> req.persist());

            }).replaceWithVoid();
    }

    @ReactiveTransactional
    public Uni<Void> rejectAvailableRequest(ProfileId profileId, NotarizationRequestId id, String rejectComment) {
        return NotarizationRequest.<NotarizationRequest>findById(id)
            .onItem().ifNull().failWith(()->{return new NotFoundException("NotarizationRequest");})
            .onItem().ifNotNull()
            .transformToUni(req -> {
                if (!Objects.equals(req.session.profileId, profileId.id())) {
                    throw new NotFoundException("The given request could not be found.");
                }
                if(!req.session.state.isRejectableByNotary()) {
                    throw new InvalidRequestStateException(new SessionId(req.session.id), NotarizationRequestState.rejectByNotaryStates, req.session.state);
                }

                req.rejectComment = rejectComment;
                req.session.state = NotarizationRequestState.EDITABLE;
                return req.session.persist()
                        .chain(() -> req.<NotarizationRequest>persist())
                        .invoke(r -> this.notifications.onRejected(new SessionId(r.session_id)));

            }).replaceWithVoid();
    }

    @ReactiveTransactional
    public Uni<Void> deleteRequest(ProfileId profileId, NotarizationRequestId id) {
        return NotarizationRequest.<NotarizationRequest>findById(id)
            .onItem().ifNull().failWith(()->{return new NotFoundException("NotarizationRequest");})
            .chain(req -> {
                if (!Objects.equals(req.session.profileId, profileId.id())) {
                    throw new NotFoundException("The given request could not be found.");
                }
                if(!req.session.state.isDeleteableByNotary()) {
                    throw new InvalidRequestStateException(new SessionId(req.session.id), NotarizationRequestState.deleteByNotaryStates, req.session.state);
                }

                var sess = req.session;
                sess.state = NotarizationRequestState.TERMINATED;
                sess.request = null;

                return Uni.createFrom().voidItem()
                    .chain(() -> sess.persistAndFlush())
                    .chain(() -> RequestorIdentity.delete("session", sess))
                    .chain(() -> req.delete())
                    .chain(() -> req.flush())
                    .invoke(r -> this.notifications.onDeleted(new SessionId(sess.id)))
                    ;
            });

    }

    @ReactiveTransactional
    public Uni<Void> acceptAvailableRequest(ProfileId profileId, NotarizationRequestId id) {
        return NotarizationRequest.<NotarizationRequest>findById(id)
                .onItem().ifNull().failWith(() -> new NotFoundException("NotarizationRequest"))
                .onItem().ifNotNull().transformToUni(req -> {
                    if (!Objects.equals(req.session.profileId, profileId.id())) {
                        throw new NotFoundException("The given request could not be found.");
                    }
                    if (!req.session.state.isAcceptableByNotary()) {
                        throw new InvalidRequestStateException(new SessionId(req.session.id), NotarizationRequestState.acceptByNotaryStates, req.session.state);
                    }
                    return this.profileService.find(profileId)
                            .onItem().ifNull().failWith(() -> new InvalidProfileException(profileId))
                            .onItem().ifNotNull().transformToUni(profile -> {

                                req.session.state = determineAcceptState(req, profile);
                                addCallbacksTo(req.session);

                                return req.session.persist()
                                        .chain(() -> req.<NotarizationRequest>persist())
                                        .chain(r -> this.handlePostAcceptance(r, profile));
                            }).replaceWithVoid();
                });
    }

    @ReactiveTransactional
    public Uni<Set<IdentityView>> getIdentity(ProfileId profileId, NotarizationRequestId id) {

        return NotarizationRequest.<NotarizationRequest>findById(id)
            .onItem().ifNull().failWith(()->{return new NotFoundException("NotarizationRequest");})
            .onItem().ifNotNull()
            .transformToUni(notReq -> {
                if (!Objects.equals(notReq.session.profileId, profileId.id())) {
                    throw new NotFoundException("The given request could not be found.");
                }
                if(!notReq.session.state.isGetIdentityByNotary()) {
                    throw new InvalidRequestStateException(new SessionId(notReq.session.id), NotarizationRequestState.fetchIdentityNotaryStates, notReq.session.state);
                }

                return notReq.session.loadIdentities()
                        .map(r -> notReq.session.identities.stream()
                        .map(reqId -> IdentityView.from(reqId))
                        .collect(Collectors.toSet()));
        });

    }

    private NotarizationRequestState determineAcceptState(NotarizationRequest req, Profile profile) {
        if(!req.hasHolderAccess(profile)) {
            return NotarizationRequestState.PENDING_DID;
        } else if(req.session.manualRelease != null && req.session.manualRelease) {
            return NotarizationRequestState.PENDING_RQUESTOR_RELEASE;
        }
        return NotarizationRequestState.ACCEPTED;
    }

    private Uni<Void> handlePostAcceptance(NotarizationRequest request, Profile profile) {
        if (request.session.state == NotarizationRequestState.ACCEPTED) {
            return this.issuanceService.issue(request, profile)
                .chain((IssuanceResponse resp) -> {
                    var invitationUrl = resp.invitationURL;
                    if(invitationUrl != null){
                        request.ssiInvitationUrl = invitationUrl.toString();
                        return request.persist()
                            .replaceWithVoid()
                            .invoke(() -> this.notifications.onAccepted(new SessionId(request.session_id), resp.invitationURL.toString()))
                            ;
                    } else {
                        return Uni.createFrom().voidItem()
                            .invoke(() -> this.notifications.onAccepted(new SessionId(request.session_id)));
                    }
                });
        } else if(request.session.state == NotarizationRequestState.PENDING_DID) {
            this.notifications.onPendingDid(new SessionId(request.session_id));
        }
        return Uni.createFrom().voidItem();
    }

    @ReactiveTransactional
    public Uni<Void> revokeRequest(SecurityContext securityContext, JsonNode credential) throws URISyntaxException {

        var credentialStatus = credential.path("cred_value").path("credentialStatus");
        var listCredential = credentialStatus.get("statusListCredential").textValue();
        var idx = Long.valueOf(credentialStatus.get("statusListIndex").textValue());

        return revocationService.getProfileName(getRevocationListName(listCredential))
            .onItem().ifNull().failWith(()-> new NotFoundException("Entry for list"))
            .onItem().ifNotNull().transformToUni((profileName)-> {
                if(!securityContext.isUserInRole(profileName)){
                    throw new AuthorizationException("Revocation not allowed.");
                }
                return revocationService.revoke(profileName, idx)
                    .onFailure(ClientWebApplicationException.class)
                    .transform(e -> {
                        var cause = e.getCause();
                        if(cause instanceof WebApplicationException){
                            if(((WebApplicationException) cause).getResponse().getStatus() == 404){
                                return new NotFoundException("Index");
                            }
                        }
                        return e;
                    });
                }).replaceWithVoid();
        }

    private String getRevocationListName(String listCredential) throws URISyntaxException {
        var path = new URI(listCredential).getPath();
        return path.substring(path.lastIndexOf("/")+1, path.length());
    }

    private String createNonce(){
        return urlSafeString(secureRandom, new byte[64]);
    }

    private void addCallbacksTo(Session session) {
        var succesNoncedUrl = createSuccessURI();
        var failedNoncedUrl = createFailURI();

        session.successCBToken = succesNoncedUrl.nonce;
        session.successCBUri = succesNoncedUrl.uri.toString();

        session.failCBToken = failedNoncedUrl.nonce;
        session.failCBUri = failedNoncedUrl.uri.toString();
    }

    public record NoncedUri(URI uri, String nonce){}

    @ConfigProperty(name = "notarization-processing.internal-url")
    URI internalUrl;

    private NoncedUri createSuccessURI() {
        var nonce = createNonce();
        return new NoncedUri(
            UriBuilder.fromUri(internalUrl)
                .path(Api.Path.V1_PREFIX)
                .path(Api.Path.FINISH_NOTARIZATION_REQUEST)
                .path(nonce)
                .path(Api.Path.SUCCESS)
                .build(),
            nonce
        );
    }

    private NoncedUri createFailURI() {
        var nonce = createNonce();
        return new NoncedUri(
            UriBuilder.fromUri(internalUrl)
                .path(Api.Path.V1_PREFIX)
                .path(Api.Path.FINISH_NOTARIZATION_REQUEST)
                .path(nonce)
                .path(Api.Path.FAIL)
                .build(),
            nonce
        );
    }

    @ReactiveTransactional
    public Uni<Void> finishRequestSuccess(String nonce) {
        return Session.<Session>find(
            "successCBToken = ?1 and state = ?2",
            nonce,
            NotarizationRequestState.ACCEPTED
        )
        .firstResult()
        .onItem().ifNull().failWith(()->new NotFoundException("Session"))
        .onItem().ifNotNull().transformToUni(s -> {
            CallbackAuditingFilter.storeSessionId(request, s.id);
            s.state = NotarizationRequestState.ISSUED;
            return s.cleanup(taskManager, notifications);
        });
    }

    @ReactiveTransactional
    public Uni<Void> finishRequestFail(String nonce) {
        return Session.<Session>find(
            "failCBToken = ?1 and state = ?2",
            nonce,
            NotarizationRequestState.ACCEPTED
        )
        .firstResult()
        .onItem().ifNull().failWith(()->new NotFoundException("Session"))
        .onItem().ifNotNull().transformToUni(s -> {
            CallbackAuditingFilter.storeSessionId(request, s.id);
            s.state = NotarizationRequestState.TERMINATED;
            return s.cleanup(taskManager, notifications);
        });
    }

    @ReactiveTransactional
    public Uni<Void> assignCredentialAugmentation(ProfileId profileId, NotarizationRequestId id, JsonObject credentialAugmentation) {
        return NotarizationRequest.<NotarizationRequest>findById(id)
                .onItem().ifNull().failWith(() -> {
                    return new NotFoundException("NotarizationRequest");
                })
                .chain(req -> {
                    if (!Objects.equals(req.session.profileId, profileId.id())) {
                        throw new NotFoundException("The given request could not be found.");
                    }
                    if (!req.session.state.isAcceptableByNotary()) {
                        throw new InvalidRequestStateException(new SessionId(req.session.id), NotarizationRequestState.acceptByNotaryStates, req.session.state);
                    }

                    req.credentialAugmentation = credentialAugmentation;

                    return req.persist()
                            .replaceWithVoid();
                });
    }
}
