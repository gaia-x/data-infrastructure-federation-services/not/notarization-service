/*
 *
 */
package eu.gaiax.notarization.request_processing.infrastructure.stringtemplate;

import org.jboss.logging.Logger;
import org.stringtemplate.v4.STErrorListener;
import org.stringtemplate.v4.misc.STMessage;

/**
 *
 * @author Neil Crossley
 */
public class LastSTErrorListener implements STErrorListener {

    public static final Logger logger = Logger.getLogger(LastSTErrorListener.class);

    private STMessage last = null;

    @Override
    public void compileTimeError(STMessage msg) {
        last = msg;
    }

    @Override
    public void runTimeError(STMessage msg) {
        last = msg;
    }

    @Override
    public void IOError(STMessage msg) {
        last = msg;
    }

    @Override
    public void internalError(STMessage msg) {
        last = msg;
    }

    public STMessage lastError() {
        return last;
    }
}
