/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.mock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.domain.model.serialization.EnhanceSerializationCustomizer;
import io.quarkus.arc.profile.UnlessBuildProfile;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
@UnlessBuildProfile("prod")
public class MockProfileServer {

    private static final String basePath = "/api/v1/profiles";

    @Inject
    Logger logger;

    @ConfigProperty(name = "quarkus.rest-client.profile-api.url")
    URL givenProfileServiceUrl;

    WireMockServer wireMockServer;

    void startup(@Observes StartupEvent event) {
        logger.info("Preparing mock oauth2 server");
        wireMockServer = new WireMockServer(wireMockConfig().port(givenProfileServiceUrl.getPort()));

        var objectMapper = new ObjectMapper();
        new EnhanceSerializationCustomizer().customize(objectMapper);
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());

        try {
            wireMockServer.stubFor(get(urlPathEqualTo(basePath))
                    .withHeader("Accept", matching(MediaType.APPLICATION_JSON))
                    .willReturn(aResponse()
                            .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                            .withStatus(200)
                            .withBody(objectMapper.writeValueAsString(MockState.profiles))));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        try {
            for (Profile currentProfile : MockState.profiles) {
                wireMockServer.stubFor(get(urlPathEqualTo(basePath + "/" + URLEncoder.encode(currentProfile.id(), StandardCharsets.UTF_8.toString())))
                    .atPriority(1)
                    .withHeader("Accept", matching(MediaType.APPLICATION_JSON))
                    .willReturn(aResponse()
                            .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                            .withStatus(200)
                            .withBody(objectMapper.writeValueAsString(currentProfile))));
            }

        } catch (JsonProcessingException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

        wireMockServer.stubFor(get(urlMatching(basePath + "/.+"))
                .atPriority(10)
                .withHeader("Accept", matching(MediaType.APPLICATION_JSON))
                .willReturn(aResponse()
                        .withStatus(404)));

        wireMockServer.addMockServiceRequestListener(
                this::requestReceived);
        wireMockServer.start();
        logger.info("Prepared mock oauth2 server");
    }

    void onStop(@Observes ShutdownEvent ev) {
        logger.info("Stopping mock oauth2 server");
        wireMockServer.stop();
    }

    protected void requestReceived(Request inRequest, Response inResponse) {
        logger.debugv("Profile WireMock request at URL: {0}", inRequest.getAbsoluteUrl());
        logger.debugv("Profile WireMock request headers: \n{0}", inRequest.getHeaders());
        logger.debugv("Profile WireMock response body: \n{0}", inResponse.getBodyAsString());
        logger.debugv("Profile WireMock response headers: \n{0}", inResponse.getHeaders());
    }
}
