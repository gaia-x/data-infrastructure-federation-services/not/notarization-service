/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.entity;

import eu.gaiax.notarization.request_processing.application.taskprocessing.TaskManager;
import eu.gaiax.notarization.request_processing.domain.exception.InvalidProfileException;
import eu.gaiax.notarization.request_processing.domain.model.AccessToken;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.Profile;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.domain.model.ProfileTaskTree;
import eu.gaiax.notarization.request_processing.domain.model.SessionId;
import eu.gaiax.notarization.request_processing.domain.services.RequestNotificationService;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.smallrye.mutiny.Uni;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.reactive.mutiny.Mutiny;

/**
 *
 * @author Neil Crossley
 */
@Entity
@Table(name = "requestsession")
public class Session extends PanacheEntityBase {

    @Id
    public String id;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "session", cascade = CascadeType.ALL)
    public NotarizationRequest request;

    public String accessToken;

    public String profileId;

    public String identityToken;

    public Boolean manualRelease;
    public String manualReleaseToken;

    public NotarizationRequestState state;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "session")
    public Set<RequestorIdentity> identities;

    @CreationTimestamp
    public OffsetDateTime createdAt;
    @UpdateTimestamp
    public OffsetDateTime lastModified;

    @Version
    public OffsetDateTime version;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "session")
    public Set<SessionTask> tasks;

    @OneToMany(mappedBy = "session", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<Document> documents;

    public String successCBToken;
    public String failCBToken;
    public String successCBUri;
    public String failCBUri;

    public Session() {
    }

    public Session(String id) {
        this.id = id;
    }

    public static Uni<Session> createNew(SessionId id, AccessToken token, Profile profile) {

        var newSession = new Session();
        newSession.id = id.id;
        newSession.accessToken = token.token;
        newSession.profileId = profile.id();
        newSession.manualRelease = false;

        newSession.tasks = new HashSet<>();

        var persistTasks = buildSessionTasks(newSession, profile);
        if(persistTasks.isEmpty()){
            return newSession.persistAndFlush();
        }else{
            return Uni.combine().all().unis(persistTasks).discardItems()
                .chain(()-> newSession.persistAndFlush());
        }
    }

    private static Set<Uni<Void>> buildSessionTasks(Session newSession, Profile profile) {

        var preConTasks = profile.preconditionTasks();
        var tasks = profile.tasks();
        var taskDescriptionsByName = profile.taskDescriptions().stream()
                .collect(Collectors.toMap(td -> td.name(), td -> td));

        newSession.state = preConTasks.treeFulfilledBySession(newSession) ?
            NotarizationRequestState.SUBMITTABLE :
            NotarizationRequestState.CREATED;

        var taskNames = new HashSet<String>();
        extractTaskNames(preConTasks, taskNames);
        extractTaskNames(tasks, taskNames);

        return taskNames.stream()
            .map((e) -> {
                var desc = taskDescriptionsByName.get(e);
                if (desc == null) {
                    throw new InvalidProfileException(new ProfileId(profile.id()));
                }

                var type = desc.type();

                SessionTask sessTask = new SessionTask();
                sessTask.taskId = UUID.randomUUID();
                sessTask.fulfilled = false;
                sessTask.running = false;
                sessTask.name = desc.name();
                sessTask.type = type;
                sessTask.session = newSession;

                newSession.tasks.add(sessTask);

                return sessTask.persist().replaceWithVoid();
            })
            .collect(Collectors.toSet());
    }

    private static void extractTaskNames(ProfileTaskTree tree, Set<String> names){
        if(tree.taskName != null){
            names.add(tree.taskName);
        } else {
            tree.allOf.stream().forEach((t)->extractTaskNames(t, names));
            tree.oneOf.stream().forEach((t)->extractTaskNames(t, names));
        }
    }

    public static Session from(String token) {
        return new Session(token);
    }

    public static String valueOf(Session token) {
        return token == null ? null : token.id;
    }

    public static Uni<Session> findWithIdentities(SessionId sessionId){
        return Session.<Session>findById(sessionId.id).call((s)->Mutiny.fetch(s.identities));
    }

    public Uni<Session> loadIdentities() {
        return Mutiny.fetch(this.identities).onItem().transform((d) -> this);
    }

    @ReactiveTransactional
    public Uni<Void> cleanup(TaskManager taskManager, RequestNotificationService notifications){

        successCBToken = null;
        failCBToken = null;
        successCBUri = null;
        failCBUri = null;
        manualReleaseToken = null;

        return Uni.createFrom().voidItem()
            .chain(() -> this.persistAndFlush())
            .chain(() -> {
                return taskManager.cancelTasks(this)
                    .onFailure().recoverWithNull();
            })
            .chain(() -> Document.delete("session", this))
            .chain(() -> RequestorIdentity.delete("session", this))
            .chain(() -> NotarizationRequest.delete("session", this))
            .invoke(r -> notifications.onDeleted(new SessionId(this.id)))
            .replaceWithVoid();
    }
}
