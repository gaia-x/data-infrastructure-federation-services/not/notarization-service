/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.model.taskprocessing;

import com.fasterxml.jackson.annotation.JsonValue;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author Florian Otto
 */
@Schema(description = "The task types.",
        enumeration = {
            TaskType.Name.BROWSER_IDENTIFICATION_TASK,
            TaskType.Name.FILEPROVISION_TASK,
            TaskType.Name.VC_IDENTIFICATION_TASK
        })
public enum TaskType {

    BROWSER_IDENTIFICATION_TASK(Name.BROWSER_IDENTIFICATION_TASK),
    FILEPROVISION_TASK(Name.FILEPROVISION_TASK),
    VC_IDENTIFICATION_TASK(Name.VC_IDENTIFICATION_TASK);

    private final String value;

    TaskType(String value){
        this.value = value;
    }

    public static TaskType fromString(String s){
        if(s.equals(Name.BROWSER_IDENTIFICATION_TASK)){
            return BROWSER_IDENTIFICATION_TASK;
        } else if(s.equals(Name.FILEPROVISION_TASK)){
            return FILEPROVISION_TASK;
        } else if(s.equals(Name.VC_IDENTIFICATION_TASK)) {
            return VC_IDENTIFICATION_TASK;
        }
        else return VC_IDENTIFICATION_TASK;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }


    public static class Name {
            public static final String BROWSER_IDENTIFICATION_TASK = "browserIdentificationTask";
            public static final String FILEPROVISION_TASK = "fileProvisionTask";
            public static final String VC_IDENTIFICATION_TASK = "vcIdentificationTask";
    }
}
