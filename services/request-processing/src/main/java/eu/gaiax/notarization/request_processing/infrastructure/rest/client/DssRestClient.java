/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.client;

import javax.ws.rs.Path;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import io.smallrye.mutiny.Uni;
import java.util.List;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Neil Crossley
 */
@Path("")
@RegisterRestClient(configKey = "dss-api")
public interface DssRestClient {

    @POST
    @Path("/validation/validateSignature")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Uni<JsonValue> validateSignature(ValidateSignatureRequest request);

    public static class ValidateSignatureRequest {
        public Policy policy = null;
        public String tokenExtractionStrategy = "NONE";
        public String signatureId = null;
        public SignedDocument signedDocument;
        public List<OriginalDocument> originalDocuments;
    }

    public static class Policy {
        public String bytes = null;
        public String digestAlgorithm = null;
        public String name = null;
    }

    public static class SignedDocument {
        public String bytes = null;
        public String digestAlgorithm = null;
        public String name = null;
    }

    // The original file(s) in case of detached signature.
    public static class OriginalDocument {
        public byte[] bytes = null;
        public String digestAlgorithm = null;
        public String name = null;
    }

}
