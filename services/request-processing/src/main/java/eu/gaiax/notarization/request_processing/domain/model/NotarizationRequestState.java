/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.model;

import com.fasterxml.jackson.annotation.JsonValue;
import java.util.Set;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author Neil Crossley
 */
@Schema(description = """
                      The states of a notarization request.
                        * `created` - Created. Pre-condition tasks must be performed.
                        * `submittable` - Proposed credential information must be submitted. All tasks may be performed.
                        * `submittable` - Request must be marked ready. All tasks may be performed. Different credentials may be re-submitted.
                        * `readyForReview` - Request must be claimed by a notary. Readiness may be removed.
                        * `workInProgress` - Request is claimed and under review by a notary. Must be approved, rejected or deleted.
                        * `accepted` - Request was accepted by a notary. The issuance process has been started.
                        * `pendingDID` - Request was accepted by a notary. Missing credential holder details must be submitted to proceed.
                        * `pendingRequestorRelease` - Request was accepted by a notary. Manual release trigger must be performed by requestor.
                        * `issued` - The credentials have been issued. The request processing is terminated. Submitted data and files are deleted.
                        * `terminated` - The request processing is terminated. Submitted data and files are deleted.
                      """,
        readOnly = true,
        enumeration = {
            NotarizationRequestState.Name.CREATED,
            NotarizationRequestState.Name.SUBMITTABLE,
            NotarizationRequestState.Name.EDITABLE,
            NotarizationRequestState.Name.READY_FOR_REVIEW,
            NotarizationRequestState.Name.WORK_IN_PROGRESS,
            NotarizationRequestState.Name.ACCEPTED,
            NotarizationRequestState.Name.PENDING_DID,
            NotarizationRequestState.Name.TERMINATED,
            NotarizationRequestState.Name.ISSUED,
            NotarizationRequestState.Name.PENDING_RQUESTOR_RELEASE,
        })
public enum NotarizationRequestState {
    CREATED(Name.CREATED),
    SUBMITTABLE(Name.SUBMITTABLE),
    EDITABLE(Name.EDITABLE),
    READY_FOR_REVIEW(Name.READY_FOR_REVIEW),
    WORK_IN_PROGRESS(Name.WORK_IN_PROGRESS),
    ACCEPTED(Name.ACCEPTED),
    PENDING_DID(Name.PENDING_DID),
    ISSUED(Name.ISSUED),
    TERMINATED(Name.TERMINATED),
    PENDING_RQUESTOR_RELEASE(Name.PENDING_RQUESTOR_RELEASE);

    private final String value;

    NotarizationRequestState(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    public static class Name {

        public static final String CREATED = "created";
        public static final String SUBMITTABLE = "submittable";
        public static final String EDITABLE = "editable";
        public static final String READY_FOR_REVIEW = "readyForReview";
        public static final String WORK_IN_PROGRESS = "workInProgress";
        public static final String ACCEPTED = "accepted";
        public static final String PENDING_DID = "pendingDID";
        public static final String ISSUED = "issued";
        public static final String TERMINATED = "terminated";
        public static final String PENDING_RQUESTOR_RELEASE = "pendingRequestorRelease";

    }

    public static final Set<NotarizationRequestState> taskProcessingStates = Set.of(CREATED, SUBMITTABLE, EDITABLE);
    public static final Set<NotarizationRequestState> revokeableStates = Set.of(CREATED, EDITABLE, READY_FOR_REVIEW);
    public static final Set<NotarizationRequestState> updateableStates = Set.of(EDITABLE);
    public static final Set<NotarizationRequestState> markReadyStates = Set.of(EDITABLE);
    public static final Set<NotarizationRequestState> markUnreadyStates = Set.of(READY_FOR_REVIEW);
    public static final Set<NotarizationRequestState> uploadDocumentsStates = Set.of(EDITABLE);
    public static final Set<NotarizationRequestState> assignDidStates = Set.of(
        EDITABLE,
        READY_FOR_REVIEW,
        WORK_IN_PROGRESS,
        ACCEPTED,
        PENDING_DID
    );

    public boolean isTaskProcessingAllowed(){
        return taskProcessingStates.contains(this);
    }

    public boolean isRevokeable() {
        return revokeableStates.contains(this);
    }

    public boolean isUpdateable() {
        return updateableStates.contains(this);
    }

    public boolean isMarkReadyAllowed() {
        return markReadyStates.contains(this);
    }

    public boolean isMarkUnreadyAllowed() {
        return markUnreadyStates.contains(this);
    }

    public boolean isAssignDidAllowed() {
        return assignDidStates.contains(this);
    }

    public boolean isUploadDocumentAllowed() {
        return updateableStates.contains(this);
    }

    public static final Set<NotarizationRequestState> fetchByNotaryStates = Set.of(
            READY_FOR_REVIEW,
            WORK_IN_PROGRESS,
            ACCEPTED,
            PENDING_DID);
    public boolean isFetchableByNotary() {
        return fetchByNotaryStates.contains(this);
    }
    public static final Set<NotarizationRequestState> claimByNotaryStates = Set.of(READY_FOR_REVIEW);
    public boolean isClaimableByNotary() {
        return claimByNotaryStates.contains(this);
    }

    public static final Set<NotarizationRequestState> rejectByNotaryStates = Set.of(WORK_IN_PROGRESS);
    public boolean isRejectableByNotary() {
        return rejectByNotaryStates.contains(this);
    }

    public static final Set<NotarizationRequestState> deleteByNotaryStates = Set.of(WORK_IN_PROGRESS);
    public boolean isDeleteableByNotary() {
        return deleteByNotaryStates.contains(this);
    }

    public static final Set<NotarizationRequestState> acceptByNotaryStates = Set.of(WORK_IN_PROGRESS);
    public boolean isAcceptableByNotary() {
        return acceptByNotaryStates.contains(this);
    }

    public static final Set<NotarizationRequestState> fetchIdentityNotaryStates = Set.of(WORK_IN_PROGRESS);
    public boolean isGetIdentityByNotary() {
        return fetchIdentityNotaryStates.contains(this);
    }

    //States which can/cannot be deleted due to timeout
    public static final Set<NotarizationRequestState> statesAffectedBySubmissionTimeout = Set.of(
        CREATED,
        SUBMITTABLE
    );
    public static final Set<NotarizationRequestState> statesAffectedBySessionTimeout = Set.of(
        EDITABLE,
        PENDING_DID,
        PENDING_RQUESTOR_RELEASE
    );
}
