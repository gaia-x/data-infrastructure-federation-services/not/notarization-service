/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.domain.entity;

import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import java.time.OffsetDateTime;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Neil Crossley
 */
@Entity
@Immutable
@Table(name = "notarizationrequest_view")
public class NotarizationRequestDbView extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "session_id", updatable = false, nullable = false)
    public String sessionId;

    public String did;

    public String profileId;

    public NotarizationRequestState state;

    @Basic
    @Column(name = "request_id", insertable=false, updatable=false, unique=true)
    public UUID requestId;

    public String rejectComment;

    @Convert(converter = StringConverter.class)
    public String data;

    public String claimedBy;

    @Column(name = "total_documents", insertable=false, updatable=false, unique=true)
    public Integer totalDocuments;

    @CreationTimestamp
    public OffsetDateTime createdAt;
    @UpdateTimestamp
    public OffsetDateTime lastModified;
}
