/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest;

/**
 *
 * @author Neil Crossley
 */
public class Api {

    public static class Tags {

        public static final String SUBMISSION = "submission";
        public static final String DOCUMENT = "document";
        public static final String ROUTINES = "routines";
        public static final String MANAGEMENT = "management";
        public static final String FINISH_TASK = "finishTask";
        public static final String FINISH_NOTARIZATION_REQUEST = "finishNotarizationRequest";
    }

    public static class Path {

        public static final String PREFIX = "api";
        public static final String V1_PREFIX = PREFIX + "/v1";
        public static final String SUBMISSION = "submission";
        public static final String IDENTIFY = "identify";
        public static final String TASK = "task";
        public static final String FINISH_TASK = "finishTask";
        public static final String FINISH_NOTARIZATION_REQUEST = "finishNotarizationRequest";
        public static final String ISSUE_TRIGGER = "triggerIssuance";
        public static final String SESSION_RESOURCE = Api.Path.V1_PREFIX + "/session";
        public static final String DOCUMENT_RESOURCE = Api.Path.V1_PREFIX + "/document";
        public static final String ROUTINES_RESOURCE = Api.Path.V1_PREFIX + "/routines";
        public static final String ROUTINES_RESOURCE_PRUNE_TERMINATED_SESS = "/deleteTerminated";
        public static final String ROUTINES_RESOURCE_PRUNE_TIMEOUT_SESS = "/deleteTimeout";
        public static final String ROUTINES_RESOURCE_PRUNE_SUBMISIION_TIMEOUT_SESS = "/deleteSubmitTimeout";
        public static final String ROUTINES_RESOURCE_PRUNE_AUDIT_LOGS = "/auditLogs/clean";
        public static final String FINISH_TASK_RESOURCE = Api.Path.V1_PREFIX + "/" + FINISH_TASK;
        public static final String ISSUE_MANUALY = "/" + ISSUE_TRIGGER;
        public static final String FINISH_TASK_RESOURCE_WITH_NONCE = Api.Path.V1_PREFIX + "/" + FINISH_TASK + "/" + Param.NONCE_PARAM;
        public static final String FINISH_NOTARIZATION_REQUEST_RESOURCE_WITH_NONCE = Api.Path.V1_PREFIX + "/" + FINISH_NOTARIZATION_REQUEST+ "/" + Param.NONCE_PARAM;
        public static final String SUCCESS = "success";
        public static final String FAIL = "fail";
        public static final String DOCUP_BYLINK = "/uploadByLink";
        public static final String DOCUP_CONTENT = "/upload";
        public static final String CREDENTIAL_AUGMENTATION = "/credentialAugmentation";
    }

    public static class Param {
        public static final String DOCUMENTID = "documentId";
        public static final String RELEASE_TOKEN = "releaseToken";
        public static final String RELEASE_TOKEN_PARAM = "{"+RELEASE_TOKEN+"}";
        public static final String NOTARIZATION_REQUEST_DOCUMENT_ID_PARAM = "{" + DOCUMENTID+ "}";
        public static final String NOTARIZATION_REQUEST_ID = "notarizationRequestId";
        public static final String NOTARIZATION_REQUEST_ID_PARAM = "{" + NOTARIZATION_REQUEST_ID + "}";
        public static final String PROFILE_ID = "profileId";
        public static final String PROFILE_ID_PARAM = "{" + PROFILE_ID + "}";
        public static final String SESSION = "sessionId";
        public static final String TASKID = "taskId";
        public static final String SESSION_PARAM = "{" + SESSION + "}";
        public static final String TASKID_PARAM = "{" + TASKID + "}";
        public static final String TOKEN = "token";
        public static final String NONCE = "nonce";
        public static final String NONCE_PARAM = "{"+NONCE+"}";
    }

    public static class Header {

        public static final String ACCESS_TOKEN = "token";
    }

    public static class Role {

        public static final String NOTARY = "notary";
    }
    public static class Response {

        public static final String CANNOT_REUSE_TOKEN = "CannotReuseToken";
    }
}
