/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.client;

import com.fasterxml.jackson.databind.JsonNode;
import eu.gaiax.notarization.request_processing.domain.model.IssuanceResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import io.smallrye.mutiny.Uni;
import java.net.URI;
import java.time.Instant;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Neil Crossley
 */
@Path("/")
@RegisterRestClient(configKey = "ssi-issuance-api")
public interface SsiIssuanceRestClient {

    @POST
    @Path("credential/start-issuance/")
    @Consumes(MediaType.APPLICATION_JSON)
    Uni<IssuanceResponse> sendIssuanceRequest(IssuanceRequest request);

    public static class IssuanceRequest {
        public ProfileId profileID;
        public JsonNode credentialData;
        public Instant issuanceTimestamp;
        public String holderDID;
        public String invitationURL;
        public String successURL;
        public String failureURL;
    }

    public static class Document {
        public String hash;
        public URI location;
    }
}
