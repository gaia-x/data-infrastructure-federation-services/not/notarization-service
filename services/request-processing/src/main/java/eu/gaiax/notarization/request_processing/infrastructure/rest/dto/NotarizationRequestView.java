/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.time.OffsetDateTime;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.exception.BusinessException;

import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import eu.gaiax.notarization.request_processing.domain.model.DistributedIdentity;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestId;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import io.vertx.core.json.JsonObject;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Neil Crossley
 */
@Schema(description = "The overview of the notarization request")
public class NotarizationRequestView {
    @NotNull
    @Schema(readOnly = true)
    public NotarizationRequestId id;
    @NotNull
    @Schema(readOnly = true)
    public ProfileId profileId;
    @NotNull
    @Schema(readOnly = true)
    public OffsetDateTime createdAt;
    @NotNull
    @Schema(readOnly = true)
    public OffsetDateTime lastModified;
    @NotNull
    @Schema(readOnly = true, description = "The current processed state of the notarization request.")
    public NotarizationRequestState requestState;
    @NotNull
    @Schema(type = SchemaType.OBJECT,
            nullable = false,
            description = "The proposed content of the credential to be issued.")
    public JsonNode data;
    @Schema(description = "The target holder of the credentials to be issued.")
    public DistributedIdentity holder;
    @Schema(description = "Summary of uploaded evidence documents.")
    public Set<DocumentView> documents;
    @Schema(description = "The notarization request may be rejected by a notary. This is the given reason.")
    public String rejectComment;
    @Schema(description = "The structure assigned by the notary."
            + " This structure will be merged into the issued credential when the template in the profile is applied."
            + " This may extend or override the structure of the credential.")
    public JsonObject credentialAugmentation;

    public static NotarizationRequestView from(NotarizationRequest request, ObjectMapper objectMapper) throws BusinessException {
        try {
            var view = new NotarizationRequestView();
            view.id = new NotarizationRequestId(request.id);
            view.profileId = new ProfileId(request.session.profileId);
            view.createdAt = request.createdAt;
            view.lastModified = request.lastModified;
            view.requestState = request.session.state;
            view.data = objectMapper.readTree(request.data);
            view.holder = new DistributedIdentity(request.did);
            view.credentialAugmentation = request.credentialAugmentation;

            view.documents = request.session.documents.stream()
                .map(document -> DocumentView.fromDocument(document))
                .collect(Collectors.toSet());

            view.rejectComment = request.rejectComment;

            return view;
        }
        catch (JsonProcessingException e){
            throw new BusinessException("Invalid json data", e);
        }
    }

}
