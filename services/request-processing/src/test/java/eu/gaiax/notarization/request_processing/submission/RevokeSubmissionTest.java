/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.submission;

import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import javax.inject.Inject;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailFor;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import static eu.gaiax.notarization.request_processing.Helper.SESSION_VARIABLE;
import static eu.gaiax.notarization.request_processing.Helper.SUBMISSION_PATH;
import static eu.gaiax.notarization.request_processing.Helper.prepareSession;
import static eu.gaiax.notarization.request_processing.Helper.prepareSessionWithState;
import static eu.gaiax.notarization.request_processing.Helper.prepareSessionWithSubmittedNotarizationRequest;
import static eu.gaiax.notarization.request_processing.Helper.withTransaction;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.RequestorIdentity;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import static io.restassured.RestAssured.given;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class RevokeSubmissionTest {

    @Inject
    Mutiny.SessionFactory sessionFactory;

    /*
    Sub set of states allowing revocation
     */
    static Stream<NotarizationRequestState> revokeableStates() {
        return Stream.of(
            NotarizationRequestState.READY_FOR_REVIEW,
            NotarizationRequestState.EDITABLE,
            NotarizationRequestState.CREATED
        );
    }

    /*
    all others
     */
    static Stream<NotarizationRequestState> nonRevokeableStates() {
        var revokeable = revokeableStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !revokeable.contains(s));
    }

    @ParameterizedTest
    @MethodSource("revokeableStates")
    public void canRevokeRequestState(NotarizationRequestState state) {

        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);
        var session = sessionWithNotarizationRequest.session();

        var notarizationRequestDatabaseId = getNotaryRequestIdFromDb(sessionWithNotarizationRequest.session().id());
        var identityId = addIdentities(session.id());

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when().delete(SUBMISSION_PATH)
            .then()
            .statusCode(204);

        assertDeletionInDB(session.id(), notarizationRequestDatabaseId, identityId);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.REVOKE, 1, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogRevoke())
        );
    }

    @ParameterizedTest
    @MethodSource("nonRevokeableStates")
    public void cannotRevokeRequestState(NotarizationRequestState state) {
        var session = prepareSessionWithState(state, sessionFactory);
        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when().delete(SUBMISSION_PATH)
            .then()
            .statusCode(400)
            .body("actual", equalTo(state.toString()));

        assertExistanceInDB(session.id());

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.REVOKE, 1, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogRevoke().httpStatus(400))
        );
    }

    @Test
    public void noTokenIsDetected() {
        var session = prepareSession();
        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .when().delete(SUBMISSION_PATH)
            .then()
            .statusCode(400);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.REVOKE, 1, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogRevoke().httpStatus(400))
        );
    }

    @Test
    public void invalidTokenIsDetected() {
        var session = prepareSession();
        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", "INVALID_TOKEN")
            .when().delete(SUBMISSION_PATH)
            .then()
            .statusCode(401);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.REVOKE, 1, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogRevoke().httpStatus(401))
        );
    }

    @Test
    public void emptyTokenIsDetected() {
        var session = prepareSession();
        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", "")
            .when().delete(SUBMISSION_PATH)
            .then()
            .statusCode(401);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.REVOKE, 1, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogRevoke().httpStatus(401))
        );
    }

    private UUID getNotaryRequestIdFromDb(String sessionId){
        var res = withTransaction(sessionFactory, (session, tx) -> session.find(Session.class, sessionId));
        return res.request.id;

    }

    private UUID addIdentities(String sessionId){
        var identiyId = UUID.randomUUID();
        withTransaction(sessionFactory, (session, tx) -> {
            var q = session.<Session>createQuery("from Session s left join fetch s.identities where s.id = :id");
            q.setParameter("id", sessionId);
            return q.getSingleResult()
                .call(s -> {
                    var identiy = new RequestorIdentity();
                    identiy.id = identiyId;
                    identiy.session = s;
                    s.identities.add(identiy);
                    return session.persist(s);
                });
            });
        return identiyId;
    }

    private void assertDeletionInDB(String id, UUID notaryReqId, UUID identityId) {
        withTransaction(sessionFactory, (session, tx) -> {
            var q = session.<Session>createQuery("from Session s left join fetch s.identities where s.id = :id");
            q.setParameter("id", id);
            return q.getSingleResult()
                 .invoke(s -> {
                    assertThat(s.state, is(NotarizationRequestState.TERMINATED));
                    assertThat(s.identities,is(empty()));
                });

        });

        var identity = withTransaction(sessionFactory, (session, tx) -> {
            return session.find(RequestorIdentity.class, identityId);
        });
        assertThat(identity, nullValue());

        var notaryRequest = withTransaction(sessionFactory, (session, tx) -> {
            return session.find(NotarizationRequest.class, notaryReqId);
        });
        assertThat(notaryRequest, nullValue());


    }

    private void assertExistanceInDB(String id) {
        var res = withTransaction(sessionFactory, (session, tx) -> session.find(Session.class, id));

        assertThat(res.id, is(id));
    }

}
