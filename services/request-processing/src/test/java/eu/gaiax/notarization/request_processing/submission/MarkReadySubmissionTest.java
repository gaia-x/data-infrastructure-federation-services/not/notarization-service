/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.submission;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.matchingJsonPath;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.MockSsiIssuanceLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import eu.gaiax.notarization.SsiIssuanceWireMock;
import static eu.gaiax.notarization.request_processing.Helper.*;
import eu.gaiax.notarization.request_processing.domain.model.IssuanceResponse;
import eu.gaiax.notarization.request_processing.domain.model.MarkReadyResponse;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailFor;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.infrastructure.messaging.FrontEndMessageService;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;
import io.smallrye.reactive.messaging.providers.connectors.InMemorySink;
import java.net.MalformedURLException;
import java.net.URI;
import javax.enterprise.inject.Any;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import static org.hamcrest.Matchers.*;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(MockSsiIssuanceLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class MarkReadySubmissionTest {

    private static final String MARK_READY_PATH = SUBMISSION_PATH + "/ready";

    @Inject
    Mutiny.SessionFactory sessionFactory;

    @Inject
    @Any
    InMemoryConnector connector;

    @SsiIssuanceWireMock
    WireMockServer issuanceWireMock;

    @Inject
    ObjectMapper objectMapper;

    @ConfigProperty(name = "quarkus.http.port")
    Integer assignedPort;

    static Stream<NotarizationRequestState> allowedStates() {
        return Stream.of(
            NotarizationRequestState.EDITABLE
        );
    }

    static Stream<NotarizationRequestState> notAllowedStates() {
        var updateable = allowedStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !updateable.contains(s));
    }

    @Test
    public void canMarkReadyOnlyWithFulfilledTasks() {
        InMemorySink<String> results = connector.sink(FrontEndMessageService.outgoingOperatorRequestChanged);
        results.clear();

        var session = prepareSessionWithSubmittedNotarizationRequest(sessionFactory).session();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when()
            .post(MARK_READY_PATH)
            .then()
            .statusCode(400);

        //simulate ready tasks
        withTransaction(sessionFactory, (sess, tx) -> {
            return sess.createQuery("update SessionTask st set st.fulfilled = true").executeUpdate();

        });

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when()
            .post(MARK_READY_PATH)
            .then()
            .statusCode(200);

        assertStateInStoredSession(session.id(), sessionFactory, NotarizationRequestState.READY_FOR_REVIEW);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.MARK_READY, 2, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogMarkReady())
        );

        assertThat(results.received(), is(not(empty())));
    }

    @ParameterizedTest
    @MethodSource("notAllowedStates")
    public void cannotMarkReady(NotarizationRequestState state) {

        var session = prepareSessionWithSubmittedNotarizationRequest(state, sessionFactory)
                .session();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken())
            .when()
            .post(MARK_READY_PATH)
            .then()
            .statusCode(400);

        assertStateInStoredSession(session.id(), sessionFactory, state);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.MARK_READY, 1, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogMarkReady().httpStatus(400))
        );

    }

    @Test
    public void invalidTokenDetectedInMarkReadyRequest() {

        var session = prepareSessionWithSubmittedNotarizationRequest(NotarizationRequestState.EDITABLE, sessionFactory)
                .session();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .header("token", session.accessToken() + "INVALID")
            .when()
            .post(MARK_READY_PATH)
            .then()
            .statusCode(401);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.MARK_READY, 1, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogMarkReady().httpStatus(401))
        );
    }

    @Test
    public void noTokenDetectedInMarkReadyRequest() {

        var session = prepareSessionWithSubmittedNotarizationRequest(NotarizationRequestState.EDITABLE, sessionFactory)
                .session();

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.id())
            .when()
            .post(MARK_READY_PATH)
            .then()
            .statusCode(400);

        assertThat(auditTrailFor(session.id(), NotarizationRequestAction.MARK_READY, 1, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogMarkReady().httpStatus(400))
        );
    }

    @Test
    public void emptyTokenDetectedInMarkReadyRequest() {

        var session = prepareSessionWithSubmittedNotarizationRequest(NotarizationRequestState.EDITABLE, sessionFactory);

        given().contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.session().id())
            .header("token", "")
            .when()
            .post(MARK_READY_PATH)
            .then()
            .statusCode(401);

        assertThat(auditTrailFor(session.session().id(), NotarizationRequestAction.MARK_READY, 1, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogMarkReady().httpStatus(401))
        );

    }

    public static final String ACCEPT_PATH = "/api/v1/profiles/{profileId}/requests/{notarizationRequestId}/accept";
    @Test
    public void canReleaseAcceptedRequest() throws MalformedURLException {
        InMemorySink<String> results = connector.sink(FrontEndMessageService.outgoingOperatorRequestChanged);
        results.clear();


        var pId = new ProfileId(MockState.profile1.id());
        var session = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, pId);

        //simulate ready tasks
        withTransaction(sessionFactory, (sess, tx) -> {
            return sess.createQuery("update SessionTask st set st.fulfilled = true").executeUpdate();
        });

        var resp = given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.session().id())
            .header("token", session.session().accessToken())
            .queryParam("manualRelease", true)
            .when()
            .post(MARK_READY_PATH)
            .then()
            .statusCode(200)
            .extract().body().as(MarkReadyResponse.class);

        var url = fixUrl(resp.releaseUrl());

        //url returns not found until notary accepts
        given()
            .when()
            .post(url)
            .then()
            .statusCode(404)
            ;

        //simulate claiming and accepting
        var reqId = withTransaction(sessionFactory, (sess, tx) -> {
            var q = sess.createQuery("select id from NotarizationRequest where session_id = :sid");
            q.setParameter("sid", session.session().id());
            return q.getSingleResult();
        });

        setSessionState(session.session(), NotarizationRequestState.WORK_IN_PROGRESS, sessionFactory);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", reqId)
            .pathParam("profileId", pId.id())
            .when()
            .post(ACCEPT_PATH)
            .then()
            .statusCode(204);


        given()
            .when()
            .post(url)
            .then()
            .statusCode(204)
            ;

        issuanceWireMock.verify(
            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
            .withRequestBody(matchingJsonPath("$.successURL", WireMock.containing("finishNotarizationRequest")))
        );

        assertStateInStoredSession(session.session().id(), sessionFactory, NotarizationRequestState.ACCEPTED);

        assertThat(results.received(), is(not(empty())));

        assertThat(auditTrailFor(session.session().id(), NotarizationRequestAction.MANUAL_RELEASE, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogManualRel())
        );

    }

    private IssuanceResponse getResp(){
        var resp = new IssuanceResponse();
        resp.invitationURL = URI.create("http://a.url");
        return resp;
    }
    @Test
    public void canReleaseAcceptedRequestGettingInviteFromSsiIssuanceService() throws MalformedURLException, JsonProcessingException {
        InMemorySink<String> results = connector.sink(FrontEndMessageService.outgoingOperatorRequestChanged);
        results.clear();

        issuanceWireMock.stubFor(
                post(urlMatching("/credential/start-issuance/"))
                    .willReturn(
                        aResponse()
                            .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                            .withStatus(200)
                            .withBody(objectMapper.writeValueAsString(getResp())))
            );



        var pId = new ProfileId(MockState.profile1.id());
        var session = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, pId);

        //simulate ready tasks
        withTransaction(sessionFactory, (sess, tx) -> {
            return sess.createQuery("update SessionTask st set st.fulfilled = true").executeUpdate();
        });

        var resp = given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.session().id())
            .header("token", session.session().accessToken())
            .queryParam("manualRelease", true)
            .when()
            .post(MARK_READY_PATH)
            .then()
            .statusCode(200)
            .extract().body().as(MarkReadyResponse.class);

        var url = fixUrl(resp.releaseUrl());

        //url returns not found until notary accepts
        given()
            .when()
            .post(url)
            .then()
            .statusCode(404)
            ;

        //simulate claiming and accepting
        var reqId = withTransaction(sessionFactory, (sess, tx) -> {
            var q = sess.createQuery("select id from NotarizationRequest where session_id = :sid");
            q.setParameter("sid", session.session().id());
            return q.getSingleResult();
        });

        setSessionState(session.session(), NotarizationRequestState.WORK_IN_PROGRESS, sessionFactory);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", reqId)
            .pathParam("profileId", pId.id())
            .when()
            .post(ACCEPT_PATH)
            .then()
            .statusCode(204);


        given()
            .when()
            .post(url)
            .then()
            .statusCode(204)
            ;

        issuanceWireMock.verify(
            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
            .withRequestBody(matchingJsonPath("$.successURL", WireMock.containing("finishNotarizationRequest")))
        );

        assertStateInStoredSession(session.session().id(), sessionFactory, NotarizationRequestState.ACCEPTED);

        assertThat(results.received(), is(not(empty())));

        assertThat(auditTrailFor(session.session().id(), NotarizationRequestAction.MANUAL_RELEASE, sessionFactory),
            hasAuditEntries(AuditTrailMatcher.auditLogManualRel())
        );

        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, session.session().id())
            .header("token", session.session().accessToken())
            .when()
            .get(SUBMISSION_PATH + "/ssiInviteUrl")
            .then()
            .statusCode(200)
            .body("inviteUrl", is("http://a.url"))
            ;
    }
    private String fixUrl(URI url) throws MalformedURLException {
        return UriBuilder.fromUri(url).port(assignedPort).build().toURL().toString();
    }

}
