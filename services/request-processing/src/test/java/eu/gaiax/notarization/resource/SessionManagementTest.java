/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.resource;

import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import static eu.gaiax.notarization.request_processing.Helper.withTransaction;
import java.util.UUID;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;

/**
 *
 * @author Neil Crossley
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class SessionManagementTest {

    private static final String SESSION_VARIABLE = "sessionId";
    private static final String SESSION_PATH = "/api/v1/session/{sessionId}";

    @Inject
    Mutiny.SessionFactory sessionFactory;

    @Test
    public void canCreateSession() {
        var response = given()
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .body(String.format("""
                                    {"profileId":"%s"}
                                    """, MockState.someProfileId.id()))
                .when().post("/api/v1/session")
                .then()
                .statusCode(201)
                .header("Location", containsString("/api/v1/session/"))
                .body("token", is(instanceOf(String.class)))
                .body("sessionId", is(instanceOf(String.class)))
                .extract();

        var actualLocation = response.header("Location");
        var actualToken = response.path("sessionId").toString();
        assertThat(actualLocation, endsWith(actualToken));
    }

    @Test
    public void canFetchCreatedSession() {
        var sessionInfo = createNewSession();

        given()
                .accept(ContentType.JSON)
                .header("token", sessionInfo.accessToken)
                .when().get(sessionInfo.location)
                .then()
                .statusCode(200)
                .body(
                   "sessionId", equalTo(sessionInfo.id),
                   "tasks", hasSize(greaterThan(0))
                );
    }

    static Stream<Arguments> allNamedStates() {
        return Stream.of(
                Arguments.of(NotarizationRequestState.ACCEPTED, NotarizationRequestState.Name.ACCEPTED),
                Arguments.of(NotarizationRequestState.EDITABLE, NotarizationRequestState.Name.EDITABLE),
                Arguments.of(NotarizationRequestState.ISSUED, NotarizationRequestState.Name.ISSUED),
                Arguments.of(NotarizationRequestState.TERMINATED, NotarizationRequestState.Name.TERMINATED),
                Arguments.of(NotarizationRequestState.PENDING_DID, NotarizationRequestState.Name.PENDING_DID),
                Arguments.of(NotarizationRequestState.READY_FOR_REVIEW, NotarizationRequestState.Name.READY_FOR_REVIEW),
                Arguments.of(NotarizationRequestState.SUBMITTABLE, NotarizationRequestState.Name.SUBMITTABLE),
                Arguments.of(NotarizationRequestState.WORK_IN_PROGRESS, NotarizationRequestState.Name.WORK_IN_PROGRESS)
        );
    }

    @ParameterizedTest
    @MethodSource("allNamedStates")
    public void givenSessionThenFetchedStateIsCorrect(NotarizationRequestState given, String expected) {
        var givenSession = SessionIdentifier.any();
        createSessionWithState(givenSession, given);

        given().contentType(ContentType.JSON)
                .pathParam(SESSION_VARIABLE, givenSession.id)
                .header("token", givenSession.accessToken)
                .when().get(SESSION_PATH)
                .then()
                .statusCode(200)
                .body("state", equalTo(expected),
                        "sessionId", equalTo(givenSession.id));
    }

    private void createSessionWithState(SessionIdentifier sessionId, NotarizationRequestState state) {
        withTransaction(this.sessionFactory, (session, tx) -> {
            Session newSession = new Session();
            newSession.id = sessionId.id;
            newSession.profileId = MockState.someProfileId.id();
            newSession.accessToken = sessionId.accessToken;
            newSession.state = state;

            return session.persist(newSession);
        });
    }

    private SessionInfo createNewSession() {
        var response = given()
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .body(String.format("""
                                    {"profileId":"%s"}
                                    """, MockState.someProfileId.id()))
                .when().post("/api/v1/session")
                .then()
                .statusCode(201).extract();

        return new SessionInfo(response.path("sessionId"),
                response.header("Location"),
                response.path("token"));
    }

    public static record SessionInfo(
            String id, String location, String accessToken) {
    }

    public static record SessionIdentifier(
            String id, String accessToken) {

        public static SessionIdentifier any() {
            return new SessionIdentifier(
                    UUID.randomUUID().toString(),
                    UUID.randomUUID().toString());
        }
    }
}
