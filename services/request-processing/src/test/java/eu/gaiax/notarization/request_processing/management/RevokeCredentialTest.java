/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.management;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.deleteRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.delete;
import static com.github.tomakehurst.wiremock.client.WireMock.noContent;
import static com.github.tomakehurst.wiremock.client.WireMock.notFound;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import io.quarkus.test.common.QuarkusTestResource;
import javax.inject.Inject;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import javax.ws.rs.core.MediaType;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Test;
import eu.gaiax.notarization.MockRevocationResource;
import eu.gaiax.notarization.RevocationWireMock;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.ListMapping;
import java.util.List;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@Tag("security")
@QuarkusTestResource(MockRevocationResource.class)
public class RevokeCredentialTest {

    public static Logger logger = Logger.getLogger(RevokeCredentialTest.class);
    public static final String PATH = "/api/v1/revoke";

    @Inject
    ObjectMapper objectMapper;

    @Inject
    Mutiny.SessionFactory sessionFactory;

    @RevocationWireMock
    WireMockServer mockRevocation;

    private ListMapping createLM(String profileName, String listName){
        var lm = new ListMapping();
        lm.profileName = profileName;
        lm.listName = listName;
        return lm;
    }

    protected void requestReceived(Request inRequest, Response inResponse, String serviceName) {
        logger.debugv(" WireMock stub {1} request at URL: {0}", inRequest.getAbsoluteUrl(), serviceName);
        logger.debugv(" WireMock stub {1} request headers: \n{0}", inRequest.getHeaders(), serviceName);
        logger.debugv(" WireMock stub {1} request body: \n{0}", inRequest.getBodyAsString(), serviceName);
        logger.debugv(" WireMock stub {1} response body: \n{0}", inResponse.getBodyAsString(), serviceName);
        logger.debugv(" WireMock stub {1} response headers: \n{0}", inResponse.getHeaders(), serviceName);
    }

    private void stubConfigLists(List<ListMapping> listMapping) {

        mockRevocation.resetAll();

        var body = "";
        try {
            body = objectMapper.writeValueAsString(listMapping);
        } catch (Exception e){
            throw new RuntimeException(e);
        }

        mockRevocation.addMockServiceRequestListener(
                (in, out) -> requestReceived(in, out, "revocation"));
        mockRevocation.stubFor(get(urlMatching(".*lists"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                        .withBody(body)
                        .withStatus(200)
                )
        );

        mockRevocation.stubFor(delete(urlMatching(".*entry/-.+"))
                .willReturn(notFound())
        );
        mockRevocation.stubFor(delete(urlMatching(".*entry/[^-]+"))
                .willReturn(noContent())
        );

    }


    @Test
    public void profileNotFoundReturnsNotFound(){
        stubConfigLists(List.of());
        var givenNotary = MockState.notary1;

        given()
            .header("Authorization", givenNotary.bearerValue())
            .header("Content-type", MediaType.APPLICATION_JSON)
            .body("""
                {
                    "cred_value": {
                        "credentialStatus": {
                            "statusListIndex": "94567",
                            "statusListCredential": "https://revocation.example.com/status/unknownListName"
                        }
                    }
                }""")
            .when()
            .post(PATH)
            .then()
            .statusCode(404)
            ;
    }

    @Test
    public void foundProfileWithNoPermissionReturnsAuthenticateError(){
        stubConfigLists(List.of(
            createLM("aProfileNotOwnedByNotary", "noPermission")
        ));
        var givenNotary = MockState.notary1;

        given()
            .header("Authorization", givenNotary.bearerValue())
            .header("Content-type", MediaType.APPLICATION_JSON)
            .body("""
                {
                    "cred_value": {
                        "credentialStatus": {
                            "statusListIndex": "94567",
                            "statusListCredential": "https://revocation.example.com/status/noPermission"
                        }
                    }
                }""")
            .when()
            .post(PATH)
            .then()
            .statusCode(401)
            ;
    }

    @Test
    public void notaryWithAccessCanCallRevoke(){
        stubConfigLists(List.of(
            createLM(MockState.profile1.id(), MockState.profile1.id()+"_lst")
        ));
        var givenNotary = MockState.notary1;

        given()
            .header("Authorization", givenNotary.bearerValue())
            .header("Content-type", MediaType.APPLICATION_JSON)
            .body(String.format("""
                {
                    "cred_value": {
                        "credentialStatus": {
                            "statusListIndex": "94567",
                            "statusListCredential": "https://revocation.example.com/status/%s"
                        }
                    }
                }""", MockState.profile1.id()+"_lst"))
            .when()
            .post(PATH)
            .then()
            .statusCode(204)
            ;

        this.mockRevocation.verify(
            deleteRequestedFor(urlMatching("/management/lists/" + MockState.profile1.id() + "/entry/94567")));
    }

    @Test
    public void wrongIndexGetsNotFound(){
        stubConfigLists(List.of(
            createLM(MockState.profile1.id(), MockState.profile1.id()+"_lst")
        ));
        var givenNotary = MockState.notary1;

        given()
            .header("Authorization", givenNotary.bearerValue())
            .header("Content-type", MediaType.APPLICATION_JSON)
            .body(String.format("""
                {
                    "cred_value": {
                        "credentialStatus": {
                            "statusListIndex": "-1",
                            "statusListCredential": "https://revocation.example.com/status/%s"
                        }
                    }
                }""", MockState.profile1.id()+"_lst"))
            .when()
            .post(PATH)
            .then()
            .statusCode(404)
            ;
    }
}
