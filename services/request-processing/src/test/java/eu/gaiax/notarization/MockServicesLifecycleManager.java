/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Map;
import javax.ws.rs.core.UriBuilder;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
public class MockServicesLifecycleManager implements QuarkusTestResourceLifecycleManager {

    private static final Logger logger = Logger.getLogger(MockServicesLifecycleManager.class);

    int mockProfilePort = 0;
    int mockOAuth2Port = 0;

    @Override
    public Map<String, String> start() {
        mockProfilePort = findFreePort();
        mockOAuth2Port = findFreePort();

        var introspectionUrl = UriBuilder.fromUri("http://localhost:9194/realms/notarization-realm/protocol/openid-connect/token/introspect")
                .port(mockOAuth2Port).build();
        var profileUrl = UriBuilder.fromUri("http://localhost")
                .port(mockProfilePort).build();

        logger.infov("Assigning mock ports {0} {1}", mockProfilePort, mockOAuth2Port);

        return Map.of("quarkus.oauth2.introspection-url", introspectionUrl.toString(),
                "quarkus.rest-client.profile-api.url", profileUrl.toString());
    }

    @Override
    public void stop() {
        releasePort(this.mockProfilePort);
        releasePort(this.mockOAuth2Port);
    }

    private static int findFreePort() {
        int port = 0;
        // For ServerSocket port number 0 means that the port number is automatically allocated.
        try ( ServerSocket socket = new ServerSocket(0)) {
            // Disable timeout and reuse address after closing the socket.
            socket.setReuseAddress(true);
            port = socket.getLocalPort();
        } catch (IOException ignored) {
        }
        if (port > 0) {
            return port;
        }
        throw new RuntimeException("Could not find a free port");
    }

    private static void releasePort(int port) {
        if (port <= 0) {
            return;
        }
        try ( ServerSocket socket = new ServerSocket(port)) {
            // No longer enabling socket reuse.
            socket.setReuseAddress(false);
        } catch (IOException ignored) {
        }
    }
}
