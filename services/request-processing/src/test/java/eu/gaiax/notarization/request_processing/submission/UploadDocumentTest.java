/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.submission;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;

import java.net.URISyntaxException;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.google.common.io.CharStreams;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import eu.gaiax.notarization.request_processing.DataGen;
import static eu.gaiax.notarization.request_processing.Helper.*;

import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;

import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.DocumentId;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.services.SignatureVerifierService;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.DocumentUploadByLink;
import eu.gaiax.notarization.request_processing.infrastructure.rest.dto.SessionTaskSummary;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditFetchDocument;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailFor;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditUploadDocument;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import static eu.gaiax.notarization.request_processing.matcher.FieldMatcher.hasField;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.smallrye.mutiny.Uni;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Base64;
import java.util.UUID;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import static org.mockito.ArgumentMatchers.argThat;
/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class UploadDocumentTest {

    public static final Logger logger = Logger.getLogger(UploadDocumentTest.class);

    private static WireMockServer downloadServer;
    private static String downloadServerBaseUrl;

    ObjectMapper objectMapper = new ObjectMapper();
    @Inject
    Mutiny.SessionFactory sessionFactory;
    Base64.Encoder encoder = Base64.getUrlEncoder();

    @InjectMock
    SignatureVerifierService mockSignatureVerifier;

    public static final String DOCUMENTS_PATH = "/api/v1/session/{sessionId}/submission/documents";
    public static final String START_TASK_PATH = "/api/v1/session/{sessionId}/task";

    @BeforeAll
    public static void init() {
        downloadServer = new WireMockServer(wireMockConfig().dynamicPort());
        downloadServer.start();
        downloadServerBaseUrl = downloadServer.baseUrl();
    }

    @AfterAll
    public static void cleanUp() {
        if (downloadServer != null) {
            downloadServer.stop();
        }
    }

    @BeforeEach
    public void setup() {
        downloadServer.resetAll();
        downloadServer.stubFor(get(urlMatching(".+"))
                .atPriority(100)
                .willReturn(aResponse()
                        .withStatus(404)));

        downloadServer.addMockServiceRequestListener(
                this::requestReceived);
    }
    protected void requestReceived(Request inRequest, Response inResponse) {
        logger.debugv("Download WireMock request at URL: {0}", inRequest.getAbsoluteUrl());
        logger.debugv("Download WireMock request headers: \n{0}", inRequest.getHeaders());
        logger.debugv("Download WireMock response headers: \n{0}", inResponse.getHeaders());
        logger.debugv("Download WireMock response body: \n{0}", inResponse.getBodyAsString());
    }

    public static DocumentBuilder someDownloadableDocument() {
        return new DocumentBuilder(createDocument(), "here is content", false).isDownloadable();
    }

    public static DocumentBuilder someDocument() {
        return new DocumentBuilder(createDocument(), null, false);
    }

    private static DocumentUploadByLink createDocument() {
        var result = new DocumentUploadByLink();
        result.id = new DocumentId(UUID.randomUUID());
        result.location = URI.create(downloadServerBaseUrl + "/" + UUID.randomUUID().toString() + "/");
        result.title = DataGen.genString(30);
        result.mimetype = DataGen.genString(10);
        result.extension = DataGen.genString(10);
        result.longDescription = DataGen.genString(60);
        result.shortDescription = DataGen.genString(10);

        return result;
    }
    public static class DocumentBuilder {

        private final DocumentUploadByLink document;
        private final String content;
        private final boolean isDownloadable;

        public DocumentBuilder(DocumentUploadByLink document, String content, boolean isDownloadable) {
            this.document = document;
            this.content = content;
            this.isDownloadable = isDownloadable;
        }

        public DocumentUploadByLink build() {
            if (isDownloadable) {
                String actualContent = content != null ? content : DataGen.createRandomJsonData().asText();
                downloadServer.stubFor(get(urlEqualTo(document.location.getPath()))
                        .atPriority(1)
                        .willReturn(aResponse()
                                .withBody(actualContent)
                                .withStatus(200)));
            }
            return document;
        }

        public DocumentBuilder isDownloadable() {
            return new DocumentBuilder(document, content, true);
        }

        public DocumentBuilder unreachable() {
            return new DocumentBuilder(document, content, false);
        }

        public DocumentBuilder withContent(String content) {
            return new DocumentBuilder(document, content, isDownloadable).isDownloadable();
        }

    }
    private static String someRawDocument() {
        return DataGen.createRandomJsonData().toString();
    }

    private static String someVerificationReport() {
        return RandomStringUtils.randomAlphabetic(512);
    }

    private static InputStream matchesInputStream(byte[] expected) {
        return argThat(input -> matchesExpected(input, new String(expected)));
    }
    private static InputStream matchesInputStream(String expected) {
        return argThat(input -> matchesExpected(input, expected));
    }

    private static boolean matchesExpected(InputStream inputStream, String expectedData) throws RuntimeException {
        try {
            if (inputStream == null) {
                return expectedData == null;
            }
            String actual = CharStreams.toString(new InputStreamReader(inputStream));
            inputStream.reset();

            return actual.equals(expectedData);
        } catch(IOException ex) {
            throw new RuntimeException("Could not match on the stream", ex);
        }
    }


    @Test
    public void canUploadDocument(@TempDir Path baseTempDir) throws JsonProcessingException, URISyntaxException, IOException {

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);
        var expectedReport = someVerificationReport();

        UploadDescription inputUpload = textFile(baseTempDir);

        Mockito.when(mockSignatureVerifier.verify(matchesInputStream(Files.readAllBytes(inputUpload.content))))
                .thenReturn(Uni.createFrom().item(expectedReport.getBytes()));

        var sessionInfo = sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(objectMapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();

        var token = sessionWithNotarizationRequest.session().accessToken();

        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", token)
            .queryParam("taskId", task.taskId)
            .when()
            .post(START_TASK_PATH)
            .then()
            .statusCode(201);

        //upload document via form
        given().spec(inputUpload.asSpec())
                .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
                .pathParam("taskId", task.taskId)
                .header("token", token)
                .when()
                .post("/api/v1/document/{sessionId}/{taskId}/upload")
                .then()
                .statusCode(204);

        //call finish
        given()
            .pathParam("taskId", task.taskId)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", token)
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/finishTask")
            .then()
            .statusCode(204)
            ;

        assertDocumentInDb(sessionWithNotarizationRequest.session(), inputUpload, encoder.encodeToString(expectedReport.getBytes()));

        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.UPLOAD_DOCUMENT, 1, sessionFactory),
            hasAuditEntries(auditUploadDocument())
        );
    }

    @Test
    public void canNotUploadDocumentWithoutStartingTask(@TempDir Path baseTempDir) throws JsonProcessingException, URISyntaxException, IOException {

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);

        UploadDescription inputUpload = textFile(baseTempDir);

        var sessionInfo =sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(objectMapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();

        var token = sessionWithNotarizationRequest.session().accessToken();

        //upload document via form
        given()
            .spec(inputUpload.asSpec())
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .pathParam("taskId", task.taskId)
            .header("token", token)
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/upload")
            .then()
            .statusCode(404)
            ;
    }

    @Test
    public void canNOTUploadDocumentWithWrongToken(@TempDir Path baseTempDir) throws JsonProcessingException, URISyntaxException {

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);

        UploadDescription inputUpload = textFile(baseTempDir);

        var sessionInfo =sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(objectMapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();

        var token = sessionWithNotarizationRequest.session().accessToken();

        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", token)
            .queryParam("taskId", task.taskId)
            .when()
            .post(START_TASK_PATH)
            .then()
            .statusCode(201);

        token = "INVALID";

        //upload document via form
        given()
            .spec(inputUpload.asSpec())
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .pathParam("taskId", task.taskId)
            .header("token", token)
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/upload")
            .then()
            .statusCode(401)
            ;

    }

    @Test
    public void canUploadDocumentByLink() throws JsonProcessingException, URISyntaxException {

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);

        var givenRawDocument = someRawDocument();
        var expectedReport = someVerificationReport();

        Mockito.when(mockSignatureVerifier.verify(matchesInputStream(givenRawDocument)))
                .thenReturn(Uni.createFrom().item(expectedReport.getBytes()));

        DocumentUploadByLink docUp = someDownloadableDocument().withContent(givenRawDocument).build();

        var sessionInfo =sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(objectMapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();

        //startTask
        given()
            .header("Content-Type", MediaType.APPLICATION_JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam("taskId", task.taskId)
            .when()
            .post(START_TASK_PATH)
            .then()
            .statusCode(201);


        //upload document via link
        given()
            .header("Content-Type", MediaType.APPLICATION_JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionInfo.accessToken())
            .pathParam("taskId", task.taskId)
            .body(docUp)
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/uploadByLink")
            .then()
            .statusCode(204)
            ;

        //call finish
        given()
            .pathParam("taskId", task.taskId)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionInfo.accessToken())
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/finishTask")
            .then()
            .statusCode(204)
            ;

        assertDocumentInDb(sessionWithNotarizationRequest.session(), docUp,givenRawDocument.getBytes(), encoder.encodeToString(expectedReport.getBytes()));

assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.UPLOAD_DOCUMENT, 1, sessionFactory),
            hasAuditEntries(auditUploadDocument())
);
    }


    @Test
    public void handleUnreachableDownloads() throws JsonProcessingException, URISyntaxException {

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);


        var givenRawDocument = someRawDocument();
        var expectedReport = someVerificationReport();

        Mockito.when(mockSignatureVerifier.verify(matchesInputStream(givenRawDocument)))
                .thenReturn(Uni.createFrom().item(expectedReport.getBytes()));

        DocumentUploadByLink docUp = someDownloadableDocument().withContent(givenRawDocument).unreachable().build();

        var sessionInfo =sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(objectMapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();

        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam("taskId", task.taskId)
            .when()
            .post(START_TASK_PATH)
            .then()
            .statusCode(201);


        //upload document via link
        given()
            .header("Content-Type", MediaType.APPLICATION_JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionInfo.accessToken())
            .pathParam("taskId", task.taskId)
            .body(docUp)
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/uploadByLink")
            .then()
            .statusCode(400)
            ;

    }

    @Test
    public void canNOTUploadDocumentByLinkWithWrongToken() throws JsonProcessingException, URISyntaxException {

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);


        var givenRawDocument = someRawDocument();
        var expectedReport = someVerificationReport();

        Mockito.when(mockSignatureVerifier.verify(matchesInputStream(givenRawDocument)))
                .thenReturn(Uni.createFrom().item(expectedReport.getBytes()));

        DocumentUploadByLink docUp = someDownloadableDocument().withContent(givenRawDocument).build();

        var sessionInfo =sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(objectMapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();


        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam("taskId", task.taskId)
            .when()
            .post(START_TASK_PATH)
            .then()
            .statusCode(201);


        //upload document via link
        given()
            .header("Content-Type", MediaType.APPLICATION_JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", "INVALID")
            .pathParam("taskId", task.taskId)
            .body(docUp)
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/uploadByLink")
            .then()
            .statusCode(401)
            ;

    }

    @Test
    public void canFetchUploadedDocument(@TempDir Path baseTempDir) throws JsonProcessingException, URISyntaxException, IOException {

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);
        var expectedReport = someVerificationReport();

        UploadDescription inputUpload = textFile(baseTempDir);

        Mockito.when(mockSignatureVerifier.verify(matchesInputStream(Files.readAllBytes(inputUpload.content))))
                .thenReturn(Uni.createFrom().item(expectedReport.getBytes()));

        var sessionInfo =sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(objectMapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();

        var token = sessionWithNotarizationRequest.session().accessToken();
        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", token)
            .queryParam("taskId", task.taskId)
            .when()
            .post(START_TASK_PATH)
            .then()
            .statusCode(201);

        //upload document via form
        given()
            .spec(inputUpload.asSpec())
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .pathParam("taskId", task.taskId)
            .header("token", token)
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/upload")
            .then()
            .statusCode(204)
            ;


        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .header("Accept", MediaType.APPLICATION_JSON)
            .pathParam("documentId", inputUpload.id.id)
            .when()
            .get("/api/v1/document/{sessionId}/{documentId}")
            .then()
            .body("id", is(inputUpload.id.id.toString()))
            .body("title" , is(inputUpload.title))
            .body("shortDescription" , is(inputUpload.shortDescription))
            .body("longDescription" , is(inputUpload.longDescription))
            .statusCode(200);

        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.FETCH_DOCUMENT, 1, sessionFactory),
            hasAuditEntries(auditFetchDocument())
        );
    }

    @Test
    public void canNotFetchUploadedDocumentWithInvalidToken(@TempDir Path baseTempDir) throws JsonProcessingException, URISyntaxException, IOException {

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);
        var expectedReport = someVerificationReport();

        UploadDescription inputUpload = textFile(baseTempDir);

        Mockito.when(mockSignatureVerifier.verify(matchesInputStream(Files.readAllBytes(inputUpload.content))))
                .thenReturn(Uni.createFrom().item(expectedReport.getBytes()));

        var sessionInfo =sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(objectMapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();

        var token = sessionWithNotarizationRequest.session().accessToken();
        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", token)
            .queryParam("taskId", task.taskId)
            .when()
            .post(START_TASK_PATH)
            .then()
            .statusCode(201);

        //upload document via form
        given()
            .spec(inputUpload.asSpec())
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .pathParam("taskId", task.taskId)
            .header("token", token)
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/upload")
            .then()
            .statusCode(204)
            ;


        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", "INVALID")
            .header("Accept", MediaType.APPLICATION_JSON)
            .pathParam("documentId", inputUpload.id.id)
            .when()
            .get("/api/v1/document/{sessionId}/{documentId}")
            .then()
            .statusCode(401);

    }

    @Test
    public void canDeleteUploadedDocument(@TempDir Path baseTempDir) throws JsonProcessingException, URISyntaxException, IOException {

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);
        var expectedReport = someVerificationReport();

        UploadDescription inputUpload = textFile(baseTempDir);

        Mockito.when(mockSignatureVerifier.verify(matchesInputStream(Files.readAllBytes(inputUpload.content))))
                .thenReturn(Uni.createFrom().item(expectedReport.getBytes()));

        var sessionInfo =sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(objectMapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();

        var token = sessionWithNotarizationRequest.session().accessToken();
        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", token)
            .queryParam("taskId", task.taskId)
            .when()
            .post(START_TASK_PATH)
            .then()
            .statusCode(201);

        //upload document via form
        given()
            .spec(inputUpload.asSpec())
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .pathParam("taskId", task.taskId)
            .header("token", token)
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/upload")
            .then()
            .statusCode(204)
            ;

        //doc is there
        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .header("Accept", MediaType.APPLICATION_JSON)
            .pathParam("documentId", inputUpload.id.id)
            .when()
            .get("/api/v1/document/{sessionId}/{documentId}")
            .then()
            .body("id", is(inputUpload.id.id.toString()))
            .body("title" , is(inputUpload.title))
            .body("shortDescription" , is(inputUpload.shortDescription))
            .body("longDescription" , is(inputUpload.longDescription))
            .statusCode(200);

        //delete
        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .header("Accept", MediaType.APPLICATION_JSON)
            .pathParam("documentId", inputUpload.id.id)
            .when()
            .delete("/api/v1/document/{sessionId}/{documentId}")
            .then()
            .statusCode(204);

        //is gone
        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .header("Accept", MediaType.APPLICATION_JSON)
            .pathParam("documentId", inputUpload.id.id)
            .when()
            .get("/api/v1/document/{sessionId}/{documentId}")
            .then()
            .statusCode(404);
        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.FETCH_DOCUMENT, 1, sessionFactory),
            hasAuditEntries(auditFetchDocument())
        );
    }

    @Test
    public void canNotDeleteUploadedDocumentWithInvalidToken(@TempDir Path baseTempDir) throws JsonProcessingException, URISyntaxException, IOException {

        //use profile without preconTasks
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory, MockState.profileId1);
        var expectedReport = someVerificationReport();

        UploadDescription inputUpload = textFile(baseTempDir);

        Mockito.when(mockSignatureVerifier.verify(matchesInputStream(Files.readAllBytes(inputUpload.content))))
                .thenReturn(Uni.createFrom().item(expectedReport.getBytes()));

        var sessionInfo =sessionWithNotarizationRequest.session();
        //call fetch session to get taskids
        var tasks = given()
            .accept(ContentType.JSON)
            .header("token", sessionInfo.accessToken())
            .when().get(sessionInfo.location())
            .then()
            .statusCode(200)
            .extract()
            .path("tasks");

        var tasklist = Arrays.asList(objectMapper.convertValue(tasks, SessionTaskSummary[].class ));
        var task = tasklist.stream()
            .findAny().orElseThrow();

        var token = sessionWithNotarizationRequest.session().accessToken();
        //startTask
        given()
            .contentType(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", token)
            .queryParam("taskId", task.taskId)
            .when()
            .post(START_TASK_PATH)
            .then()
            .statusCode(201);

        //upload document via form
        given()
            .spec(inputUpload.asSpec())
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .pathParam("taskId", task.taskId)
            .header("token", token)
            .when()
            .post("/api/v1/document/{sessionId}/{taskId}/upload")
            .then()
            .statusCode(204)
            ;

        //doc is there
        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .header("Accept", MediaType.APPLICATION_JSON)
            .pathParam("documentId", inputUpload.id.id)
            .when()
            .get("/api/v1/document/{sessionId}/{documentId}")
            .then()
            .body("id", is(inputUpload.id.id.toString()))
            .body("title" , is(inputUpload.title))
            .body("shortDescription" , is(inputUpload.shortDescription))
            .body("longDescription" , is(inputUpload.longDescription))
            .statusCode(200);

        //delete
        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", "INVALID")
            .header("Accept", MediaType.APPLICATION_JSON)
            .pathParam("documentId", inputUpload.id.id)
            .when()
            .delete("/api/v1/document/{sessionId}/{documentId}")
            .then()
            .statusCode(401);

    }

    private void assertDocumentInDb(SessionInfo sessionInfo, UploadDescription documentUpload, String verificationReport) {

        withTransaction(sessionFactory, (session, tx) -> {
            return session.find(Session.class, sessionInfo.id())
                .chain((found) -> Mutiny.fetch(found.documents))
                .invoke(docs-> {
                    assertThat(docs, hasItem(allOf(
                        hasField("id", is(documentUpload.id.id)),
                        hasField("content", is(documentUpload.contentBytes())),
                        hasField("shortDescription", is(documentUpload.shortDescription)),
                        hasField("longDescription", is(documentUpload.longDescription)),
                        hasField("title", is(documentUpload.title)),
                        hasField("mimetype", is(documentUpload.mimetype)),
                        hasField("extension", is(documentUpload.extension)),
                        hasField("verificationReport", is(verificationReport)),
                        hasField("hash", notNullValue())
                    )));
                });
        });
    }

    private void assertDocumentInDb(SessionInfo sessionInfo, DocumentUploadByLink documentUpload, byte[] content, String verificationReport) {

        withTransaction(sessionFactory, (session, tx) -> {
            return session.find(Session.class, sessionInfo.id())
                .chain((found) -> Mutiny.fetch(found.documents))
                .invoke(docs-> {
                    assertThat(docs, hasItem(allOf(
                        hasField("id", is(documentUpload.id.id)),
                        hasField("content", is(content)),
                        hasField("shortDescription", is(documentUpload.shortDescription)),
                        hasField("longDescription", is(documentUpload.longDescription)),
                        hasField("title", is(documentUpload.title)),
                        hasField("mimetype", is(documentUpload.mimetype)),
                        hasField("extension", is(documentUpload.extension)),
                        hasField("verificationReport", is(verificationReport)),
                        hasField("hash", notNullValue())
                    )));
                });
        });
    }

    public static class UploadDescription {

        public DocumentId id;

        public Path content;

        public String title;

        public String shortDescription;

        public String longDescription;

        public String mimetype;

        public String extension;

        public byte[] contentBytes() {
            try {
                return Files.readAllBytes(content);
            } catch (IOException ex) {
                throw new IllegalArgumentException("Could not read given content", ex);
            }
        }

        public RequestSpecification asSpec() {
            return given()
                .multiPart("content", content.toFile(), mimetype)
                .multiPart("id", id.id.toString())
                .multiPart("title", title)
                .multiPart("shortDescription", shortDescription)
                .multiPart("longDescription", longDescription)
                    .contentType(ContentType.MULTIPART);
        }

    }
    public UploadDescription textFile(Path baseTempDir) {
        final var extension = "txt";
        Path inputUpload = baseTempDir.resolve("inputUpload." + extension);
        try {
            Files.write(inputUpload, DataGen.genBytes());
        } catch (IOException ex) {
            throw new IllegalArgumentException("Could not initalize test data", ex);
        }

        UploadDescription result = new UploadDescription();
        result.id = new DocumentId(UUID.randomUUID());
        result.content = inputUpload;
        result.extension = extension;
        result.mimetype = "text/plain";
        result.title = "Some title";
        result.shortDescription = "short";
        result.longDescription = "long";
        return result;
    }
}
