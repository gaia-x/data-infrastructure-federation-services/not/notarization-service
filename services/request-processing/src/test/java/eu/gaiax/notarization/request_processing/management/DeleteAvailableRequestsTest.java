/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.management;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import eu.gaiax.notarization.request_processing.DataGen;
import static eu.gaiax.notarization.request_processing.Helper.withTransaction;
import static eu.gaiax.notarization.request_processing.Helper.withTransactionAsync;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.RequestorIdentity;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditLogNotaryDelete;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailForNotarizationRequestID;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import static eu.gaiax.notarization.request_processing.matcher.FieldMatcher.hasField;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import java.util.HashSet;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@Tag("security")
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class DeleteAvailableRequestsTest {

    public static final String REQUESTS_PATH = "/api/v1/requests";
    public static final String REQUEST_PATH = "/api/v1/profiles/{profileId}/requests/{notarizationRequestId}";

    public static final Logger logger = Logger.getLogger(DeleteAvailableRequestsTest.class);

    ObjectMapper objectMapper = new ObjectMapper();
    @Inject
    Mutiny.SessionFactory sessionFactory;

    static Stream<NotarizationRequestState> allowedStates() {
        return Stream.of(
            NotarizationRequestState.WORK_IN_PROGRESS
        );
    }

    static Stream<NotarizationRequestState> notAllowedStates() {
        var updateable = allowedStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !updateable.contains(s));
    }

    public static record Sess_Req_Identity_IDs(UUID sessId, UUID notarizationReqId, UUID identityId){};
    private Sess_Req_Identity_IDs createRequestWithStateInDB(ProfileId profile, NotarizationRequestState state) {

        var notarizationReqId = UUID.randomUUID();
        var sessionId = UUID.randomUUID();
        var identityId = UUID.randomUUID();

        withTransactionAsync(sessionFactory, (session, tx) -> {

            var sess =new Session();
            sess.id = sessionId.toString();
            sess.state = state;
            sess.profileId = profile.id();

            sess.identities = new HashSet<RequestorIdentity>();
            var identity = new RequestorIdentity();
            identity.id = identityId;
            identity.data = DataGen.genString();
            identity.session = sess;
            sess.identities.add(identity);

            var nr = new NotarizationRequest();
            nr.id = notarizationReqId;
            nr.session = sess;

            return session.persist(sess).chain(() -> session.persist(nr));
        }).await().indefinitely();

        return new Sess_Req_Identity_IDs(
            sessionId,
            notarizationReqId,
            identityId
        );

    }

    @ParameterizedTest
    @MethodSource("allowedStates")
    public void canDeleteNotarizationRequest(NotarizationRequestState state){
        var profile = MockState.someProfileId;
        var ids = createRequestWithStateInDB(profile, state);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("profileId", profile.id())
            .pathParam("notarizationRequestId", ids.notarizationReqId)
            .when()
            .delete(REQUEST_PATH)
            .then()
            .statusCode(204);

        assertSoftDeletionInDB(ids);

        assertThat(auditTrailForNotarizationRequestID(ids.notarizationReqId.toString(), NotarizationRequestAction.NOTARY_DELETE, 1, sessionFactory),
            hasAuditEntries(auditLogNotaryDelete())
        );
    }

    @ParameterizedTest
    @MethodSource("notAllowedStates")
    public void canNotRejectNotarizationRequest(NotarizationRequestState state){
        var profile = MockState.someProfileId;
        var ids = createRequestWithStateInDB(profile, state);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("profileId", profile.id())
            .pathParam("notarizationRequestId", ids.notarizationReqId)
            .when()
            .delete(REQUEST_PATH)
            .then()
            .statusCode(400);

        assertExistInDb(ids);

        assertThat(auditTrailForNotarizationRequestID(ids.notarizationReqId.toString(), NotarizationRequestAction.NOTARY_DELETE, 1, sessionFactory),
            hasAuditEntries(auditLogNotaryDelete().httpStatus(400))
        );
    }

    private void assertExistInDb(Sess_Req_Identity_IDs ids) {
        withTransaction(sessionFactory, (session, tx) -> {
            var q = session.<Session>createQuery("from Session s left join fetch s.identities where s.id = :id");
            q.setParameter("id", ids.sessId.toString());
            return q.getSingleResult()
                 .invoke(s -> {
                    assertThat(s.request.id, is(ids.notarizationReqId));
                    assertThat(s.identities, contains(hasField("id", is(ids.identityId))));
                });

        });

    }

    private void assertSoftDeletionInDB(Sess_Req_Identity_IDs ids) {
        withTransaction(sessionFactory, (session, tx) -> {
            var q = session.<Session>createQuery("from Session s left join fetch s.identities where s.id = :id");
            q.setParameter("id", ids.sessId.toString());
            return q.getSingleResult()
                 .invoke(s -> {
                    assertThat(s.state, is(NotarizationRequestState.TERMINATED));
                    assertThat(s.identities,is(empty()));
                });

        });

        var identity = withTransaction(sessionFactory, (session, tx) -> {
            return session.find(RequestorIdentity.class, ids.identityId);
        });
        assertThat(identity, nullValue());

        var notaryRequest = withTransaction(sessionFactory, (session, tx) -> {
            return session.find(NotarizationRequest.class, ids.notarizationReqId);
        });
        assertThat(notaryRequest, nullValue());


    }

}
