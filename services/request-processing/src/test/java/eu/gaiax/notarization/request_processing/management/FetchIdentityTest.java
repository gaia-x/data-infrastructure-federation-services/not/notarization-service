/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.management;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.request_processing.DataGen;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import static eu.gaiax.notarization.request_processing.Helper.withTransactionAsync;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.RequestorIdentity;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.entity.SessionTask;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditLogFetchIdentity;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailForNotarizationRequestID;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import io.quarkus.test.common.QuarkusTestResource;
import javax.inject.Inject;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import io.smallrye.mutiny.Uni;
import java.time.Duration;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.oneOf;
import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@Tag("security")
@QuarkusTestResource(MockServicesLifecycleManager.class)
public class FetchIdentityTest {

    public static final String REQUESTS_PATH = "/api/v1/requests";
    public static final String REQUEST_PATH = "/api/v1/profiles/{profileId}/requests/{notarizationRequestId}/identity";

    ObjectMapper objectMapper = new ObjectMapper();
    @Inject
    Mutiny.SessionFactory sessionFactory;

    public static final Random random = new Random();

    static Stream<NotarizationRequestState> allowedStates() {
        return Stream.of(
            NotarizationRequestState.WORK_IN_PROGRESS
        );
    }

    static Stream<NotarizationRequestState> notAllowedStates() {
        var updateable = allowedStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !updateable.contains(s));
    }

    public static record NotReqId_Identies(String notReqId, Set<RequestorIdentity> identities){};

    private NotReqId_Identies createRequestWithStateInDB(NotarizationRequestState state, ProfileId profile, int nbrIdentities) {

        var id = UUID.randomUUID();
        var setOfIdentities = new HashSet<RequestorIdentity>();

        withTransactionAsync(sessionFactory, (session, tx) -> {

            var chain = Uni.createFrom().nullItem();

            var sess =new Session();
            sess.id = UUID.randomUUID().toString();
            sess.state = state;
            sess.profileId = profile.id();

            sess.tasks = new HashSet<SessionTask>();

            var nr = new NotarizationRequest();
            nr.id = id;
            nr.session = sess;
            sess.identities = new HashSet<RequestorIdentity>();

            for(var i=0; i< nbrIdentities; i++){
                var reqId = new RequestorIdentity();
                reqId.id = UUID.randomUUID();
                reqId.algorithm = "An alogrithm";
                reqId.data = DataGen.genString();
                reqId.encryption = "encryption";
                reqId.jwk = "jwk";
                reqId.session = sess;

                setOfIdentities.add(reqId);
                sess.identities.add(reqId);

                chain = chain.chain(() -> session.persist(reqId));
            }

            return chain
                .chain(()-> session.persist(sess))
                .chain(() -> session.persist(nr));

        }).await().atMost(Duration.ofSeconds(20));

        return new NotReqId_Identies(id.toString(), setOfIdentities);

    }

    @ParameterizedTest
    @MethodSource("allowedStates")
    public void canFetchIdentity(NotarizationRequestState state){
        var profile = MockState.someProfileId;
        var ids_identities = createRequestWithStateInDB(state, profile, random.nextInt(1,10));

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("profileId", profile.id())
            .pathParam("notarizationRequestId", ids_identities.notReqId())
            .when()
            .get(REQUEST_PATH)
            .then()
            .statusCode(200)
            .body("[0].data", oneOf(
                ids_identities.identities().stream()
                    .map(i -> i.data)
                    .collect(Collectors.toList())
                    .toArray(String[]::new)
                ));

        assertThat(auditTrailForNotarizationRequestID(ids_identities.notReqId, NotarizationRequestAction.FETCH_IDENTITY, 1, sessionFactory),
            hasAuditEntries(auditLogFetchIdentity())
        );

    }

    @ParameterizedTest
    @MethodSource("notAllowedStates")
    public void canNotFetchIdentity(NotarizationRequestState state){
        var profile = MockState.someProfileId;
        var ids_identities = createRequestWithStateInDB(state, profile, random.nextInt(1,10));

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("profileId", profile.id())
            .pathParam("notarizationRequestId", ids_identities.notReqId())
            .when()
            .get(REQUEST_PATH)
            .then()
            .statusCode(400)
            ;

        assertThat(auditTrailForNotarizationRequestID(ids_identities.notReqId, NotarizationRequestAction.FETCH_IDENTITY, 1, sessionFactory),
            hasAuditEntries(auditLogFetchIdentity().httpStatus(400))
        );
    }

    @Test
    public void canNotFetchIdentity(){
        var profile = MockState.someProfileId;
        createRequestWithStateInDB(NotarizationRequestState.WORK_IN_PROGRESS, profile, random.nextInt(1,10));

        var unkownId = UUID.randomUUID().toString();
        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("profileId", profile.id())
            .pathParam("notarizationRequestId", unkownId)
            .when()
            .get(REQUEST_PATH)
            .then()
            .statusCode(404)
            ;

        assertThat(auditTrailForNotarizationRequestID(unkownId, NotarizationRequestAction.FETCH_IDENTITY, 1, sessionFactory),
            hasAuditEntries(auditLogFetchIdentity().httpStatus(404))
        );
    }

}
