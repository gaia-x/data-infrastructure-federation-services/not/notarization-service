/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.management;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.matchingJsonPath;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.MockSsiIssuanceLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import eu.gaiax.notarization.SsiIssuanceWireMock;
import static eu.gaiax.notarization.request_processing.Helper.withTransaction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import static eu.gaiax.notarization.request_processing.Helper.withTransactionAsync;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.IssuanceResponse;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.ProfileId;
import eu.gaiax.notarization.request_processing.infrastructure.messaging.FrontEndMessageService;
import eu.gaiax.notarization.request_processing.infrastructure.messaging.MsgType;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState.Notary;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditLogAccept;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditLogAssignCredentialOverride;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailForNotarizationRequestID;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import io.quarkus.test.common.QuarkusTestResource;
import javax.inject.Inject;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;
import io.smallrye.reactive.messaging.providers.connectors.InMemorySink;
import io.vertx.core.json.JsonObject;
import java.net.URI;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.enterprise.inject.Any;
import javax.ws.rs.core.MediaType;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@Tag("security")
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
@QuarkusTestResource(MockSsiIssuanceLifecycleManager.class)
public class AcceptAvailableRequestsTest {

    public static final String REQUESTS_PATH = "/api/v1/requests";
    public static final String REQUEST_PATH = "/api/v1/profiles/{profileId}/requests/{notarizationRequestId}/accept";
    public static final String CRED_AUGMENT_PATH = "/api/v1/profiles/{profileId}/requests/{notarizationRequestId}/credentialAugmentation";

    @Inject
    Mutiny.SessionFactory sessionFactory;

    @SsiIssuanceWireMock
    WireMockServer issuanceWireMock;

    @Inject
    ObjectMapper objectMapper;

    @Inject
    @Any
    InMemoryConnector connector;

    InMemorySink<JsonObject> outgoingRequestor;

    static Stream<NotarizationRequestState> allowedStates() {
        return Stream.of(
            NotarizationRequestState.WORK_IN_PROGRESS
        );
    }

    static Stream<NotarizationRequestState> notAllowedStates() {
        var updateable = allowedStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !updateable.contains(s));
    }

    public static record NotAndSessId(
        String notReqId,
        String sessId
    ){};

    private NotAndSessId createRequestWithStateInDB(NotarizationRequestState state, boolean didSet, ProfileId profile) {

        var notReqId = UUID.randomUUID();
        var sessId = UUID.randomUUID();
        withTransactionAsync(sessionFactory, (session, tx) -> {

            var sess =new Session();
            sess.id = sessId.toString();
            sess.state = state;
            sess.profileId = profile.id();

            var nr = new NotarizationRequest();
            nr.id = notReqId;
            nr.session = sess;

            if(didSet){
                nr.did = "did.action:value";
                nr.requestorInvitationUrl = "some-invitation";
            }

            return session.persist(sess).chain(() -> session.persist(nr));
        }).await().indefinitely();

        return new NotAndSessId(
            notReqId.toString(),
            sessId.toString()
        );

    }

    private void assertStateInDB(String id, NotarizationRequestState state){
        withTransactionAsync(sessionFactory, (dbSession, tx) -> {
            return dbSession.find(NotarizationRequest.class, UUID.fromString(id)).invoke((notReq -> {
                assertThat(notReq.session.state, is(state));
            }));
        }).await().indefinitely();
    }

    @BeforeEach
    public void setup() {

        outgoingRequestor = connector.sink(FrontEndMessageService.outgoingRequestorRequestChanged);
        outgoingRequestor.clear();
    }

    @ParameterizedTest
    @MethodSource("allowedStates")
    public void canAcceptNotarizationRequestSummaryById_NoDid(NotarizationRequestState state){

        var profile = MockState.someProfileId;
        var ids = createRequestWithStateInDB(state, false, profile);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", ids.notReqId())
            .pathParam("profileId", profile.id())
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(204);

        assertStateInDB(ids.notReqId(), NotarizationRequestState.PENDING_DID);

        assertThat(auditTrailForNotarizationRequestID(ids.notReqId(), NotarizationRequestAction.ACCEPT, 1, sessionFactory),
            hasAuditEntries(auditLogAccept())
        );

        assertThat(outgoingRequestor.received(), is(not(empty())));
        assertThat(outgoingRequestor.received().get(0).getPayload(), is(new JsonObject(
                String.format(
                        """
                        { "id": "%s", "msg": "%s", "payload": null }
                        """, ids.sessId(), MsgType.REQUEST_ACCEPTED_PENDING_DID))));
    }

    @ParameterizedTest
    @MethodSource("allowedStates")
    public void canAcceptNotarizationRequestSummaryById_didSet(NotarizationRequestState state) {

        var profile = MockState.someProfileId;
        var ids = createRequestWithStateInDB(state, true, profile);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", ids.notReqId())
            .pathParam("profileId", profile.id())
            .contentType(ContentType.JSON)
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(204);

        issuanceWireMock.verify(
            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
            .withRequestBody(matchingJsonPath("$.successURL", WireMock.containing("finishNotarizationRequest")))
        );

        assertStateInDB(ids.notReqId(), NotarizationRequestState.ACCEPTED);
        assertThat(auditTrailForNotarizationRequestID(ids.notReqId(), NotarizationRequestAction.ACCEPT, 1, sessionFactory),
            hasAuditEntries(auditLogAccept())
        );

        assertThat(outgoingRequestor.received(), is(not(empty())));
        assertThat(outgoingRequestor.received().get(0).getPayload(), is(new JsonObject(
                String.format(
                        """
                        { "id": "%s", "msg": "%s", "payload": "" }
                        """, ids.sessId(), MsgType.REQUEST_ACCEPTED))));
    }

    private IssuanceResponse getResp(){
        var resp = new IssuanceResponse();
        resp.invitationURL = URI.create("http://a.url");
        return resp;
    }

    @ParameterizedTest
    @MethodSource("allowedStates")
    public void canAcceptNotarizationRequestSummaryById_didSet_withInviteFromSSI(NotarizationRequestState state) throws JsonProcessingException {
            issuanceWireMock.stubFor(
                post(urlMatching("/credential/start-issuance/"))
                    .willReturn(
                        aResponse()
                            .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                            .withStatus(200)
                            .withBody(objectMapper.writeValueAsString(getResp())))
            );

        var profile = MockState.someProfileId;
        var ids = createRequestWithStateInDB(state, true, profile);

        withTransaction(sessionFactory, (sess, tx) -> {
            var q = sess.createQuery("update NotarizationRequest nr set nr.requestorInvitationUrl = '' where nr.id = :id");
            q.setParameter("id", UUID.fromString(ids.notReqId));
            return q.executeUpdate();
        });

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", ids.notReqId())
            .pathParam("profileId", profile.id())
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(204);

        issuanceWireMock.verify(
            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
            .withRequestBody(matchingJsonPath("$.successURL", WireMock.containing("finishNotarizationRequest")))
        );

        assertStateInDB(ids.notReqId(), NotarizationRequestState.ACCEPTED);
        assertThat(auditTrailForNotarizationRequestID(ids.notReqId(), NotarizationRequestAction.ACCEPT, 1, sessionFactory),
            hasAuditEntries(auditLogAccept())
        );

        assertThat(outgoingRequestor.received(), is(not(empty())));
        assertThat(outgoingRequestor.received().get(0).getPayload(), is(new JsonObject(
                String.format(
                        """
                        { "id": "%s", "msg": "%s", "payload": "http://a.url" }
                        """, ids.sessId(), MsgType.REQUEST_ACCEPTED))));
    }


    @Test
    public void acceptWithManualIssuanceLeadsToPendingRequestorReleaseAndCanBeReleased() {

        var profile = MockState.someProfileId;
        var ids = createRequestWithStateInDB(NotarizationRequestState.WORK_IN_PROGRESS, true, profile);

        //simulate manual release set
        withTransaction(sessionFactory, (sess, tx) -> {
            var q = sess.createQuery("update Session s set s.manualRelease = :relFlag where s.id = :id");
            q.setParameter("relFlag" , true);
            q.setParameter("id" , ids.sessId);
            var q2 = sess.createQuery("update Session s set s.manualReleaseToken = :token where s.id = :id");
            q2.setParameter("token" , "token");
            q2.setParameter("id" , ids.sessId);

            return q.executeUpdate()
                .chain(s -> q2.executeUpdate())
                ;
        });

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", ids.notReqId())
            .pathParam("profileId", profile.id())
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(204);

        assertStateInDB(ids.notReqId(), NotarizationRequestState.PENDING_RQUESTOR_RELEASE);
        assertThat(auditTrailForNotarizationRequestID(ids.notReqId(), NotarizationRequestAction.ACCEPT, 1, sessionFactory),
            hasAuditEntries(auditLogAccept())
        );

        //call manualRelease
        given()
            .when()
            .post("/api/v1/session/triggerIssuance/token")
            .then()
            .statusCode(204)
            ;

        assertStateInDB(ids.notReqId(), NotarizationRequestState.ACCEPTED);

        assertThat(outgoingRequestor.received(), is(not(empty())));
        assertThat(outgoingRequestor.received().get(0).getPayload(), is(new JsonObject(
                String.format(
                        """
                        { "id": "%s", "msg": "%s", "payload": "" }
                        """, ids.sessId(), MsgType.REQUEST_ACCEPTED))));
    }

    @ParameterizedTest
    @MethodSource("notAllowedStates")
    public void canNotAcceptNotarizationRequestSummaryById(NotarizationRequestState state){
        var profile = MockState.someProfileId;
        var ids = createRequestWithStateInDB(state, true, profile);

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", ids.notReqId())
            .pathParam("profileId", profile.id())
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(400);

        assertStateInDB(ids.notReqId(), state);
        assertThat(auditTrailForNotarizationRequestID(ids.notReqId(), NotarizationRequestAction.ACCEPT, 1, sessionFactory),
            hasAuditEntries(auditLogAccept().httpStatus(400))
        );
        assertThat(outgoingRequestor.received(), is(empty()));
    }

    @Test
    public void canNotAcceptNotarizationRequestWithUnknownId() {
        var profile = MockState.someProfileId;
        createRequestWithStateInDB(NotarizationRequestState.WORK_IN_PROGRESS, true, profile);
        var unkownId = UUID.randomUUID().toString();

        given()
            .header("Authorization", MockState.notary1.bearerValue())
            .pathParam("notarizationRequestId", unkownId)
            .pathParam("profileId", profile.id())
            .when()
            .post(REQUEST_PATH)
            .then()
            .statusCode(404);

        assertThat(auditTrailForNotarizationRequestID(unkownId, NotarizationRequestAction.ACCEPT, 1, sessionFactory),
            hasAuditEntries(auditLogAccept().httpStatus(404))
        );
        assertThat(outgoingRequestor.received(), is(empty()));
    }


    @Test
    public void acceptWithNotaryPayloadHasPayload() {

        var profile = MockState.augmentingProfileId;
        var ids = createRequestWithStateInDB(NotarizationRequestState.WORK_IN_PROGRESS, true, profile);

        /*var inputJson = String.format(
                """
                { "notaryValues" : "{ \"uniqueTag\": \"%s\" }" }
                """,
                UUID.randomUUID()); */
        var inputUniqueTag = UUID.randomUUID();
        var inputJson = String.format(
                """
                { "notaryValues" : { "uniqueTag": "%s" } }
                """,
                inputUniqueTag);
        final Notary notary = MockState.notary4;
        given()
                .header("Authorization", notary.bearerValue())
                .pathParam("notarizationRequestId", ids.notReqId())
                .pathParam("profileId", profile.id())
                .contentType(ContentType.JSON)
                .body(inputJson)
                .when()
                .put(CRED_AUGMENT_PATH)
                .then()
                .statusCode(204);

        assertThat(auditTrailForNotarizationRequestID(ids.notReqId(), NotarizationRequestAction.CREDENTIAL_AUGMENTATION_PUT, 1, sessionFactory),
                hasAuditEntries(auditLogAssignCredentialOverride())
        );

        expectAccept(ids, profile, notary);

        assertStateInDB(ids.notReqId(), NotarizationRequestState.ACCEPTED);
        assertThat(auditTrailForNotarizationRequestID(ids.notReqId(), NotarizationRequestAction.ACCEPT, 1, sessionFactory),
            hasAuditEntries(auditLogAccept())
        );

        issuanceWireMock.verify(
            WireMock.postRequestedFor(WireMock.urlMatching("/credential/start-issuance/"))
            .withRequestBody(matchingJsonPath("$.credentialData.givenData.notaryValues.uniqueTag", WireMock.equalTo(inputUniqueTag.toString())))
        );
    }

    private void expectAccept(NotAndSessId ids, ProfileId profile, Notary notary) {
        given()
                .header("Authorization", notary.bearerValue())
                .pathParam("notarizationRequestId", ids.notReqId())
                .pathParam("profileId", profile.id())
                .when()
                .post(REQUEST_PATH)
                .then()
                .statusCode(204);
    }
}
