/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization;

import javax.inject.Inject;
import eu.gaiax.notarization.request_processing.domain.entity.Session;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import static eu.gaiax.notarization.request_processing.Helper.withTransaction;
import eu.gaiax.notarization.request_processing.domain.entity.Document;
import eu.gaiax.notarization.request_processing.domain.entity.NotarizationRequest;
import eu.gaiax.notarization.request_processing.domain.entity.RequestorIdentity;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.time.Period;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.hamcrest.BaseMatcher;
import static org.hamcrest.CoreMatchers.*;
import org.hamcrest.Description;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
public class CleanupRoutinesTest {

    private static final String ROUTINES_PATH = "/api/v1/routines";
    private static final String DEL_TERMINATED_PATH = ROUTINES_PATH + "/deleteTerminated";
    private static final String DEL_TIMEOUT_PATH = ROUTINES_PATH + "/deleteTimeout";
    private static final String DEL_SUBMISSION_TIMEOUT_PATH = ROUTINES_PATH + "/deleteSubmitTimeout";

    @Inject
    Mutiny.SessionFactory sessionFactory;

    public static Logger log = Logger.getLogger(CleanupRoutinesTest.class);

    @ConfigProperty(name = "notarization-processing.terminated.session.retention.period")
    Period retentionPeriod;

    @ConfigProperty(name = "notarization-processing.session.timeout.period")
    Period timeoutPeriod;

    @ConfigProperty(name = "notarization-processing.session.submission.timeout.period")
    Period submissionTimeoutPeriod;

    private UUID addSession(NotarizationRequestState state, OffsetDateTime modDate){
        var id = UUID.randomUUID();
        withTransaction(sessionFactory, (dbSess, ty)->{

            var req = new NotarizationRequest();
            req.id = UUID.randomUUID();

            var d = new Document();
            d.id = UUID.randomUUID();

            var s = new Session();
            s.request = req;
            req.session = s;
            d.session = s;
            s.documents = Set.of(d);
            s.id = id.toString();
            s.state = state;
            return dbSess
                .persist(s)
                .chain(() -> dbSess.persist(req))
                .chain(() -> dbSess.persist(d))
                ;
        });

        addIdentities(id.toString());

        withTransaction(sessionFactory, (dbSess, tx) -> {
            return dbSess.createQuery("update Session s set s.lastModified = '" + modDate.toString() + "' where s.id = '" + id.toString()+"'").executeUpdate();
        });

        return id;
    }

    private UUID addIssuedOldEnoughSession(){
         return addSession(
             NotarizationRequestState.ISSUED,
             OffsetDateTime.now().minus(retentionPeriod).minus(Period.ofDays(1))
         );
    }
    private UUID addTerminatedOldEnoughSession(){
         return addSession(
             NotarizationRequestState.TERMINATED,
             OffsetDateTime.now().minus(retentionPeriod).minus(Period.ofDays(1))
         );
    }
    private UUID addNotTerminatedOldEnoughSession(){
         return addSession(
             NotarizationRequestState.EDITABLE,
             OffsetDateTime.now().minus(retentionPeriod).minus(Period.ofDays(1))
         );
    }
    private UUID addIssuedNotOldEnoughSession(){
         return addSession(
             NotarizationRequestState.ISSUED,
             OffsetDateTime.now().minus(retentionPeriod).plus(Period.ofDays(1))
         );
    }
    private UUID addTerminatedNotOldEnoughSession(){
         return addSession(
             NotarizationRequestState.TERMINATED,
             OffsetDateTime.now().minus(retentionPeriod).plus(Period.ofDays(1))
         );
    }
    private UUID addNotTerminatedNotOldEnoughSession(){
         return addSession(
             NotarizationRequestState.EDITABLE,
             OffsetDateTime.now().minus(retentionPeriod).plus(Period.ofDays(1))
         );
    }

    @Test
    public void routineDeletesTerminatedOrIssuedSessionsOlderThanRetentionPeriod() {

        var toDeleteSessions = List.of(
            addTerminatedOldEnoughSession(),
            addTerminatedOldEnoughSession(),
            addIssuedOldEnoughSession()
        );
        var notToDeleteSessions = List.of(
            addTerminatedNotOldEnoughSession(),
            addTerminatedNotOldEnoughSession(),
            addIssuedNotOldEnoughSession(),

            addNotTerminatedOldEnoughSession(),
            addNotTerminatedNotOldEnoughSession()

        );

        given()
            .when()
            .post(DEL_TERMINATED_PATH)
            .then()
            .statusCode(204);

        assertThat(toDeleteSessions, everyItem(notInDB));
        assertThat(notToDeleteSessions, everyItem(inDB));

    }

    private UUID timedOutSession(NotarizationRequestState state, Period p){
         return addSession(
             state,
             OffsetDateTime.now().minus(p).minus(Period.ofDays(1))
         );
    }

    private UUID notTimedOutSession(NotarizationRequestState state, Period p){
         return addSession(
             state,
             OffsetDateTime.now().minus(p).plus(Period.ofDays(1))
         );
    }


    @Test
    public void routineDeletesTimeoutSessions() {

        var toDeleteSessions = List.of(
            timedOutSession(NotarizationRequestState.PENDING_DID,timeoutPeriod),
            timedOutSession(NotarizationRequestState.EDITABLE,timeoutPeriod)
        );
        var notToDeleteSessions = List.of(
            notTimedOutSession(NotarizationRequestState.PENDING_DID,timeoutPeriod),
            notTimedOutSession(NotarizationRequestState.EDITABLE,timeoutPeriod),

            timedOutSession(NotarizationRequestState.CREATED,timeoutPeriod),
            timedOutSession(NotarizationRequestState.SUBMITTABLE,timeoutPeriod),
            timedOutSession(NotarizationRequestState.ISSUED,timeoutPeriod)
        );

        given()
            .when()
            .post(DEL_TIMEOUT_PATH)
            .then()
            .statusCode(204);

        assertThat(toDeleteSessions, everyItem(sessionTerminated));
        assertThat(notToDeleteSessions, everyItem(sessionNotTerminated));


    }

    @Test
    public void routineDeletesSubmissionTimeoutSessions() {

        var toDeleteSessions = List.of(
            timedOutSession(NotarizationRequestState.CREATED, submissionTimeoutPeriod),
            timedOutSession(NotarizationRequestState.SUBMITTABLE, submissionTimeoutPeriod)
        );
        var notToDeleteSessions = List.of(
            notTimedOutSession(NotarizationRequestState.CREATED, submissionTimeoutPeriod),
            notTimedOutSession(NotarizationRequestState.SUBMITTABLE, submissionTimeoutPeriod),

            timedOutSession(NotarizationRequestState.EDITABLE, submissionTimeoutPeriod),
            timedOutSession(NotarizationRequestState.WORK_IN_PROGRESS, submissionTimeoutPeriod),
            timedOutSession(NotarizationRequestState.ISSUED, submissionTimeoutPeriod)
        );

        given()
            .when()
            .post(DEL_SUBMISSION_TIMEOUT_PATH)
            .then()
            .statusCode(204);

        assertThat(toDeleteSessions, everyItem(sessionTerminated));
        assertThat(notToDeleteSessions, everyItem(sessionNotTerminated));


    }

    private BaseMatcher<UUID> sessionNotTerminated = new BaseMatcher<UUID>() {
        @Override
        public boolean matches(Object id) {
            var s = withTransaction(sessionFactory, (session, tx) -> {
                return session.<Session>find(Session.class, id.toString());
            });
            return s != null && s.state != NotarizationRequestState.TERMINATED;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("is a not terminated session in database");
        }

        @Override
        public void describeMismatch(Object item, Description description) {
            description.appendText("<" + item.toString() + "> was not");
        }
    };

    private BaseMatcher<UUID> inDB = new BaseMatcher<UUID>() {
        @Override
        public boolean matches(Object id) {
            var s = withTransaction(sessionFactory, (session, tx) -> {
                return session.<Session>find(Session.class, id.toString());
            });
            return s != null;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("is an id found in database");
        }

        @Override
        public void describeMismatch(Object item, Description description) {
            description.appendText("<" + item.toString() + "> was not");
        }
    };

    private BaseMatcher<UUID> sessionTerminated = new BaseMatcher<UUID>() {
        private String reason="";
        @Override
        public boolean matches(Object id) {
            reason = "";
            var s = withTransaction(sessionFactory, (session, tx) -> {
                return session.<Session>find(Session.class, id.toString());
            });
            //we also check if identities are deleted just to be sure
            var identities_count = withTransaction(sessionFactory, (session, tx) -> {
                var q = session.<BigInteger>createNativeQuery("select count(id) from requestor_identity r where r.session_id = :sid");
                q.setParameter("sid", id.toString());
                return q.getSingleResult();
            });

            var documents_count = withTransaction(sessionFactory, (session, tx) -> {
                var q = session.<BigInteger>createNativeQuery(" select count(id) from document d where d.session_id = :sid");
                q.setParameter("sid", id.toString());
                return q.getSingleResult();
            });

            if(s != null && s.state == NotarizationRequestState.TERMINATED){
                if(identities_count.intValue() != 0){
                    reason += "found identity, count was: " + identities_count.toString();
                    return false;
                }
                if(documents_count.intValue() != 0){
                    reason += "found documents with no association, count was: " + documents_count.toString();
                    return false;
                }
                return true;
            }else{
                if(s == null){
                    reason += "found no session";
                } else {
                    reason += "found session with state: '"+ s.state + "' instead of TERMINATED";
                }
                return false;
            }
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("a session with terminated state in database");
        }

        @Override
        public void describeMismatch(Object item, Description description) {
            description.appendText("<" + item.toString() + "> was errornous, reason is: " + reason);
        }

    };

    private BaseMatcher<UUID> notInDB = new BaseMatcher<UUID>() {
        private String reason="";
        @Override
        public boolean matches(Object id) {
            reason = "";
            var s = withTransaction(sessionFactory, (session, tx) -> {
                return session.<Session>find(Session.class, id.toString());
            });
            //we also check if identities are deleted just to be sure
            var i = withTransaction(sessionFactory, (session, tx) -> {
                var q = session.<BigInteger>createNativeQuery("select count(id) from requestor_identity r where r.session_id = :sid");
                q.setParameter("sid", id.toString());
                return q.getSingleResult();
            });

            if(s==null){
                if(i.intValue() == 0){
                    return true;
                }else{
                    reason += "found identity, count was: " + i.toString();
                    return false;
                }
            }else{
                reason += "found session";
                return false;
            }
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("an id not found in database");
        }

        @Override
        public void describeMismatch(Object item, Description description) {
            description.appendText("<" + item.toString() + "> was found reson is: " + reason);
        }
    };

    private UUID addIdentities(String sessionId){
        var identiyId = UUID.randomUUID();
        withTransaction(sessionFactory, (session, tx) -> {
            var q = session.<Session>createQuery("from Session s left join fetch s.identities where s.id = :id");
            q.setParameter("id", sessionId);
            return q.getSingleResult()
                .call(s -> {
                    var identiy = new RequestorIdentity();
                    identiy.id = identiyId;
                    identiy.session = s;
                    s.identities.add(identiy);
                    return session.persist(s);
                });
            });
        return identiyId;
    }

}
