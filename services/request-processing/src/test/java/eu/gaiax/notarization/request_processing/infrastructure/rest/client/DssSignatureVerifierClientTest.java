/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.infrastructure.rest.client;

import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.vertx.RunOnVertxContext;
import io.quarkus.test.vertx.UniAsserter;
import io.smallrye.mutiny.Uni;
import java.io.IOException;
import java.io.InputStream;
import javax.inject.Inject;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Michael Rauh
 */
@QuarkusTest
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
public class DssSignatureVerifierClientTest {

    private static final Logger LOG = Logger.getLogger(DssSignatureVerifierClientTest.class);

    private static InputStream validSignedDocument;
    private static InputStream invalidSignedDocument;

    @Inject
    DssSignatureVerifierClient signatureVerifierService;
    
    @BeforeAll
    public static void init() throws IOException {
        validSignedDocument = DssSignatureVerifierClientTest.class.getResourceAsStream("/valid-signed-document.xml");
        invalidSignedDocument = DssSignatureVerifierClientTest.class.getResourceAsStream("/invalid-xades-structure.xml");
    }

    @Test
    @RunOnVertxContext
    public void testValidSignedDocument(UniAsserter asserter) {
        Uni<byte[]> rawEtsiReport = this.signatureVerifierService.verify(validSignedDocument);

        asserter.execute(() -> rawEtsiReport.invoke((report) -> {
            var asString = new String(report);
            assertThat(asString, containsString("mainindication:passed"));
            assertThat(asString, not(containsString("mainindication:failed")));
        }));
    }

    @Test
    @RunOnVertxContext
    public void testInValidSignedDocument(UniAsserter asserter) {
        Uni<byte[]> rawEtsiReport = this.signatureVerifierService.verify(invalidSignedDocument);

        asserter.execute(() -> rawEtsiReport.invoke((report) -> {
            var asString = new String(report);
            assertThat(asString, containsString("total-failed"));
            assertThat(asString, containsString("HASH_FAILURE"));
        }));
    }
}
