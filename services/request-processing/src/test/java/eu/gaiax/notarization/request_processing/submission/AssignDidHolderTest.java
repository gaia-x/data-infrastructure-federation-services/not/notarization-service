/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.submission;

import eu.gaiax.notarization.request_processing.Helper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.MockSsiIssuanceLifecycleManager;
import eu.gaiax.notarization.RabbitMqTestResourceLifecycleManager;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestState;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditAssignDid;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailFor;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import static eu.gaiax.notarization.request_processing.Helper.SESSION_VARIABLE;
import static eu.gaiax.notarization.request_processing.Helper.SUBMISSION_PATH;
import static eu.gaiax.notarization.request_processing.Helper.assertStateInStoredSession;
import static eu.gaiax.notarization.request_processing.Helper.createSubmitNotarizationRequest;
import static eu.gaiax.notarization.request_processing.Helper.prepareSessionWithSubmittedNotarizationRequest;
import eu.gaiax.notarization.request_processing.infrastructure.rest.mock.MockState;
import io.quarkus.test.common.QuarkusTestResource;
import javax.inject.Inject;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import static io.restassured.RestAssured.given;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hibernate.reactive.mutiny.Mutiny;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
@QuarkusTestResource(RabbitMqTestResourceLifecycleManager.class)
@QuarkusTestResource(MockSsiIssuanceLifecycleManager.class)
public class AssignDidHolderTest {

    public static final String ASSIGN_DID_HOLDER_PATH = "/api/v1/session/{sessionId}/submission/did-holder";
    public static final String HOLDER_VARIABLE = "didHolder";
    public static final String INVITATION_VARIABLE = "invitation";

    ObjectMapper objectMapper = new ObjectMapper();
    @Inject
    Mutiny.SessionFactory sessionFactory;

    static Stream<NotarizationRequestState> allowedStates() {
        return Stream.of(
            NotarizationRequestState.EDITABLE,
            NotarizationRequestState.READY_FOR_REVIEW,
            NotarizationRequestState.WORK_IN_PROGRESS,
            NotarizationRequestState.ACCEPTED,
            NotarizationRequestState.PENDING_DID
        );
    }

    static Stream<NotarizationRequestState> notAllowedStates() {
        var updateable = allowedStates().collect(Collectors.toSet());
        return Stream.of(NotarizationRequestState.values()).filter((s) -> !updateable.contains(s));
    }

    @Test
    public void stateChangesIfPendingDid(){

        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);
        Helper.setSessionState(sessionWithNotarizationRequest.session(),NotarizationRequestState.PENDING_DID , sessionFactory);

        var holderVal = "did:action:newVal";
        var invitationVal = "did:this-is-another-invitation";

        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam(HOLDER_VARIABLE, holderVal)
            .queryParam(INVITATION_VARIABLE, invitationVal)
            .when().put(ASSIGN_DID_HOLDER_PATH)
            .then()
            .statusCode(204);


        assertStateInStoredSession(sessionWithNotarizationRequest.session().id(), sessionFactory, NotarizationRequestState.ACCEPTED);


    }

    @ParameterizedTest
    @MethodSource("allowedStates")
    public void canAssignDidHolderToNotRequest(NotarizationRequestState state) throws JsonProcessingException {

        var notarizationRequest = createSubmitNotarizationRequest();
        notarizationRequest.holder = null;
        notarizationRequest.invitation = null;
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(
                notarizationRequest, state, sessionFactory, MockState.someProfileId);

        Helper.setSessionState(sessionWithNotarizationRequest.session(), state, sessionFactory);

        var holderVal = "did:action:newVal";
        var invitationVal = "did:this-is-another-invitation";

        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam(HOLDER_VARIABLE, holderVal)
            .queryParam(INVITATION_VARIABLE, invitationVal)
            .when().put(ASSIGN_DID_HOLDER_PATH)
            .then()
            .statusCode(204);

        given().accept(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .when()
            .get(SUBMISSION_PATH)
            .then()
            .statusCode(200)
            .body("holder", is(holderVal))
            ;

        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.ASSIGN_DID, 1, sessionFactory),
            hasAuditEntries(auditAssignDid())
        );
    }

    @ParameterizedTest
    @MethodSource("notAllowedStates")
    public void cannotAssignDidHolderToNotRequest(NotarizationRequestState state) throws JsonProcessingException {

        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);
        Helper.setSessionState(sessionWithNotarizationRequest.session(), state, sessionFactory);

        var holderVal = "did:action:newVal";

        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .queryParam(HOLDER_VARIABLE, holderVal)
            .when().put(ASSIGN_DID_HOLDER_PATH)
            .then()
            .statusCode(400);

        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.ASSIGN_DID, 1, sessionFactory),
            hasAuditEntries(auditAssignDid().httpStatus(400))
        );
    }


    @Test
    public void invalidTokenDetected() {
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);

        var holderVal = "did:action:newVal";

        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken() + "INVALID")
            .queryParam(HOLDER_VARIABLE, holderVal)
            .when().put(ASSIGN_DID_HOLDER_PATH)
            .then()
            .statusCode(401);

        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.ASSIGN_DID, 1, sessionFactory),
            hasAuditEntries(auditAssignDid().httpStatus(401))
        );
    }

    @Test
    public void noTokenDetected() {
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);

        var holderVal = "did:action:newVal";

        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .queryParam(HOLDER_VARIABLE, holderVal)
            .when().put(ASSIGN_DID_HOLDER_PATH)
            .then()
            .statusCode(400);

        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.ASSIGN_DID, 1, sessionFactory),
            hasAuditEntries(auditAssignDid().httpStatus(400))
        );
    }

    @Test
    public void emptyTokenDetected() {
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);

        var holderVal = "did:action:newVal";

        given()
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", "")
            .queryParam(HOLDER_VARIABLE, holderVal)
            .when().put(ASSIGN_DID_HOLDER_PATH)
            .then()
            .statusCode(401);

        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.ASSIGN_DID, 1, sessionFactory),
            hasAuditEntries(auditAssignDid().httpStatus(401))
        );
    }
}
