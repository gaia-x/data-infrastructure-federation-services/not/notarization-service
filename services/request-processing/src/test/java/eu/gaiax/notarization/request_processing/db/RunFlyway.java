/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.db;

import io.quarkus.arc.Arc;
import io.quarkus.flyway.runtime.FlywayContainer;
import io.quarkus.flyway.runtime.FlywayContainerProducer;
import io.quarkus.flyway.runtime.QuarkusPathLocationScanner;
import io.quarkus.runtime.StartupEvent;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.sql.DataSource;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class RunFlyway {
    private static final Logger LOGGER = Logger.getLogger(RunFlyway.class);

    String datasourceUrl;
    String datasourceUsername;
    String datasourcePassword;

    List<String> files;

    List<String> locations;

    public RunFlyway(@ConfigProperty(name = "quarkus.datasource.reactive.url") String datasourceUrl,
            @ConfigProperty(name = "quarkus.datasource.username") String datasourceUsername,
            @ConfigProperty(name = "quarkus.datasource.password") String datasourcePassword,
            @ConfigProperty(name = "notarization-processing.flyway.files") List<String> locations,
            @ConfigProperty(name = "notarization-processing.flyway.files") List<String> files) {
        this.datasourceUrl = datasourceUrl;
        this.datasourceUsername = datasourceUsername;
        this.datasourcePassword = datasourcePassword;
        this.files = files;
        this.locations = locations;
    }

    public void runFlywayMigration(@Observes StartupEvent event) {
        runMigration();
    }

    public void runMigration() throws FlywayException {
        LOGGER.info("Initalizing flyway ...");
        QuarkusPathLocationScanner.setApplicationMigrationFiles(files);
        DataSource ds = Flyway.configure().dataSource(nonReactiveDatasourceUrl(), datasourceUsername, datasourcePassword).getDataSource();
        FlywayContainerProducer flywayProducer = Arc.container().instance(FlywayContainerProducer.class).get();
        FlywayContainer flywayContainer = flywayProducer.createFlyway(ds, "<default>", true, true);
        Flyway flyway = flywayContainer.getFlyway();

        flyway.migrate();
        LOGGER.info("Completed flyway migration");
    }

    private String nonReactiveDatasourceUrl() {
        return "jdbc:" + datasourceUrl.replaceFirst("^vertx-reactive:", "");
    }
}
