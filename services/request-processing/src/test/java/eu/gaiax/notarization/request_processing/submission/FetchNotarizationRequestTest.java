/****************************************************************************
 * Copyright 2022 ecsec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************/

package eu.gaiax.notarization.request_processing.submission;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.notarization.MockServicesLifecycleManager;
import eu.gaiax.notarization.request_processing.domain.model.NotarizationRequestAction;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditLogFetch;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.auditTrailFor;
import static eu.gaiax.notarization.request_processing.matcher.AuditTrailMatcher.hasAuditEntries;
import static eu.gaiax.notarization.request_processing.Helper.SESSION_VARIABLE;
import static eu.gaiax.notarization.request_processing.Helper.SUBMISSION_PATH;
import static eu.gaiax.notarization.request_processing.Helper.prepareSessionWithSubmittedNotarizationRequest;
import io.quarkus.test.common.QuarkusTestResource;
import javax.inject.Inject;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Florian Otto
 */
@QuarkusTest
@QuarkusTestResource(MockServicesLifecycleManager.class)
public class FetchNotarizationRequestTest {

    public static final Logger LOG = Logger.getLogger(FetchNotarizationRequestTest.class);

    public static final String DOCUMENTS_PATH = "/api/v1/session/{sessionId}/submission/documents";

    ObjectMapper objectMapper = new ObjectMapper();
    @Inject
    Mutiny.SessionFactory sessionFactory;

    @Test
    public void canFetchSubmittedNotarizationRequestByLocation() throws JsonProcessingException {
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);

        var res = given().accept(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken())
            .when().get(SUBMISSION_PATH)
            .then()
            .statusCode(200)
            .body("holder", is(sessionWithNotarizationRequest.notarizationRequest().holder.value))
            .body("profileId", is(sessionWithNotarizationRequest.session().profileId()))
            .extract().response();

        var returnedData = objectMapper.readTree(res.body().asString());

        assertThat(returnedData.get("data").toPrettyString(), is(sessionWithNotarizationRequest.notarizationRequest().data.toPrettyString()));

        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.FETCH, 1, sessionFactory),
            hasAuditEntries(auditLogFetch())
        );
    }

    @Test
    public void invalidTokenDetectedOnFetchNotRequest() {
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);

        given().accept(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", sessionWithNotarizationRequest.session().accessToken() + "INVALID")
            .when().get(SUBMISSION_PATH)
            .then()
            .statusCode(401);

        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.FETCH, 1, sessionFactory),
            hasAuditEntries(auditLogFetch().httpStatus(401))
        );
    }

    @Test
    public void noTokenDetectedOnFetchNotRequest() {
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);

        given().accept(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .when().get(SUBMISSION_PATH)
            .then()
            .statusCode(400);

        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.FETCH, 1, sessionFactory),
            hasAuditEntries(auditLogFetch().httpStatus(400))
        );
    }

    @Test
    public void emptyTokenDetectedOnFetchNotRequest() {
        var sessionWithNotarizationRequest = prepareSessionWithSubmittedNotarizationRequest(sessionFactory);

        given().accept(ContentType.JSON)
            .pathParam(SESSION_VARIABLE, sessionWithNotarizationRequest.session().id())
            .header("token", "")
            .when().get(SUBMISSION_PATH)
            .then()
            .statusCode(401);
        assertThat(auditTrailFor(sessionWithNotarizationRequest.session().id(), NotarizationRequestAction.FETCH, 1, sessionFactory),
            hasAuditEntries(auditLogFetch().httpStatus(401))
        );
    }
}
