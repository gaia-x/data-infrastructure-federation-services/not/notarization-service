version: "3"
services:

  # Jaeger
  jaeger-all-in-one:
    image: jaegertracing/all-in-one:1.34.1
    container_name: jaegertracing
    ports:
      - "16686:16686"
      - "14268"
      - "14250"

  # OpenTelemetry Collector
  otel-collector:
    image: otel/opentelemetry-collector:0.50.0
    container_name: opentelemetry-collector
    command: ["--config=/etc/otel-collector-config.yaml"]
    volumes:
      - ../../../deploy/resources/config/otel-collector-config.yaml:/etc/otel-collector-config.yaml
    ports:
      - "13133:13133"  # Health_check extension
      - "4317:4317"    # OTLP gRPC receiver
      - "55680:55680"  # OTLP gRPC receiver alternative port
    depends_on:
      - jaeger-all-in-one

  dss:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/dss-demo-webapp:5.10.1
    container_name: dss
    ports:
      - "8080"

  request-processing-db:
    image: postgres:14
    container_name: request-processing-db
    ports:
      - "5434:5432"
    environment:
      POSTGRES_USER: request
      POSTGRES_PASSWORD: request
      POSTGRES_DB: requests_database
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U request -d requests_database"]
      interval: 5s
      timeout: 5s
      retries: 5

  request-processing-flyway:
    image: flyway/flyway:8.5
    container_name: request-processing-flyway
    command: "-locations=filesystem:/sql-migrations -url=jdbc:postgresql://request-processing-db:5432/requests_database -schemas=public -user=request -password=request -connectRetries=60 migrate"
    volumes:
      - ../../../services/request-processing/src/main/jib/db-flyway/:/sql-migrations
    depends_on:
      request-processing-db:
        condition: service_healthy

  rabbitmq:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/rabbitmq-dev:3.9-management
    container_name: 'rabbitmq'
    ports:
        - 5671:5671   # amqp/tls
        - 5672:5672   # amqp
        - 15672:15672 # http
        - 15692:15692 # prometheus
    environment:
      RABBITMQ_DEFAULT_USER: request-rabbit
      RABBITMQ_DEFAULT_PASS: request-rabbit-password
    volumes:
      - ../../../services/request-processing/deploy/docker-compose/infra/rabbitmq.conf:/etc/rabbitmq/rabbitmq.conf
      - ../../../services/request-processing/deploy/docker-compose/infra/rabbitmq_definitions.json:/etc/rabbitmq/definitions.json
      - ../../../services/request-processing/deploy/docker-compose/infra/ca_certificate.pem:/notarization/ca_certificate.pem
      - ../../../services/request-processing/deploy/docker-compose/infra/server_notarization_certificate.pem:/notarization/server_certificate.pem
      - ../../../services/request-processing/deploy/docker-compose/infra/server_notarization_key.pem:/notarization/server_key.pem

  keycloak:
    image: quay.io/keycloak/keycloak:${KEYCLOAK_VERSION:-18.0.0}
    ports:
      - "${KEYCLOAK_PORT:-9194}:${KEYCLOAK_PORT:-9194}"
    command: "start-dev --http-port=${KEYCLOAK_PORT:-9194} --log-level=INFO"
    environment:
      - KEYCLOAK_ADMIN=${KEYCLOAK_ADMIN:-keycloak}
      - KEYCLOAK_PORT=${KEYCLOAK_PORT:-9194}
      - KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD:-keycloakcd}
      - KC_HOSTNAME=localhost
      - DB_DATABASE=${KEYCLOAK_DATABASE_NAME:-keycloakdb}
      - DB_USER=${KEYCLOAK_DATABASE_USER:-keycloakdb}
      - DB_PASSWORD=${KEYCLOAK_DATABASE_PASSWORD:-keycloakdb}
      - DB_ADDR=keycloakdb
      - DB_VENDOR=postgres
    networks:
      default:
        aliases:
          - keycloak
    depends_on:
      - keycloakdb
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:${KEYCLOAK_PORT:-9194}"]
      start_period: 50s
      interval: 5s
      timeout: 5s
      retries: 150

  keycloakdb:
    image: postgres:${POSTGRES_VERSION:-14}
    container_name: keycloak-db
    ports:
      - "5433:5432"
    environment:
      - POSTGRES_USER=${KEYCLOAK_DATABASE_USER:-keycloakdb}
      - POSTGRES_PASSWORD=${KEYCLOAK_DATABASE_PASSWORD:-keycloakdb}
      - POSTGRES_DB=${KEYCLOAK_DATABASE_NAME:-keycloakdb}
    networks:
      default:

  keycloak-initial-config-importer:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/keycloak-jq:18.0.0
    container_name: keycloak-config
    depends_on:
      keycloak:
        condition: service_healthy
    environment:
      - KEYCLOAK_ADMIN=${KEYCLOAK_ADMIN:-keycloak}
      - KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD:-keycloakcd}
      - KEYCLOAK_PORT=9194
      - KC_HOSTNAME=keycloak
    volumes:
      - ../../../services/request-processing/deploy/docker-compose/infra/keycloak_init.sh:/tmp/keycloak_init.sh
    restart: on-failure
    entrypoint: "/tmp/keycloak_init.sh"
    networks:
      default:

  request-processing-java17:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/not/notarization-service/request-processing:java17-latest
    container_name: request-processing-java17
    depends_on:
      - request-processing-flyway
      - keycloak-initial-config-importer
    ports:
      - "8084:8084"
    environment:
      QUARKUS_HTTP_PORT: 8084
      QUARKUS_HTTP_ACCESS_LOG_ENABLED: "true"
      QUARKUS_REST_CLIENT_PROFILE_API_URL: http://profile:8083/
      BROWSER_IDENTIFICATION_URL: http://oidc-identity-resolver:8085/session/
      VC_IDENTIFICATION_URL: "${SSI_SERVICE_URL:-http://ssi-issuance:8080}/credential/verify"
      QUARKUS_REST_CLIENT_LOGGING_SCOPE: request-response
      QUARKUS_REST_CLIENT_LOGGING_BODY_LIMIT: 500
      QUARKUS_HTTP_ACCESS_LOG_EXCLUDE_PATTERN: (\/api\/v1\/routines\/deleteTimeout|\/api\/v1\/routines\/deleteSubmitTimeout|\/api\/v1\/routines\/deleteTerminated)
      QUARKUS_LOG_CATEGORY__ORG_JBOSS_RESTEASY_REACTIVE_CLIENT_LOGGING__LEVEL: DEBUG
      QUARKUS_REST_CLIENT_SSI_ISSUANCE_API_URL: "${SSI_SERVICE_URL:-http://ssi-issuance:8080}"
      QUARKUS_REST_CLIENT_REVOCATION_API_URL: http://revocation:8086
      QUARKUS_DATASOURCE_REACTIVE_URL: postgresql://request-processing-db:5432/requests_database
      QUARKUS_HIBERNATE_ORM_DATABASE_GENERATION: none
      QUARKUS_DATASOURCE_USERNAME: request
      QUARKUS_DATASOURCE_PASSWORD: request
      QUARKUS_OIDC_AUTH_SERVER_URL: http://keycloak:9194/realms/notarization-realm
      QUARKUS_OIDC_CLIENT_ID: notarization-client
      QUARKUS_OIDC_CREDENTIALS_SECRET: notarization-api-secret-12345
      QUARKUS_OIDC_DISCOVERY_ENABLED: false
      QUARKUS_OIDC_INTROSPECTION_PATH: http://keycloak:9194/realms/notarization-realm/protocol/openid-connect/token/introspect
      RABBITMQ_HOST: rabbitmq
      RABBITMQ_PORT: 5672
      RABBITMQ_USERNAME: request-processing-user
      RABBITMQ_PASSWORD: request-rabbit-password
      NOTARIZATION_PROCESSING_INTERNAL_URL: http://request-processing:8084
      NOTARIZATION_RABBITMQ_SSL: "false"
      NOTARIZATION_RABBITMQ_CA_PATH: /tmp/ca_certificate.pem
      NOTARIZATION_RABBITMQ_TLS_CERT_PATH: /tmp/client_certificate.pem
      NOTARIZATION_RABBITMQ_TLS_KEY_PATH: /tmp/client_key.pem
      QUARKUS_OPENTELEMETRY_TRACER_EXPORTER_OTLP_ENDPOINT: http://otel-collector:4317
      QUARKUS_REST_CLIENT_DSS_API_URL: http://dss:8080/services/rest
    volumes:
      - ../../../services/request-processing/deploy/docker-compose/infra/ca_certificate.pem:/tmp/ca_certificate.pem
      - ../../../services/request-processing/deploy/docker-compose/infra/client_notarization_certificate.pem:/tmp/client_certificate.pem
      - ../../../services/request-processing/deploy/docker-compose/infra/client_notarization_key.pem:/tmp/client_key.pem
    restart: on-failure
    networks:
      default:
        aliases:
          - request-processing
    deploy:
      resources:
        limits:
          memory: 1G
          cpus: '1'
        reservations:
          memory: 256M
          cpus: '0.25'
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:8084/q/health/ready"]
      interval: 20s
      timeout: 20s
      retries: 5
